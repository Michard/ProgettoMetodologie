package com.myunit.progetto.play;

import com.myunit.assertion.TestFailedError;
import com.myunit.test.Sorted;
import com.myunit.test.Test;
import gapp.ulg.game.GameFactory;
import gapp.ulg.game.board.GameRuler;
import gapp.ulg.game.board.PieceModel;
import gapp.ulg.game.board.Player;
import gapp.ulg.game.util.Utils;
import gapp.ulg.games.MNKgame;
import gapp.ulg.games.MNKgameFactory;
import gapp.ulg.play.MCTSPlayer;
import gapp.ulg.play.RandPlayer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;

import static com.myunit.assertion.Assert.assertNull;
import static com.myunit.assertion.Assert.assertTrue;

public class TestMCTSPlayer {
    @Test
    @Sorted(0)
    public void MCTS_SequenzialeThreadsDefault() {
        assertTrue(isGoodEnough(new MCTSPlayer<>("a", 30, false), 100, 75, 3));
    }

    @Test
    @Sorted(10)
    public void MCTS_ParalleloThreadsDefault() {
        assertTrue(isGoodEnough(new MCTSPlayer<>("a", 30, true), 100, 75, 3));
    }

    @Test
    @Sorted(20)
    public void MCTS_ParalleloBgexec() {
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 100, true);

        ExecutorService ex = Executors.newFixedThreadPool(10);
        p.threads(0, null, ex);
        assertTrue(isGoodEnough(p, 100, 75, 3), "1");
        ex.shutdown();

        ex = Executors.newSingleThreadExecutor();
        p.threads(0, null, ex);
        assertTrue(isGoodEnough(p, 100, 75, 3), "2");
        ex.shutdown();

        ex = Executors.newCachedThreadPool();
        p.threads(0, null, ex);
        assertTrue(isGoodEnough(p, 100, 75, 3), "3");
        ex.shutdown();
    }

    @Test
    @Sorted(30)
    public void MCTS_ParalleloForkJoinPool() {
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 100, true);
        ForkJoinPool fjp = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        p.threads(0, fjp, null);
        assertTrue(isGoodEnough(p, 100, 75, 3), "1");
        p.threads(0, ForkJoinPool.commonPool(), null);
        assertTrue(isGoodEnough(p, 100, 75, 3), "2");
    }

    @Test
    @Sorted(40)
    public void MCTS_Parallelo0Thread() {
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 100, true);
        p.threads(0, null, null);
        assertTrue(isGoodEnough(p, 100, 75, 3));
    }

    @Test
    @Sorted(50)
    public void MCTS_Parallel10Thread() {
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 100, true);
        p.threads(10, null, null);
        assertTrue(isGoodEnough(p, 100, 75, 3));
    }

    @Test
    @Sorted(60)
    public void MCTS_ParalleloThreadNonLimitati() {
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 100, true);
        p.threads(-1, null, null);
        assertTrue(isGoodEnough(p, 100, 75, 3));
    }

    @Test
    @Sorted(70)
    public void MCTS_ControllaTimerPerLaMossaNonCausaEccezione_Seq() {
        Utils.play(newMinTimeOutMNKgame(), new MCTSPlayer<>("a", 30, false), new RandPlayer<>("b"));
    }

    @Test
    @Sorted(80)
    public void MCTS_ControllaTimerPerLaMossaNonCausaEccezione_ParDefault() {
        Utils.play(newMinTimeOutMNKgame(), new MCTSPlayer<>("a", 30, true), new RandPlayer<>("b"));
    }

    @Test
    @Sorted(90)
    public void MCTS_ControllaTimerPerLaMossaNonCausaEccezione_ParBgexec() {
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 30, true);
        ExecutorService ex = Executors.newFixedThreadPool(10);
        p.threads(0, null, ex);
        Utils.play(newMinTimeOutMNKgame(), p, new RandPlayer<>("b"));
        ex.shutdown();
    }

    @Test
    @Sorted(100)
    public void MCTS_InterruptSeq() {
        testInterrupt(false);
    }

    @Test
    @Sorted(110)
    public void MCTS_InterruptParDef() {
        testInterrupt(true);
    }

    private void testInterrupt(boolean parallel) {
        ThreadCommunicator tC = new ThreadCommunicator();
        MCTSPlayer<PieceModel<PieceModel.Species>> p = new MCTSPlayer<>("a", 200000, parallel);
        p.setGame(newMinTimeOutMNKgame().newGame());
        Thread t = new Thread(() -> {
            try {
                assertNull(p.getMove());
            } catch (TestFailedError e) {
                tC.signalException(e);
            }
        });
        t.setDaemon(true);
        t.start();
        t.interrupt();
        if (tC.getException() != null) throw new RuntimeException(tC.getException());
    }

    private static class ThreadCommunicator {
        Throwable exception = null;

        void signalException(Throwable t) {
            exception = t;
        }

        Throwable getException() {
            return exception;
        }
    }

    /**
     * Simula {@code numWholeTest} tentativi, cioè {@code numTries} partite,
     * tra {@code p} e un {@link RandPlayer} al 3,3,3-game. Un tentativo è riuscito se {@code p}
     * vince almeno {@code atLeast} partite. Ritorna false se e solo se falliscono tutti i
     * tentativi.
     *
     * @param p            Il player da testare
     * @param numTries     Numero di partite per tentativo
     * @param atLeast      Numero di partite da vincere per completare con successo il tentativo
     * @param numWholeTest Numero di tentativi
     * @return Se almeno un tentativo è stato completato con successo
     */
    private boolean isGoodEnough(
            Player<PieceModel<PieceModel.Species>> p,
            int numTries,
            int atLeast,
            int numWholeTest
    ) {
        GameFactory<GameRuler<PieceModel<PieceModel.Species>>> gF = new MNKgameFactory();
        gF.setPlayerNames("a", "b");
        RandPlayer<PieceModel<PieceModel.Species>> p2 = new RandPlayer<>("b");
        for (int i = 0; i < numWholeTest; i++) {
            int wins = 0;
            for (int j = 0; j < numTries; j++) {
                GameRuler<PieceModel<PieceModel.Species>> gR = Utils.play(gF, p, p2);
                if (gR.result() == 1) {
                    wins++;
                }
            }
            if (wins >= atLeast) return true;
        }
        return false;
    }

    private GameFactory<GameRuler<PieceModel<PieceModel.Species>>> newMinTimeOutMNKgame() {
        return new MNKgameFactory() {
            @Override
            public GameRuler<PieceModel<PieceModel.Species>> newGame() {
                return new MNKgame(1, 3, 3, 3, "a", "b");
            }
        };
    }
}

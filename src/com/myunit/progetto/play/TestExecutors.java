package com.myunit.progetto.play;

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.concurrent.*;

public class TestExecutors {
    public static void main(String[] args) throws java.lang.Exception {
        ExecutorService e = Executors.newSingleThreadExecutor(runnable -> {
            Thread t = Executors.defaultThreadFactory().newThread(runnable);
            t.setDaemon(true);
            return t;
        });
        e.submit(() -> {while(true) try {Thread.sleep(100);}catch (Throwable t) {}});
        System.out.println("AAAAAAA");
    }
}

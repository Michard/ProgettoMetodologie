package com.myunit.progetto.games;

import gapp.ulg.game.board.PieceModel;
import gapp.ulg.game.util.Utils;
import gapp.ulg.games.MyBreakthroughFactory;
import gapp.ulg.play.MCTSPlayer;
import gapp.ulg.play.RandPlayer;

public class TestMyBreaktrough {
    public static void main(String[] args) {
        MyBreakthroughFactory gF = new MyBreakthroughFactory();
        gF.setPlayerNames("a","b");
        Utils.play(gF, new MCTSPlayer<PieceModel<PieceModel.Species>>("a", 10, false), new MCTSPlayer<PieceModel<PieceModel.Species>>("b", 10, false));
    }
}

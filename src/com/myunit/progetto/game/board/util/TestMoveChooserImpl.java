package com.myunit.progetto.game.board.util;

import com.myunit.test.Sorted;
import com.myunit.test.Test;
import gapp.ulg.game.board.*;
import gapp.ulg.game.util.*;
import gapp.ulg.games.MNKgame;
import gapp.ulg.games.Othello;
import gapp.ulg.play.MCTSPlayer;
import gapp.ulg.play.RandPlayer;

import java.util.*;
import java.util.function.Consumer;

import static com.myunit.assertion.Assert.*;
import static gapp.ulg.game.board.Board.Dir.*;

@SuppressWarnings("unused")
public class TestMoveChooserImpl {
    private static class DamaInizio extends GameRulerAdapter<PieceModel<PieceModel.Species>> {
        @Override
        public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
            Set<Move<PieceModel<PieceModel.Species>>> mosse = new HashSet<>();
            Action<PieceModel<PieceModel.Species>>
                    action1 = new Action<>(new Pos(2, 0), new Pos(4, 2)),
                    action2 = new Action<>(new Pos(4, 2), new Pos(2, 4)),
                    action3 = new Action<>(new Pos(2, 4), new Pos(0, 6)),
                    action4 = new Action<>(new Pos(3, 1), new Pos(3, 3), new Pos(1, 5)),
                    action5 = new Action<>(new Pos(2, 4), new Pos(0, 2)),
                    action6 = new Action<>(new Pos(3, 1), new Pos(3, 3), new Pos(1, 3)),
                    action7 = new Action<>(new Pos(4, 2), new Pos(6, 4)),
                    action8 = new Action<>(new Pos(6, 4), new Pos(4, 6)),
                    action9 = new Action<>(new Pos(3, 1), new Pos(5, 3), new Pos(5, 5));
            mosse.add(new Move<>(action1, action2, action3, action4));
            mosse.add(new Move<>(action1, action2, action5, action6));
            mosse.add(new Move<>(action1, action7, action8, action9));
            return mosse;
        }
    }

    @Test
    @Sorted(0)
    public static void costruttore_SituazioneDamaSuSpiegazioneProgetto() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new DamaInizio();
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.value.size(), 1, "Dimensione radice errata: " + root.value.size());
        assertEquals(root.value.get(0), new Action<>(new Pos(2, 0), new Pos(4, 2)), "Azione errata");
    }

    @Test
    @Sorted(10)
    public static void costruttore_RadiceOthelloInizio() {
        Othello o = new Othello("a", "b");
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(o, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.value, Collections.EMPTY_LIST);
    }

    @Test
    @Sorted(20)
    public static void costruttore_RadiceMNKInizio() {
        MNKgame o = new MNKgame(-1, 3, 3, 3, "a", "b");
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(o, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.value, Collections.EMPTY_LIST);
    }

    @Test
    @Sorted(30)
    public static void costruttore_AlberoConUnaMossa() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                Set<Move<PieceModel<PieceModel.Species>>> moves = new HashSet<>();
                moves.add(new Move<>(new Action<>(new Pos(0, 0), new Pos(1, 1))));
                return moves;
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.children.size(), 0, "Numero di figli della radice: " + root.children.size());
        assertEquals(root.value.size(), 1, "Numero di azioni nella radice: " + root.value.size());
    }

    @Test
    @Sorted(40)
    public static void costruttore_AlberoBinarioCompletoDiAltezzaDue() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                Set<Move<PieceModel<PieceModel.Species>>> mosse = new HashSet<>();
                Action<PieceModel<PieceModel.Species>>
                        action1 = new Action<>(new Pos(2, 0), new Pos(4, 2)),
                        action2 = new Action<>(new Pos(4, 2), new Pos(2, 4)),
                        action3 = new Action<>(new Pos(2, 4), new Pos(0, 6)),
                        action4 = new Action<>(new Pos(3, 1), new Pos(3, 3), new Pos(1, 5)),
                        action5 = new Action<>(new Pos(2, 4), new Pos(0, 2)),
                        action6 = new Action<>(new Pos(3, 1), new Pos(3, 3), new Pos(1, 3)),
                        action7 = new Action<>(new Pos(4, 2), new Pos(6, 4));
                mosse.add(new Move<>(action1, action2, action3));
                mosse.add(new Move<>(action1, action2, action4));
                mosse.add(new Move<>(action1, action5, action6));
                mosse.add(new Move<>(action1, action5, action7));
                return mosse;
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.value.size(), 1, "Dimensione root");
        assertEquals(root.children.size(), 2, "Dimensione root.children");
        assertEquals(root.children.get(0).value.size(), 1, "Dimensione value del primo child");
        assertEquals(root.children.get(1).value.size(), 1, "Dimensione value del secondo child");
        assertEquals(root.children.get(0).children.size(), 2, "Dimensione children del primo elemento di root.child");
        assertEquals(root.children.get(1).children.size(), 2, "Dimensione children del secondo elemento di root.child");
    }

    @Test
    @Sorted(50)
    public static void costruttore_AlberoUnoTreDueCompleto() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                Set<Move<PieceModel<PieceModel.Species>>> mosse = new HashSet<>();
                Action<PieceModel<PieceModel.Species>>
                        action1 = new Action<>(new Pos(2, 0), new Pos(4, 2)),
                        action2 = new Action<>(new Pos(4, 2), new Pos(2, 4)),
                        action3 = new Action<>(new Pos(2, 4), new Pos(0, 6)),
                        action4 = new Action<>(new Pos(3, 1), new Pos(3, 3), new Pos(1, 5));
                mosse.add(new Move<>(action1, action2, action3));
                mosse.add(new Move<>(action1, action2, action4));
                mosse.add(new Move<>(action1, action3, action2));
                mosse.add(new Move<>(action1, action3, action4));
                mosse.add(new Move<>(action1, action4, action2));
                mosse.add(new Move<>(action1, action4, action3));
                return mosse;
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.value.size(), 1, "Dimensione root");
        assertEquals(root.children.size(), 3, "Dimensione root.children");
        assertEquals(root.children.get(0).value.size(), 1, "Dimensione value del primo child");
        assertEquals(root.children.get(1).value.size(), 1, "Dimensione value del secondo child");
        assertEquals(root.children.get(2).value.size(), 1, "Dimensione value del terzo child");
        assertEquals(root.children.get(0).children.size(), 2, "Dimensione children del primo elemento di root.child");
        assertEquals(root.children.get(1).children.size(), 2, "Dimensione children del secondo elemento di root.child");
        assertEquals(root.children.get(2).children.size(), 2, "Dimensione children del terzo elemento di root.child");
    }

    @Test
    @Sorted(60)
    public static void costruttore_AlberoRadiceConQuattroFigli() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                Set<Move<PieceModel<PieceModel.Species>>> mosse = new HashSet<>();
                Action<PieceModel<PieceModel.Species>>
                        action1 = new Action<>(new Pos(2, 0), new Pos(4, 2)),
                        action2 = new Action<>(new Pos(4, 2), new Pos(2, 4)),
                        action3 = new Action<>(new Pos(2, 4), new Pos(0, 6)),
                        action4 = new Action<>(new Pos(3, 1), new Pos(3, 3), new Pos(1, 5)),
                        action5 = new Action<>(new Pos(2, 4), new Pos(0, 2));
                mosse.add(new Move<>(action1, action2));
                mosse.add(new Move<>(action1, action3));
                mosse.add(new Move<>(action1, action4));
                mosse.add(new Move<>(action1, action5));
                return mosse;
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.value.size(), 1, "Dimensione root");
        assertEquals(root.children.size(), 4, "Dimensione root.children");
        for (int i = 0; i < 4; ++i) {
            String num = i < 2 ? (i == 0 ? "primo" : "secondo") : (i == 2 ? "terzo" : "quarto");
            assertEquals(root.children.get(0).value.size(), 1, "Dimensione value del " + num + " child");
            assertEquals(root.children.get(0).children.size(), 0, "Dimensione children del " + num + " elemento di root.child");
        }
    }

    @Test
    @Sorted(70)
    public static void costruttore_AlberoConPrefissoVuoto() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                Set<Move<PieceModel<PieceModel.Species>>> moves = new HashSet<>();
                moves.add(new Move<>(new Action<>(new Pos(1, 1), new Pos(2, 2))));
                moves.add(new Move<>(new Action<>(new Pos(0, 0), new Pos(1, 1))));
                return moves;
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        Node<List<Action<PieceModel<PieceModel.Species>>>> root = mC.getRoot();
        assertEquals(root.children.size(), 2, "Numero di figli della radice: " + root.children.size());
        assertEquals(root.value.size(), 0, "Numero di azioni nella radice: " + root.value.size());
    }

    @Test
    @Sorted(80)
    public static void costruttore_AlberoVuoto() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                return Collections.emptySet();
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        assertEquals(mC.getRoot().value, null);
        assertEquals(mC.getRoot().children, Collections.EMPTY_LIST);
    }

    @Test
    @Sorted(90)
    public static void costruttore_AlberoConUnSoloNodo() {
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                return new HashSet<>(Collections.singletonList(
                        new Move<>(new Action<PieceModel<PieceModel.Species>>(new Pos(0, 0), new Pos(1, 1))))
                );
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        assertEquals(mC.getRoot().value.size(), 1);
        assertEquals(mC.getRoot().children, Collections.EMPTY_LIST);
    }

    @Test
    @Sorted(100)
    public static void costruttore_AlberoUnNodoPerLivello() {
        Action<PieceModel<PieceModel.Species>>
                action1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                action2 = new Action<>(new Pos(1, 1), new Pos(2, 2)),
                action3 = new Action<>(new Pos(2, 2), new Pos(3, 3)),
                action4 = new Action<>(new Pos(3, 3), new Pos(4, 4)),
                action5 = new Action<>(new Pos(4, 4), new Pos(5, 5)),
                action6 = new Action<>(new Pos(5, 5), new Pos(6, 6));
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                HashSet<Move<PieceModel<PieceModel.Species>>> moves = new HashSet<>();
                moves.add(new Move<>(action1));
                moves.add(new Move<>(action1, action2, action3));
                moves.add(new Move<>(action1, action2, action3, action4, action5, action6));
                return moves;
            }
        };
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = new MoveChooserImpl<>(gR, new MCTSPlayer<>("foo", 10, false));
        assertEquals(mC.getRoot().value.size(), 1, "Dimensione effettiva: " + mC.getRoot().value.size());
        assertEquals(mC.getRoot().children.size(), 1, "Numero di figli effettivo: " + mC.getRoot().children.size());
        int n = mC.getRoot().children.get(0).value.size();
        assertEquals(n, 2, "Numero di elementi del nodo al livello 1: " + n);
        n = mC.getRoot().children.get(0).children.size();
        assertEquals(n, 1, "Numero di figli dell'unico figlio della radice: " + n);
        n = mC.getRoot().children.get(0).children.get(0).value.size();
        assertEquals(n, 3, "Numero di elementi del nodo al livello 2: " + n);
    }

    @Test
    @Sorted(120)
    public static void _internal_makeCustomGameRuler() {
        Action<PieceModel<PieceModel.Species>>
                action1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                action2 = new Action<>(new Pos(1, 1), new Pos(2, 2));
        Move<PieceModel<PieceModel.Species>>
                move1 = new Move<>(action1),
                move2 = new Move<>(action2),
                move3 = new Move<>(action1, action2);
        GameRuler<PieceModel<PieceModel.Species>> gR =
                makeCustomGameRuler(move1, move2, move3);
        assertEquals(
                gR.validMoves(),
                new HashSet<>(Arrays.asList(move1, move2, move3)),
                "GamerRuler errato"
        );
    }

    @Test
    @Sorted(130)
    public void subMove_controllaRadiceAlberoVuoto() {
        GameRuler<PieceModel<PieceModel.Species>> gR = makeCustomGameRuler();
        PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>> mC =
                new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        assertEquals(
                mC.subMove(),
                null
        );
    }

    @Test
    @Sorted(140)
    public void subMove_controllaRadiceVuotaDiAlberoNonVuoto() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a2);
        GameRuler<PieceModel<PieceModel.Species>> gR = makeCustomGameRuler(m1, m2);
        PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>> mC =
                new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        assertEquals(
                mC.subMove(),
                Optional.empty()
        );
    }

    @Test
    @Sorted(150)
    public void subMove_controllaRadiceNonVuotaDiAlberoNonVuoto() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a2),
                m2 = new Move<>(a1, a3);
        GameRuler<PieceModel<PieceModel.Species>> gR = makeCustomGameRuler(m1, m2);
        PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>> mC =
                new MoveChooserImpl<>(gR, new RandPlayer<>("a"));
        assertEquals(
                mC.subMove(),
                Optional.of(new Move<>(a1))
        );
    }

    @Test
    @Sorted(160)
    public void childrenSubMoves_alberoVuoto() {
        assertEquals(
                new MoveChooserImpl<>(makeCustomGameRuler(), new RandPlayer<>("a")).childrenSubMoves(),
                null
        );
    }

    @Test
    @Sorted(170)
    public void childrenSubMoves_alberoConUnSoloNodo() {
        assertEquals(
                new MoveChooserImpl<>(makeCustomGameRuler(new Move<>(new Action<>(new Pos(0, 0), new Pos(1, 1)))), new RandPlayer<>("a")).childrenSubMoves(),
                Collections.EMPTY_LIST
        );
    }

    @Test
    @Sorted(180)
    public void childrenSubMoves_alberoConDueNodi() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a1, a2);
        assertEquals(
                new MoveChooserImpl<>(makeCustomGameRuler(m1, m2), new RandPlayer<>("a")).childrenSubMoves(),
                Collections.singletonList(new Move<>(a2))
        );
    }

    @Test(expected = NullPointerException.class)
    @Sorted(190)
    public void select_UnaPosNull() {
        new MoveChooserImpl<>(
                makeCustomGameRuler(),
                randPlayer()
        ).select(new Pos(0, 0), null, new Pos(1, 1));
    }

    @Test(expected = NullPointerException.class)
    @Sorted(200)
    public void select_ArrayPosNull() {
        makeMoveChooser().select((Pos[]) null);
    }

    @Test(expected = IllegalArgumentException.class)
    @Sorted(210)
    public void select_NessunaPosizione() {
        new MoveChooserImpl<>(makeCustomGameRuler(), randPlayer()).select();
    }

    @Test(expected = IllegalArgumentException.class)
    @Sorted(220)
    public void select_PosizioniDuplicate() {
        new MoveChooserImpl<>(makeCustomGameRuler(), randPlayer()).select(new Pos(0, 0), new Pos(1, 1), new Pos(0, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    @Sorted(230)
    public void select_PosizioniFuoriBoard() {
        new MoveChooserImpl<>(new MNKgame(-1, 3, 3, 3, "a", "b"), randPlayer()).select(new Pos(0, 0), new Pos(50, 50), new Pos(1, 1));
    }

    @Test
    @Sorted(240)
    public void select_AlberoVuoto() {
        assertNull(new MoveChooserImpl<>(makeCustomGameRuler(), randPlayer()).select(new Pos(0, 0)));
    }

    @Test
    @Sorted(250)
    public void select_DaNodoRadiceSenzaFigli() {
        PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>
                mC = new MoveChooserImpl<>(makeCustomGameRuler(new Move<>(new Action<>(new Pos(0, 0), new Pos(1, 1)))), randPlayer());
        assertEquals(mC.select(new Pos(0, 0)), Collections.EMPTY_LIST, "1");
        assertEquals(mC.select(new Pos(1, 1)), Collections.EMPTY_LIST, "2");
        assertEquals(mC.select(new Pos(0, 0), new Pos(1, 1)), Collections.EMPTY_LIST, "3");
        assertEquals(mC.select(new Pos(2, 2)), Collections.EMPTY_LIST, "4");
    }

    @Test
    @Sorted(260)
    public void select_TestGenerale() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3)),
                a4 = new Action<>(new Pos(1, 1), new PieceModel<>(PieceModel.Species.KING, "bianco"));
        Move<PieceModel<PieceModel.Species>>
                m12 = new Move<>(a1, a2),
                m13 = new Move<>(a1, a3),
                m14 = new Move<>(a1, a4);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = new MoveChooserImpl<>(makeCustomGameRuler(m12, m13, m14), randPlayer());
        assertEquals(mC.select(new Pos(0, 0)), Collections.EMPTY_LIST, "1");
        assertEquals(
                new HashSet<>(mC.select(new Pos(1, 1))),
                new HashSet<>(Arrays.asList(new Move<>(a2), new Move<>(a4))), "2"
        );
        assertEquals(mC.select(new Pos(2, 2)), Collections.singletonList(new Move<>(a3)), "3");
        assertEquals(mC.select(new Pos(1, 1), new Pos(2, 2)), Collections.EMPTY_LIST, "4"); //Caso particolare del JUMP
        assertEquals(mC.select(new Pos(0, 0), new Pos(1, 1)), Collections.EMPTY_LIST, "5");
        assertEquals(mC.select(new Pos(1, 1), new Pos(2, 2)), Collections.EMPTY_LIST, "6");
    }

    @Test
    @Sorted(270)
    public void select_TestGenerale2() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(Board.Dir.DOWN, 3, new Pos(3, 3), new Pos(3, 4), new Pos(4, 4));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a1, a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = new MoveChooserImpl<>(makeCustomGameRuler(m1, m2), randPlayer());
        assertEquals(mC.select(new Pos(0, 0)), Collections.EMPTY_LIST, "1");
        assertEquals(mC.select(new Pos(3, 3)), Collections.EMPTY_LIST, "2");
        assertEquals(mC.select(new Pos(3, 4)), Collections.EMPTY_LIST, "3");
        assertEquals(
                mC.select(new Pos(3, 3), new Pos(3, 4), new Pos(4, 4)),
                Collections.singletonList(new Move<>(a2)),
                "4"
        );
        assertEquals(
                mC.select(new Pos(4, 4), new Pos(3, 3), new Pos(3, 4)),
                Collections.singletonList(new Move<>(a2)),
                "5"
        );
        assertEquals(
                mC.select(new Pos(4, 4), new Pos(3, 3), new Pos(3, 4), new Pos(4, 3)),
                Collections.EMPTY_LIST,
                "6"
        );
    }

    @Test
    @Sorted(280)
    public void quasiSelected_AlberoVuoto() {
        MoveChooserImpl<PieceModel<PieceModel.Species>> mC = makeMoveChooser();
        assertNull(mC.quasiSelected(), "1");
        mC.select(new Pos(0, 0));
        assertNull(mC.quasiSelected(), "2");
    }

    @Test
    @Sorted(290)
    public void quasiSelected_AlberoSoloRadice() {
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(new Move<>(new Action<>(new Pos(0, 0), new Pos(1, 1))));
        assertEquals(mC.quasiSelected(), Collections.EMPTY_LIST, "1");
        mC.select(new Pos(0, 0));
        assertEquals(mC.quasiSelected(), Collections.EMPTY_LIST, "2");
    }

    @Test
    @Sorted(300)
    public void quasiSelected_TestGenerale1() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(1, 2)),
                a3 = new Action<>(UP, 2, new Pos(1, 1), new Pos(1, 2)),
                a4 = new Action<>(Board.Dir.DOWN, 3, new Pos(4, 4), new Pos(3, 3), new Pos(3, 4)),
                a5 = new Action<>(Board.Dir.DOWN, 2, new Pos(4, 4));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a1, a2),
                m3 = new Move<>(a1, a2, a3),
                m4 = new Move<>(a1, a3),
                m5 = new Move<>(a1, a4, a5),
                m6 = new Move<>(a1, a5);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4, m5, m6);
        assertEquals(
                mC.quasiSelected().size(),
                0,
                "1"
        );
        mC.select(new Pos(1, 1));
        assertEquals(
                new HashSet<>(mC.quasiSelected()),
                new HashSet<>(Collections.singletonList(new Move<>(a3))),
                "2"
        );
        mC.select(new Pos(4, 4));
        assertEquals(
                new HashSet<>(mC.quasiSelected()),
                new HashSet<>(Collections.singletonList(new Move<>(a4, a5))),
                "3"
        );
    }

    @Test
    @Sorted(310)
    public void selectionPieces_AlberoVuoto() {
        assertNull(makeMoveChooser().selectionPieces());
    }

    @Test
    @Sorted(320)
    public void selectionPieces_CasoRemove() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2), new Pos(3, 3)),
                a3 = new Action<>(new PieceModel<>(PieceModel.Species.DISC, "bianco"), new Pos(0, 0));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a2),
                m2 = new Move<>(a1, a3);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2);
        assertEquals(mC.selectionPieces(), Collections.EMPTY_LIST, "1");
        assertEquals(
                new HashSet<>(mC.select(new Pos(1, 1), new Pos(2, 2), new Pos(3, 3))),
                new HashSet<>(Collections.singletonList(new Move<>(a2))),
                "2"
        );
        List<Object> listWithANull = new ArrayList<>();
        listWithANull.add(null);
        assertEquals(mC.selectionPieces(), listWithANull, "3");
    }

    @Test
    @Sorted(330)
    public void selectionPieces_CasoAddESwap() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "bianco"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco"),
                p3 = new PieceModel<>(PieceModel.Species.ROOK, "nero");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), p1),
                a2 = new Action<>(new Pos(0, 0), p2),
                a3 = new Action<>(new Pos(1, 1), p3),
                a4 = new Action<>(p1, new Pos(1, 1), new Pos(2, 2)),
                a5 = new Action<>(p2, new Pos(1, 1));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a2),
                m3 = new Move<>(a3),
                m4 = new Move<>(a4),
                m5 = new Move<>(a5);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4, m5);
        assertEquals(
                mC.selectionPieces(),
                Collections.EMPTY_LIST,
                "1"
        );
        assertEquals(
                new HashSet<>(mC.select(new Pos(0, 0))),
                new HashSet<>(Arrays.asList(m1, m2)),
                "2"
        );
        assertEquals(
                new HashSet<>(mC.selectionPieces()),
                new HashSet<>(Arrays.asList(p1, p2)),
                "3"
        );
        assertEquals(
                new HashSet<>(mC.select(new Pos(1, 1))),
                new HashSet<>(Arrays.asList(m3, m5)),
                "4"
        );
        assertEquals(
                mC.selectionPieces(),
                Collections.EMPTY_LIST,
                "5 "
        );
        assertEquals(
                new HashSet<>(mC.select(new Pos(1, 1), new Pos(2, 2))),
                new HashSet<>(Collections.singletonList(m4)),
                "6"
        );
        assertEquals(
                new HashSet<>(mC.selectionPieces()),
                new HashSet<>(Collections.singletonList(p1)),
                "7"
        );
    }

    @Test
    @Sorted(340)
    public void selectionPieces_controllaListaCreataExNovo() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "bianco"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), p1),
                a2 = new Action<>(new Pos(0, 0), p2);
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2);
        mC.select(new Pos(0, 0));
        assertTrue(
                mC.selectionPieces() != mC.selectionPieces(),
                "La lista non è creata ex-novo"
        );
    }

    @Test
    @Sorted(350)
    public void selectionPieces_controllaListaModificabile() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "bianco"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "nero");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), p1),
                a2 = new Action<>(new Pos(0, 0), p2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(new Move<>(a1), new Move<>(a2));
        mC.selectionPieces().add(new PieceModel<>(PieceModel.Species.KING, "nero"));
        mC.select(new Pos(0, 0));
        assertEquals(mC.selectionPieces().size(), 2);
        mC.selectionPieces().add(new PieceModel<>(PieceModel.Species.KING, "nero"));
    }

    @Test
    @Sorted(360)
    public void clearSelection() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(new Pos(4, 4));
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(new Move<>(a1), new Move<>(a1, a2), new Move<>(a1, a3));
        mC.select(new Pos(0, 0));
        assertEquals(
                mC.quasiSelected().size(),
                1,
                "1"
        );
        mC.clearSelection();
        assertEquals(
                mC.quasiSelected().size(),
                0,
                "2"
        );
    }

    @Test
    @Sorted(370)
    public void doSelection_AlberoVuoto() {
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser();
        assertNull(mC.doSelection(null));
        mC.select(new Pos(0, 0));
        assertNull(mC.doSelection(null));
        assertNull(mC.doSelection(new PieceModel<>(PieceModel.Species.KING, "bianco")));
    }

    @Test
    @Sorted(380)
    public void doSelection_CasoRemove() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)),
                a2 = new Action<>(new Pos(0, 0), new Pos(1, 1));
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(new Move<>(a1), new Move<>(a2));
        assertNull(mC.doSelection(null));
        assertEquals(mC.quasiSelected().size(), 0);
        assertEquals(mC.select(new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)), Collections.singletonList(new Move<>(a1)));
        assertEquals(mC.doSelection(null), new Move<>(a1));
        assertEquals(mC.quasiSelected(), Collections.EMPTY_LIST);
    }

    @Test
    @Sorted(390)
    public void doSelection_TestGenerale() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "bianco"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "nero");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), p1),
                a2 = new Action<>(new Pos(1, 1), p1),
                a3 = new Action<>(new Pos(1, 1), p1),
                a4 = new Action<>(new Pos(1, 1), p2),
                a5 = new Action<>(p1, new Pos(2, 2)),
                a6 = new Action<>(p2, new Pos(2, 2)),
                a7 = new Action<>(new Pos(3, 3), p1),
                a8 = new Action<>(p2, new Pos(3, 3));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a3),
                m2 = new Move<>(a1, a4),
                m3 = new Move<>(a1, a5),
                m4 = new Move<>(a1, a6),
                m5 = new Move<>(a1, a7),
                m6 = new Move<>(a1, a8),
                m7 = new Move<>(a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4, m5, m6, m7);
        assertNull(mC.doSelection(null));
        assertNull(mC.doSelection(p1));
        assertNull(mC.doSelection(p2));
        assertEquals(mC.select(new Pos(1, 1)).size(), 1);
        assertNull(mC.doSelection(null));
        assertEquals(mC.select(new Pos(1, 1)).size(), 1);
        assertNull(mC.doSelection(null));
        assertEquals(mC.doSelection(p1), m7);

        mC = makeMoveChooser(m1, m2, m3, m4, m5, m6, m7);
        assertEquals(mC.select(new Pos(0, 0)), Collections.singletonList(new Move<>(a1)));
        assertEquals(mC.doSelection(p1), new Move<>(a1));
        assertEquals(mC.quasiSelected().size(), 0, "1");
        assertEquals(mC.select(new Pos(1, 1)).size(), 2);
        assertNull(mC.doSelection(null));
        assertEquals(mC.doSelection(p1), new Move<>(a3));
        assertEquals(mC.quasiSelected(), Collections.EMPTY_LIST, "2");

        mC = makeMoveChooser(m1, m2, m3, m4, m5, m6, m7);
        mC.select(new Pos(0, 0));
        mC.doSelection(p1);
        assertEquals(mC.select(new Pos(2, 2)).size(), 2);
        assertEquals(mC.doSelection(p1), new Move<>(a5));
    }

    @Test
    @Sorted(400)
    public void jumpSelection_AlberoVuoto() {
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser();
        assertNull(mC.jumpSelection(new Pos(0, 0)));
        assertNull(mC.select(new Pos(1, 1)));
        assertNull(mC.jumpSelection(new Pos(2, 2)));
    }

    @Test
    @Sorted(410)
    public void jumpSelection_TestGenerale() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(0, 0)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3)),
                a4 = new Action<>(new Pos(2, 2), new Pos(4, 4)),
                a5 = new Action<>(new Pos(5, 5), new Pos(6, 6)),
                a6 = new Action<>(new Pos(2, 2));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a3),
                m2 = new Move<>(a1, a4),
                m3 = new Move<>(a1, a5),
                m4 = new Move<>(a1, a6),
                m5 = new Move<>(a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4, m5);
        assertEquals(mC.quasiSelected().size(), 0);
        assertNull(mC.jumpSelection(new Pos(0, 0)));
        assertNull(mC.jumpSelection(new Pos(1, 1)));
        assertNull(mC.jumpSelection(new Pos(2, 2)));
        assertEquals(mC.select(new Pos(1, 1)).size(), 1);
        assertEquals(mC.jumpSelection(new Pos(0, 0)), m5);

        mC = makeMoveChooser(m1, m2, m3, m4, m5);
        assertEquals(mC.select(new Pos(2, 2)), Collections.EMPTY_LIST, "1");
        assertEquals(mC.select(new Pos(0, 0)), Collections.singletonList(new Move<>(a1)), "2");
        assertEquals(mC.jumpSelection(new Pos(1, 1)), new Move<>(a1), "3");
        assertEquals(mC.select(new Pos(7, 7)), Collections.EMPTY_LIST, "4");
        int s = mC.select(new Pos(2, 2)).size();
        assertEquals(mC.select(new Pos(2, 2)).size(), 3, "5 - Dimensione effettiva: " + s);
        assertEquals(mC.jumpSelection(new Pos(3, 3)), new Move<>(a3), "6");
    }

    @Test
    @Sorted(420)
    public void moveSelection_AlberoVuoto() {
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser();
        assertNull(mC.moveSelection(UP, 3));
        assertNull(mC.select(new Pos(0, 0)));
        assertNull(mC.moveSelection(UP, 4));
    }

    @Test
    @Sorted(430)
    public void moveSelection_TestGenerale() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(0, 0), new Pos(2, 2)),
                a3 = new Action<>(UP, 5, new Pos(0, 0), new Pos(1, 1)),
                a4 = new Action<>(UP, 5, new Pos(0, 0), new Pos(2, 2)),
                a5 = new Action<>(UP, 5, new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)),
                a6 = new Action<>(UP, 5, new Pos(0, 0)),
                a7 = new Action<>(UP, 4, new Pos(0, 0), new Pos(1, 1)),
                a8 = new Action<>(Board.Dir.LEFT, 5, new Pos(0, 0), new Pos(1, 1));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a3, a7),
                m8 = new Move<>(a1, a3, a8),
                m2 = new Move<>(a1, a4),
                m3 = new Move<>(a1, a5),
                m4 = new Move<>(a1, a6),
                m5 = new Move<>(a1, a7),
                m6 = new Move<>(a1, a8),
                m7 = new Move<>(a2, a1);

        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4, m5, m6, m7, m8);
        assertEquals(mC.quasiSelected().size(), 0, "1 - quasiSelected.size() = " + mC.quasiSelected().size());
        assertEquals(mC.select(new Pos(0, 0)).size(), 2, "2");
        assertNull(mC.jumpSelection(new Pos(3, 3)), "3");
        assertEquals(mC.jumpSelection(new Pos(1, 1)), new Move<>(a1), "4");
        //assertEquals(mC.quasiSelected().size(), 6, "5");
        assertEquals(mC.select(new Pos(0, 0), new Pos(1, 1)).size(), 3, "6");
        assertNull(mC.moveSelection(UP, 7), "7");
        assertNull(mC.moveSelection(Board.Dir.DOWN, 5), "8");
        assertNull(mC.moveSelection(Board.Dir.DOWN, 7), "9");
        assertEquals(mC.moveSelection(UP, 5), new Move<>(a3), "10");
        assertEquals(mC.quasiSelected().size(), 0, "10");
    }

    @Test
    @Sorted(440)
    public void back_AlberoVuoto() {
        assertNull(makeMoveChooser().back());
    }

    @Test
    @Sorted(450)
    public void back_CasoRadice() {
        assertNull(makeMoveChooser(new Move<>(new Action<>(new Pos(0, 0), new Pos(1, 1)))).back());
    }

    @Test
    @Sorted(460)
    public void back_CasoTuttiAdd() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "bianco"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "nero");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), p1),
                a2 = new Action<>(new Pos(0, 0), p2),
                a3 = new Action<>(new Pos(1, 1), p1),
                a4 = new Action<>(new Pos(1, 1), p2);
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a3),
                m2 = new Move<>(a2, a4),
                m3 = new Move<>(a3),
                m4 = new Move<>(a4);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4);
        assertEquals(mC.select(new Pos(0, 0)).size(), 2, "1");
        assertEquals(mC.doSelection(p1), m1, "2");
        assertEquals(mC.quasiSelected().size(), 0, "3");
        assertEquals(
                mC.back(),
                new Move<PieceModel<PieceModel.Species>>(new Action<>(new Pos(1, 1)), new Action<>(new Pos(0, 0))),
                "4"
        );
        assertNull(mC.back(), "5");
        assertEquals(mC.quasiSelected().size(), 0, "6");
        assertEquals(mC.select(new Pos(1, 1)).size(), 2);
        assertEquals(mC.doSelection(p2), m4);
        assertEquals(
                mC.back(),
                new Move<PieceModel<PieceModel.Species>>(new Action<>(new Pos(1, 1)))
        );
        assertNull(mC.back());
        assertNull(mC.back());
    }

    @Test
    @Sorted(470)
    public void back_CasoTuttiMove() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco"),
                p3 = new PieceModel<>(PieceModel.Species.ROOK, "nero"),
                p4 = new PieceModel<>(PieceModel.Species.BISHOP, "bianco");
        Board<PieceModel<PieceModel.Species>> board = makeBoard16x16(
                p1, new Pos(0, 0),
                p2, new Pos(1, 1),
                p3, new Pos(2, 2),
                p4, new Pos(0, 1),
                p1, new Pos(1, 0)
        );
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(UP, 3, new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)),
                a2 = new Action<>(UP, 4, new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(UP_R, 5, new Pos(0, 1), new Pos(1, 0));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a3),
                m2 = new Move<>(a2),
                m3 = new Move<>(a3);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(makeBoardGameRuler(board, m1, m2, m3));
        assertEquals(mC.select(new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)).size(), 2, "1");
        assertNull(mC.moveSelection(UP_R, 5), "2");
        assertNull(mC.moveSelection(UP, 5), "3");
        assertNull(mC.moveSelection(UP_R, 4), "4");
        assertEquals(mC.moveSelection(UP, 3), m1, "5");
        Move<PieceModel<PieceModel.Species>> b1 = mC.back();
        assertNotNull(b1, "6.1");
        assertEquals(b1.kind, Move.Kind.ACTION, "6.2");
        assertEquals(b1.actions.size(), 2, "6.3 - Dimensione effettiva: " + b1.actions.size());
        assertEquals(
                b1,
                new Move<PieceModel<PieceModel.Species>>(
                        new Action<>(DOWN_L, 5, new Pos(5, 6), new Pos(6, 5)),
                        new Action<>(DOWN, 3, new Pos(0, 3), new Pos(1, 4), new Pos(2, 5))
                ),
                "6"
        );
        assertNull(mC.back());
        assertEquals(mC.quasiSelected().size(), 0);
        assertEquals(mC.select(new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)).size(), 2);
        assertEquals(mC.moveSelection(UP, 4), m2);
        assertNull(mC.moveSelection(UP, 4));
        assertEquals(
                mC.back(),
                new Move<PieceModel<PieceModel.Species>>(
                        new Action<>(DOWN, 4, new Pos(0, 4), new Pos(1, 5), new Pos(2, 6))
                )
        );
        assertNull(mC.back());
    }

    @Test
    @Sorted(480)
    public void back_CasoTuttiMove2() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero");
        Board<PieceModel<PieceModel.Species>> b1 = makeBoard16x16(
                p1, new Pos(0, 0),
                p1, new Pos(1, 1),
                p1, new Pos(2, 2)
        );
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(UP, 3, new Pos(0, 0)),
                a2 = new Action<>(UP_R, 4, new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(RIGHT, 5, new Pos(0, 3), new Pos(5, 5), new Pos(6, 6));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a1, a2),
                m3 = new Move<>(a1, a2, a3),
                m4 = new Move<>(a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(makeBoardGameRuler(b1, m1, m2, m3, m4));
        for (int i = 0; i < 3; ++i) {
            assertEquals(mC.select(new Pos(0, 0)).size(), 1, "1");
            assertEquals(mC.quasiSelected().size(), 0, "2");
            assertEquals(mC.moveSelection(UP, 3), m1, "3");
            assertEquals(mC.select(new Pos(1, 1), new Pos(2, 2)).size(), 1, "4");
            assertEquals(mC.moveSelection(UP_R, 4), new Move<>(a2), "5");
            assertEquals(mC.select(new Pos(0, 3), new Pos(5, 5), new Pos(6, 6)).size(), 1, "6");
            assertEquals(mC.moveSelection(RIGHT, 5), new Move<>(a3), "7");
            assertNull(mC.moveSelection(RIGHT, 5), "8");
            assertEquals(
                    mC.back(),
                    new Move<PieceModel<PieceModel.Species>>(new Action<>(
                            LEFT, 5, new Pos(5, 3), new Pos(10, 5), new Pos(11, 6)
                    )),
                    "9"
            );
            assertEquals(
                    mC.back(),
                    new Move<PieceModel<PieceModel.Species>>(new Action<>(
                            DOWN_L, 4, new Pos(5, 5), new Pos(6, 6)
                    )),
                    "10"
            );
            assertEquals(
                    mC.back(),
                    new Move<PieceModel<PieceModel.Species>>(new Action<>(DOWN, 3, new Pos(0, 3))),
                    "11"
            );
            assertNull(mC.back());
        }
    }

    @Test
    @Sorted(490)
    public void back_CasoTuttiJump() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero");
        Board<PieceModel<PieceModel.Species>> board = makeBoard16x16(
                p1, new Pos(0, 0),
                p1, new Pos(6, 6),
                p1, new Pos(2, 2)
        );
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(6, 6), new Pos(7, 7)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3)),
                a4 = new Action<>(new Pos(3, 3), new Pos(4, 4)),
                a5 = new Action<>(new Pos(1, 1), new Pos(5, 5));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a3, a4),
                m2 = new Move<>(a1, a3, a5),
                m3 = new Move<>(a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(makeBoardGameRuler(board, m1, m2, m3));
        //assertEquals(mC.quasiSelected().size(), 2, "1");
        assertEquals(mC.select(new Pos(6, 6)), Collections.singletonList(m3), "2");
        assertNull(mC.back(), "3");
        assertEquals(mC.jumpSelection(new Pos(7, 7)), m3, "4");
        assertEquals(mC.back(), new Move<>(new Action<>(new Pos(7, 7), new Pos(6, 6))), "5");
        assertNull(mC.back(), "6");
        assertEquals(mC.select(new Pos(0, 0)), Collections.singletonList(new Move<>(a1, a3)), "7");
        assertEquals(mC.jumpSelection(new Pos(1, 1)), new Move<>(a1, a3), "8");
        assertEquals(mC.select(new Pos(3, 3)), Collections.singletonList(new Move<>(a4)), "9");
        assertEquals(mC.jumpSelection(new Pos(4, 4)), new Move<>(a4), "10");
        //assertEquals(mC.quasiSelected().size(), 0);
        assertEquals(mC.back(), new Move<PieceModel<PieceModel.Species>>(new Action<>(new Pos(4, 4), new Pos(3, 3))), "12");
        assertEquals(
                mC.back(),
                new Move<PieceModel<PieceModel.Species>>(
                        new Action<>(new Pos(3, 3), new Pos(2, 2)),
                        new Action<>(new Pos(1, 1), new Pos(0, 0))
                ),
                "13"
        );
        assertNull(mC.back());
    }

    @Test
    @Sorted(500)
    public void back_CasoTuttiRemove() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "bianco"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "nero"),
                p3 = new PieceModel<>(PieceModel.Species.PAWN, "bianco"),
                p4 = new PieceModel<>(PieceModel.Species.ROOK, "nero");
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), p1),
                a2 = new Action<>(new Pos(1, 1), p2),
                a3 = new Action<>(new Pos(2, 2), p3),
                a4 = new Action<>(new Pos(3, 3), p4),
                a5 = new Action<>(new Pos(0, 0)),
                a6 = new Action<>(new Pos(1, 1), new Pos(2, 2), new Pos(3, 3));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a2, a3, a4, a5),
                m2 = new Move<>(a1, a2, a3, a4, a6);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2);
        mC.select(new Pos(0, 0));
        mC.doSelection(null);
        assertEquals(mC.back(), new Move<>(a1), "1");
        mC.select(new Pos(1, 1), new Pos(2, 2), new Pos(3, 3));
        assertNotNull(mC.doSelection(null), "2");
        Move<PieceModel<PieceModel.Species>> back = mC.back();
        assertNotNull(back, "3.1");
        assertEquals(back.actions.size(), 3, "3.2 - Numero di azioni: " + back.actions.size());
        assertEquals(back, new Move<>(a2, a3, a4), "3");
        assertNull(mC.back(), "4");
    }

    @Test
    @Sorted(510)
    public void back_CasoTuttiRemove2() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco"),
                p3 = new PieceModel<>(PieceModel.Species.BISHOP, "rosso"),
                p4 = new PieceModel<>(PieceModel.Species.ROOK, "blu"),
                p5 = new PieceModel<>(PieceModel.Species.KNIGHT, "giallo"),
                p6 = new PieceModel<>(PieceModel.Species.PAWN, "verde");
        Board<PieceModel<PieceModel.Species>> board = makeBoard16x16(
                p1, new Pos(0, 0),
                p2, new Pos(1, 1),
                p3, new Pos(2, 2),
                p4, new Pos(3, 3),
                p5, new Pos(4, 4),
                p6, new Pos(5, 5)
        );
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos[]{new Pos(0, 0)}),
                a2 = new Action<>(new Pos[]{new Pos(1, 1), new Pos(2, 2)}),
                a3 = new Action<>(new Pos[]{new Pos(3, 3), new Pos(4, 4), new Pos(5, 5)}),
                ra1 = new Action<>(new Pos(0, 0), p1),
                ra2_1 = new Action<>(new Pos(1, 1), p2),
                ra2_2 = new Action<>(new Pos(2, 2), p3),
                ra3_1 = new Action<>(new Pos(3, 3), p4),
                ra3_2 = new Action<>(new Pos(4, 4), p5),
                ra3_3 = new Action<>(new Pos(5, 5), p6);
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1),
                m2 = new Move<>(a2),
                m3 = new Move<>(a3),
                rm1 = new Move<>(ra1),
                rm2 = new Move<>(ra2_1, ra2_2),
                rm3 = new Move<>(ra3_1, ra3_2, ra3_3);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(makeBoardGameRuler(board, m1, m2, m3));
        mC.select(new Pos(0, 0));
        mC.doSelection(null);
        assertEquals(mC.back(), rm1, "Mossa inversa 1");
        mC.select(new Pos(1, 1), new Pos(2, 2));
        mC.doSelection(null);
        assertEquals(mC.back(), rm2, "Mossa inversa 2");
        mC.select(new Pos(3, 3), new Pos(4, 4), new Pos(5, 5));
        mC.doSelection(null);
        assertEquals(mC.back(), rm3, "Mossa inversa 3");
    }

    @Test
    @Sorted(520)
    public void back_CasoTuttiSwap() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco"),
                p3 = new PieceModel<>(PieceModel.Species.BISHOP, "rosso"),
                p4 = new PieceModel<>(PieceModel.Species.ROOK, "blu"),
                p5 = new PieceModel<>(PieceModel.Species.KNIGHT, "giallo"),
                p6 = new PieceModel<>(PieceModel.Species.PAWN, "verde");
        Board<PieceModel<PieceModel.Species>> board = makeBoard16x16(
                p1, new Pos(0, 0),
                p2, new Pos(1, 1),
                p3, new Pos(2, 2),
                p4, new Pos(3, 3),
                p5, new Pos(4, 4),
                p6, new Pos(5, 5)
        );
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(p1, new Pos(1, 1)),
                a2 = new Action<>(p5, new Pos(2, 2), new Pos(3, 3)),
                a3 = new Action<>(p3, new Pos(4, 4), new Pos(5, 5), new Pos(0, 0)),
                a0_1 = new Action<>(p4, new Pos(0, 0), new Pos(1, 1), new Pos(2, 2)),
                a0_2 = new Action<>(p2, new Pos(3, 3), new Pos(4, 4), new Pos(5, 5));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a0_1, a1),
                m2 = new Move<>(a0_1, a2),
                m3 = new Move<>(a0_1, a3),
                m4 = new Move<>(a0_2, a1),
                m5 = new Move<>(a0_2, a2),
                m6 = new Move<>(a0_2, a3);
        GameRuler<PieceModel<PieceModel.Species>>
                gR = makeBoardGameRuler(board, m1, m2, m3, m4, m5, m6);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(gR);
        mC.select(new Pos(0, 0), new Pos(1, 1), new Pos(2, 2));
        assertNotNull(mC.doSelection(p4));
        mC.select(new Pos(1, 1));
        assertNotNull(mC.doSelection(p1));
        assertEquals(mC.back(), new Move<>(new Action<>(p4, new Pos(1, 1))), "1");
        mC.select(new Pos(2, 2), new Pos(3, 3));
        assertNotNull(mC.doSelection(p5));
        assertEquals(
                mC.back(),
                new Move<>(
                        new Action<>(p4, new Pos(2, 2)),
                        new Action<>(p4, new Pos(3, 3))
                ),
                "2"
        );
        mC.select(new Pos(4, 4), new Pos(5, 5), new Pos(0, 0));
        assertNotNull(mC.doSelection(p3));
        assertEquals(
                mC.back(),
                new Move<>(
                        new Action<>(p5, new Pos(4, 4)),
                        new Action<>(p6, new Pos(5, 5)),
                        new Action<>(p4, new Pos(0, 0))
                ),
                "3"
        );
        assertEquals(
                mC.back(),
                new Move<>(
                        new Action<>(p1, new Pos(0, 0)),
                        new Action<>(p2, new Pos(1, 1)),
                        new Action<>(p3, new Pos(2, 2))
                ),
                "4"
        );
    }

    @Test
    @Sorted(530)
    public void back_testGenerale() {
        PieceModel<PieceModel.Species>
                p1 = new PieceModel<>(PieceModel.Species.KING, "nero"),
                p2 = new PieceModel<>(PieceModel.Species.QUEEN, "bianco"),
                p3 = new PieceModel<>(PieceModel.Species.BISHOP, "rosso"),
                p4 = new PieceModel<>(PieceModel.Species.ROOK, "blu"),
                p5 = new PieceModel<>(PieceModel.Species.KNIGHT, "giallo"),
                p6 = new PieceModel<>(PieceModel.Species.PAWN, "verde");
        Board<PieceModel<PieceModel.Species>> board = makeBoard16x16(
                p1, new Pos(0, 0),
                p2, new Pos(1, 1),
                p2, new Pos(1, 2),
                p3, new Pos(2, 2),
                p4, new Pos(3, 3),
                p5, new Pos(4, 4),
                p6, new Pos(5, 5)
        );
        Action<PieceModel<PieceModel.Species>>
                //Move
                a1 = new Action<>(UP, 2, new Pos(0, 0), new Pos(2, 2)),
                a2 = new Action<>(RIGHT, 3, new Pos(3, 3)),
                //Jump
                a3 = new Action<>(new Pos(6, 3), new Pos(8, 3)),
                //Add
                a4 = new Action<>(new Pos(8, 0), p6),
                a5 = new Action<>(new Pos(3, 3), p1),
                //Remove
                a6 = new Action<>(new Pos(3, 3)),
                //Swap
                a7 = new Action<>(p2, new Pos(8, 3), new Pos(8, 0)),
                //Azioni di base dell'albero
                a0_1 = new Action<>(new Pos(0, 8), p1),
                a0_2 = new Action<>(new Pos(0, 8), p2);
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a0_1, a1, a2, a3, a4, a5, a6, a7),
                m2 = new Move<>(a0_1, a2),
                m3 = new Move<>(a0_2);
        GameRuler<PieceModel<PieceModel.Species>>
                gR = makeBoardGameRuler(board, m1, m2, m3);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(gR);
        assertEquals(
                new HashSet<>(mC.select(new Pos(0, 8))),
                new HashSet<>(Arrays.asList(new Move<>(a0_1), new Move<>(a0_2))),
                "1"
        );
        assertEquals(
                new HashSet<>(mC.selectionPieces()),
                new HashSet<>(Arrays.asList(p1, p2)),
                "2"
        );
        mC.doSelection(p1);
        mC.select(new Pos(0, 0), new Pos(2, 2));
        assertNotNull(mC.moveSelection(UP, 2), "3");
        Move<PieceModel<PieceModel.Species>> backMove = mC.back();
        assertNotNull(backMove, "4.1");
        List<Action<PieceModel<PieceModel.Species>>> backActions = backMove.actions;
        assertEquals(backActions.size(), 8, "4.2 - Dimensione: " + backActions.size());
        assertEquals(backActions.get(0), new Action<>(p4, new Pos(8, 3)), "4.3");
        assertEquals(backActions.get(1), new Action<>(p6, new Pos(8, 0)), "4.4");
        assertEquals(
                backActions,
                Arrays.asList(
                        new Action<>(p4, new Pos(8, 3)),
                        new Action<>(p6, new Pos(8, 0)),
                        a5,
                        a6,
                        new Action<PieceModel<PieceModel.Species>>(new Pos(8, 0)),
                        new Action<PieceModel<PieceModel.Species>>(new Pos(8, 3), new Pos(6, 3)),
                        new Action<PieceModel<PieceModel.Species>>(LEFT, 3, new Pos(6, 3)),
                        new Action<PieceModel<PieceModel.Species>>(DOWN, 2, new Pos(0, 2), new Pos(2, 4))
                ),
                "4"
        );
    }

    @Test(expected = IllegalStateException.class)
    @Sorted(540)
    public void isFinal_AlberoVuoto() {
        makeMoveChooser().isFinal();
    }

    @Test
    @Sorted(550)
    public void isFinal_TestGenerale() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3)),
                a4 = new Action<>(new Pos(3, 3), new Pos(4, 4));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a2, a3, a4),
                m2 = new Move<>(a1, a2, a3, a1),
                m3 = new Move<>(a1, a2),
                m4 = new Move<>(a2);
        MoveChooserImpl<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(m1, m2, m3, m4);
        assertFalse(mC.isFinal());
        mC.select(new Pos(1, 1));
        mC.jumpSelection(new Pos(2, 2));
        assertTrue(mC.isFinal());
        mC = makeMoveChooser(m1, m2, m3, m4);
        mC.select(new Pos(0, 0));
        mC.jumpSelection(new Pos(1, 1));
        assertTrue(mC.isFinal());
        mC.select(new Pos(3, 3));
        mC.jumpSelection(new Pos(4, 4));
        assertTrue(mC.isFinal());
    }

    @Test
    @Sorted(560)
    public void move_SenzaTempoLimite() {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a2),
                m2 = new Move<>(a1, a3),
                m3 = new Move<>(a2);
        GameRuler<PieceModel<PieceModel.Species>>
                gR = makeCustomGameRuler(m1, m2, m3);
        assertEquals(gR.mechanics().time, -1, "Tempo per la mossa in mechanics: " + gR.mechanics().time);
        Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>>
                master = mC -> {
            Thread t = new Thread(() -> {
                mC.select(new Pos(0, 0));
                mC.jumpSelection(new Pos(1, 1));
                mC.select(new Pos(1, 1));
                assertEquals(mC.jumpSelection(new Pos(2, 2)), m3);
                assertTrue(mC.isFinal());
                mC.move();
                assertTrue(((MoveChooserImpl) mC).isMoveChosen());
            });
            t.setDaemon(true);
            t.start();
        };
        PlayerGUI<PieceModel<PieceModel.Species>>
                p = new PlayerGUI<>("a", master);
        p.setGame(gR);
        assertEquals(p.getMove(), m1);
    }

    @Test
    @Sorted(570)
    public void move_ConTempoLimite() {
        GameRuler<PieceModel<PieceModel.Species>> gR = _moveTest_timedGameRuler(1000);
        Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>>
                master = mC -> {
            Thread t = new Thread(() -> {
                mC.select(new Pos(0, 0));
                mC.jumpSelection(new Pos(1, 1));
                mC.select(new Pos(1, 1));
                mC.jumpSelection(new Pos(2, 2));
                mC.move();
            });
            t.setDaemon(true);
            t.start();
        };
        PlayerGUI<PieceModel<PieceModel.Species>>
                p = new PlayerGUI<>("a", master);
        p.setGame(gR);
        assertEquals(
                p.getMove(),
                new Move<PieceModel<PieceModel.Species>>(
                        new Action<>(new Pos(0, 0), new Pos(1, 1)),
                        new Action<>(new Pos(1, 1), new Pos(2, 2))
                )
        );
    }

    @Test
    @Sorted(580)
    public void move_FuoriTempoLimite() {
        GameRuler<PieceModel<PieceModel.Species>> gR = _moveTest_timedGameRuler(100);
        Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>>
                master = _moveTest_waiterMaster(2000);
        PlayerGUI<PieceModel<PieceModel.Species>>
                p = new PlayerGUI<>("a", master);
        p.setGame(gR);
        assertEquals(p.getMove(), new Move(Move.Kind.RESIGN));
    }

    @Test
    @Sorted(590)
    public void move_ThreadInterrotto() {
        class Holder {
            Object o;
            public void set(Object o) {this.o = o;}
            public Object get() {return this.o;}
        }
        GameRuler<PieceModel<PieceModel.Species>> gR = _moveTest_timedGameRuler(500);
        Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>>
                master = _moveTest_waiterMaster(2000);
        PlayerGUI<PieceModel<PieceModel.Species>>
                p = new PlayerGUI<>("a", master);
        p.setGame(gR);
        Holder h = new Holder();
        h.set(new Object());
        Thread t = new Thread(()->{
            h.set(p.getMove());
        });
        t.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {}
        t.interrupt();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {}
        assertNull(h.get());
    }

    @Test
    @Sorted(600)
    public void mayPass_PassDisponibile() {
        PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(new Move<>(Move.Kind.PASS));
        assertTrue(mC.mayPass());
    }

    @Test
    @Sorted(610)
    public void mayPass_PassNonDisponibile() {
        PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>
                mC = makeMoveChooser(new Move<>(new Action<>(new Pos(0,0), new Pos(1,1))));
        assertFalse(mC.mayPass());
    }

    @Test(expected = IllegalStateException.class)
    @Sorted(620)
    public void pass_MossaNonValida() {
        makeMoveChooser(new Move<>(new Action<>(new Pos(0,0), new Pos(1,1)))).pass();
    }

    @Test
    @Sorted(630)
    public void pass_MossaValida() {
        Move m = new Move(Move.Kind.PASS);
        MoveChooserImpl mC = makeMoveChooser(m);
        mC.pass();
        assertEquals(mC.getMove(), new Move<>(Move.Kind.PASS));
    }

    @Test
    @Sorted(640)
    public void resign() {
        Move m = new Move(Move.Kind.RESIGN);
        MoveChooserImpl mC = makeMoveChooser(m);
        mC.resign();
        assertEquals(mC.getMove(), m);
    }

    //todo testare gli IllegalStateException

    private GameRuler<PieceModel<PieceModel.Species>> _moveTest_timedGameRuler(long timePerMoveMillis) {
        Action<PieceModel<PieceModel.Species>>
                a1 = new Action<>(new Pos(0, 0), new Pos(1, 1)),
                a2 = new Action<>(new Pos(1, 1), new Pos(2, 2)),
                a3 = new Action<>(new Pos(2, 2), new Pos(3, 3));
        Move<PieceModel<PieceModel.Species>>
                m1 = new Move<>(a1, a2),
                m2 = new Move<>(a1, a3),
                m3 = new Move<>(a2);
        GameRuler<PieceModel<PieceModel.Species>> gR = new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            Board<PieceModel<PieceModel.Species>> b = new BoardOct<>(16, 16);

            @Override
            public List<String> players() {
                return Arrays.asList("a", "b");
            }

            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                return new HashSet<>(Arrays.asList(m1, m2, m3));
            }

            @Override
            public Board<PieceModel<PieceModel.Species>> getBoard() {
                return Utils.UnmodifiableBoard(b);
            }

            @Override
            public Mechanics<PieceModel<PieceModel.Species>> mechanics() {
                return new Mechanics<>(timePerMoveMillis, Collections.emptyList(), b.positions(), 2, null, null);
            }
        };
        return gR;
    }

    private Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>> _moveTest_waiterMaster(long waitMillis) {
        return mC -> {
            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(waitMillis);
                } catch (InterruptedException e) {
                }
            });
            t.setDaemon(true);
            t.start();
        };
    }

    /**
     * Ritorna un GameRuler con mosse valide sempre, tutte e solo quelle specificate.
     *
     * @param moves Le mosse valide del GameRuler
     * @return Il nuovo GameRuler
     */
    @SafeVarargs
    private static GameRuler<PieceModel<PieceModel.Species>> makeCustomGameRuler(
            Move<PieceModel<PieceModel.Species>>... moves
    ) {
        return makeBoardGameRuler(new BoardOct<>(10, 10), moves);
    }

    @SafeVarargs
    private static GameRuler<PieceModel<PieceModel.Species>> makeBoardGameRuler(
            Board<PieceModel<PieceModel.Species>> board,
            Move<PieceModel<PieceModel.Species>>... moves
    ) {
        return new GameRulerAdapter<PieceModel<PieceModel.Species>>() {
            Board<PieceModel<PieceModel.Species>> _board = board;

            @Override
            public List<String> players() {
                return Arrays.asList("a", "b");
            }

            @Override
            public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
                List<Move<PieceModel<PieceModel.Species>>>
                        movesList = new ArrayList<>(Arrays.asList(moves));
                return new HashSet<>(movesList);
            }

            @Override
            public Board<PieceModel<PieceModel.Species>> getBoard() {
                return Utils.UnmodifiableBoard(_board);
            }

            @Override
            public Mechanics<PieceModel<PieceModel.Species>> mechanics() {
                return new Mechanics<>(-1, Collections.emptyList(), getBoard().positions(), 2, null, null);
            }
        };
    }

    /**
     * @param pieces_and_positions PieceModel (indici pari) e posizioni (indici dispari)
     * @return Una BoardOct 10x10 con i pezzi specificati
     */
    @SuppressWarnings("unchecked")
    private static Board<PieceModel<PieceModel.Species>> makeBoard16x16(Object... pieces_and_positions) {
        BoardOct<PieceModel<PieceModel.Species>> board = new BoardOct<>(16, 16);

        for (int i = 0; i < pieces_and_positions.length; i += 2) {
            board.put(
                    (PieceModel<PieceModel.Species>) (pieces_and_positions[i]),
                    (Pos) (pieces_and_positions[i + 1])
            );
        }

        return board;
    }

    private static Player<PieceModel<PieceModel.Species>> randPlayer() {
        return new RandPlayer<>("a");
    }

    private static MoveChooserImpl<PieceModel<PieceModel.Species>> makeMoveChooser(
            GameRuler<PieceModel<PieceModel.Species>> gR
    ) {
        return new MoveChooserImpl<>(gR, new Object());
    }

    @SafeVarargs
    private static MoveChooserImpl<PieceModel<PieceModel.Species>> makeMoveChooser(
            Move<PieceModel<PieceModel.Species>>... moves
    ) {
        return new MoveChooserImpl<>(makeCustomGameRuler(moves), new Object());
    }

    /**
     * Stampa un albero di mosse in cui tutte le azioni sono JUMP
     *
     * @param root La radice dell'albero da stampare
     */
    private static void printJumpMoveTree(Node<List<Action<PieceModel<PieceModel.Species>>>> root) {
        int l = 0;
        Set<Node<List<Action<PieceModel<PieceModel.Species>>>>> nodes = new HashSet<>(), next = new HashSet<>();
        nodes.add(root);
        while (nodes.size() > 0) {
            System.out.println("Inizio livello " + l++ + " - Nodi nel livello: " + nodes.size());
            for (Node<List<Action<PieceModel<PieceModel.Species>>>> node : nodes) {
                System.out.println("Inizio nodo");
                for (Action<PieceModel<PieceModel.Species>> a : node.value) {
                    System.out.println("---------");
                    for (Pos pos : a.pos) {
                        System.out.print("(" + pos.b + " - " + pos.t + ")");
                    }
                    System.out.println();
                    System.out.println("---------");
                }
                next.addAll(node.children);
            }
            nodes.clear();
            nodes.addAll(next);
            next.clear();
        }
    }
}

package gapp.ulg.games;

import gapp.ulg.game.GameFactory;
import gapp.ulg.game.Param;
import gapp.ulg.game.board.GameRuler;
import gapp.ulg.game.board.PieceModel;
import gapp.ulg.game.util.ConcreteParameter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MyBreakthroughFactory implements GameFactory<GameRuler<PieceModel<PieceModel.Species>>> {
    private String p1, p2;
    private List<Param<?>> params;

    public MyBreakthroughFactory() {
        Param<String> param1 = new ConcreteParameter<>(
                "Time",
                "Time limit for a move",
                new String[] {"No limit","1s","2s","3s","5s","10s","20s","30s","1m","2m","5m"},
                "No limit"
        );
        Param<Integer> param2 = new ConcreteParameter<>(
                "M",
                "Board width",
                new Integer[] {6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},
                8
        );
        Param<Integer> param3 = new ConcreteParameter<>(
                "N",
                "Board height",
                new Integer[] {6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},
                8
        );
        params = Collections.unmodifiableList(Arrays.asList(param1, param2, param3));
    }

    @Override
    public String name() {
        return "Breakthrough (progetto)";
    }

    @Override
    public int minPlayers() {
        return 2;
    }

    @Override
    public int maxPlayers() {
        return 2;
    }

    @Override
    public List<Param<?>> params() {
        return params;
    }

    @Override
    public void setPlayerNames(String... names) {
        for (String name : names) {
            if (name == null) {
                throw new NullPointerException();
            }
        }
        if (names.length == 2) {
            p1 = names[0];
            p2 = names[1];
        } else {
            throw new IllegalArgumentException("There must be exactly 2 players");
        }
    }

    @Override
    public GameRuler<PieceModel<PieceModel.Species>> newGame() {
        if (p1 == null ||  p2 == null) {
            throw new IllegalStateException("Nomi giocatori non impostati");
        }

        //todo tempo
        return new MyBreakthrough(getTimeMillis(), (Integer)params.get(1).get(), (Integer)params.get(2).get(), p1, p2);
    }

    /**
     * Restituisce il valore, espresso in millisecondi, del tempo permesso per eseguire dalla mossa,
     * ricavato dal valore corrente dell'apposito {@link Param}.
     *
     * @return Il tempo in millisecondi per poter eseguire la mossa
     */
    private long getTimeMillis() {
        String timeParam = (String)params.get(0).get();
        switch (timeParam) {
            case "No limit": return -1;
            case "1s": return 1000;
            case "2s": return 2000;
            case "3s": return 3000;
            case "5s": return 5000;
            case "10s": return 10_000;
            case "20s": return 20_000;
            case "30s": return 30_000;
            case "1m": return 60_000;
            case "2m": return 120_000;
            case "5m": return 300_000;
            default: throw new IllegalStateException("Valore del Param Time non valido: " + timeParam);
        }
    }
}

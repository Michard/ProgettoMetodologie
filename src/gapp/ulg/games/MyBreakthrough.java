package gapp.ulg.games;

import gapp.ulg.game.board.*;
import gapp.ulg.game.util.BoardOct;
import gapp.ulg.game.util.Utils;

import java.util.*;

import static gapp.ulg.game.board.Board.Dir.*;

public class MyBreakthrough implements GameRuler<PieceModel<PieceModel.Species>> {
    // Pezzi del gioco
    private static final PieceModel<PieceModel.Species> BLACK_PIECE = new PieceModel<>(PieceModel.Species.PAWN, "nero");
    private static final PieceModel<PieceModel.Species> WHITE_PIECE = new PieceModel<>(PieceModel.Species.PAWN, "bianco");

    private long time; // Tempo per effettuare la mossa
    private BoardOct<PieceModel<PieceModel.Species>> board;
    private Board<PieceModel<PieceModel.Species>> boardView;
    private String p1Name, p2Name;
    private int gameTurn; // A fine partita, è il risultato
    private boolean isGameEnded; // Se il gioco è finito
    private Set<Move<PieceModel<PieceModel.Species>>> currentPlayerValidMoves;
    /**
     * Storico delle mosse.
     *
     * In Breakthrough è sufficiente la lista delle mosse eseguite per eseguire l'unMove:
     * il turno passa da un giocatore all'altro e non può accadere che il turno venga passato.
     *
     * Per l'implementazione corrente, in caso di gioco finito per raggiungimento dell'obiettivo da parte di un
     * giocatore, il turno di questo resta in gameTurn, quindi anche in questo caso non c'è bisogno di salvarlo.
     *
     * In caso di mossa invalida o resign, infine, si risolve la situazione tramite il flag isResign.
     */
    private List<Move<PieceModel<PieceModel.Species>>> moveHistory;
    private boolean isResign = false;

    public MyBreakthrough(long time, int boardWidth, int boardHeight, String p1, String p2) {
        Objects.requireNonNull(p1);
        Objects.requireNonNull(p2);
        this.time = time;
        if (boardWidth < 6 || boardWidth > 20 || boardHeight < 6 || boardHeight > 20) {
            throw new IllegalArgumentException("Dimensioni non valide");
        }
        board = new BoardOct<>(boardWidth, boardHeight);
        boardView = Utils.UnmodifiableBoard(board);
        p1Name = p1;
        p2Name = p2;
        gameTurn = 1;
        isGameEnded = false;
        currentPlayerValidMoves = new HashSet<>();
        board.put(WHITE_PIECE, new Pos(0, 0), Board.Dir.RIGHT, board.width());
        board.put(WHITE_PIECE, new Pos(0, 1), Board.Dir.RIGHT, board.width());
        board.put(BLACK_PIECE, new Pos(0, board.height() - 1), Board.Dir.RIGHT, board.width());
        board.put(BLACK_PIECE, new Pos(0, board.height() - 2), Board.Dir.RIGHT, board.width());
        computeCurrentPlayerValidMoves();
        moveHistory = new ArrayList<>();
        isResign = false;
    }

    @Override
    public String name() {
        return "Breakthrough " + board.width() + "x" + board.width();
    }

    @Override
    public <T> T getParam(String name, Class<T> c) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(c);
        switch (name) {
            case "Board width":
                return c.cast(board.width());
            case "Board height":
                return c.cast(board.height());
            default:
                throw new IllegalArgumentException("Nome parametro non valido");
        }
    }

    @Override
    public List<String> players() {
        return Collections.unmodifiableList(Arrays.asList(p1Name, p2Name));
    }

    @Override
    public String color(String name) {
        Objects.requireNonNull(name);
        if (name.equals(p1Name)) {
            return "bianco";
        } else if (name.equals(p2Name)) {
            return "nero";
        } else {
            throw new IllegalArgumentException("Nome giocatore non valido");
        }
    }

    @Override
    public Board<PieceModel<PieceModel.Species>> getBoard() {
        return boardView;
    }

    @Override
    public int turn() {
        return isGameEnded ? 0 : gameTurn;
    }

    @Override
    public boolean move(Move<PieceModel<PieceModel.Species>> m) {
        Objects.requireNonNull(m);
        if (isGameEnded) {
            throw new IllegalStateException("Gioco terminato");
        }
        if (isValid(m)) {
            //Aggiunta della mossa a moveHistory
            moveHistory.add(m);

            //Effettuazione della mossa
            m.actions.forEach(a -> {
                if (a.kind == Action.Kind.REMOVE) {
                    board.remove(a.pos.get(0)); //In Breakthrough, si elimina massimo un pezzo per turno
                } else { //Per forza move
                    board.put(board.remove(a.pos.get(0)), board.adjacent(a.pos.get(0), a.dir)); //Si muove solo un pezzo
                }
            });

            //Calcolo fine gioco
            PieceModel<PieceModel.Species> enemyPiece = gameTurn == 1 ? BLACK_PIECE : WHITE_PIECE;
            if (board.get(enemyPiece).size() == 0) { // Tutti i pezzi nemici sono stati mangiati
                isGameEnded = true;
                return true;
            } else if (m.actions.get(m.actions.size() - 1).pos.get(0).t == (gameTurn == 1 ? board.height() - 2 : 1)) { // Se il pezzo appena mosso è arrivato alla fine
                isGameEnded = true;
                return true;
            }
            gameTurn = gameTurn == 1 ? 2 : 1;
            computeCurrentPlayerValidMoves();
            return true;
        } else {
            gameTurn = gameTurn == 1 ? 2 : 1;
            isGameEnded = true;
            isResign = true;
            return false;
        }
    }

    @Override
    public boolean unMove() {
        if (isResign) { // Mossa invalida o resign
            isResign = false;
            gameTurn = gameTurn == 1 ? 2 : 1;
            isGameEnded = false;
            computeCurrentPlayerValidMoves();
            return true;
        } else if (moveHistory.size() > 0) { // Dopo l'inizio della partita
            if (!isGameEnded) {
                gameTurn = gameTurn == 1 ? 2 : 1;
            }
            Move<PieceModel<PieceModel.Species>> lastMove = moveHistory.remove(moveHistory.size()-1);
            Action<PieceModel<PieceModel.Species>> moveAction = lastMove.actions.get(lastMove.actions.size()-1); //Ultima azione
            Pos startPos = moveAction.pos.get(0);
            Pos endPos = board.adjacent(startPos, moveAction.dir);
            board.put(board.remove(endPos), startPos);
            if (lastMove.actions.size() == 2) { // Se c'è stato anche il remove
                board.put(gameTurn == 1 ? BLACK_PIECE : WHITE_PIECE, endPos);
            }
            isGameEnded = false;
            computeCurrentPlayerValidMoves();
            return true;
        } else { // Inizio partita
            return false;
        }
    }

    @Override
    public boolean isPlaying(int i) {
        if (i == 1 || i == 2) {
            return !isGameEnded;
        } else {
            throw new IllegalArgumentException("Indice non valido");
        }
    }

    @Override
    public int result() {
        return isGameEnded ? gameTurn : -1;
    }

    @Override
    public Set<Move<PieceModel<PieceModel.Species>>> validMoves() {
        if (result() != -1) {
            throw new IllegalStateException("Gioco terminato");
        }

        return currentPlayerValidMoves;
    }

    private void computeCurrentPlayerValidMoves() {
        if (isGameEnded) currentPlayerValidMoves = Collections.emptySet();
        currentPlayerValidMoves.clear();
        PieceModel<PieceModel.Species> playerPiece = gameTurn == 1 ? WHITE_PIECE : BLACK_PIECE;
        PieceModel<PieceModel.Species> enemyPiece = gameTurn == 1 ? BLACK_PIECE : WHITE_PIECE;
        Set<Pos> playerPositions = board.get(playerPiece);
        playerPositions.forEach(pos -> {
            Board.Dir adjDirA = gameTurn == 1 ? Board.Dir.UP_L : Board.Dir.DOWN_L;
            Board.Dir adjDirB = gameTurn == 1 ? Board.Dir.UP : DOWN;
            Board.Dir adjDirC = gameTurn == 1 ? Board.Dir.UP_R : Board.Dir.DOWN_R;
            Pos adjacentA = board.adjacent(pos, adjDirA);
            Pos adjacentB = board.adjacent(pos, adjDirB);
            Pos adjacentC = board.adjacent(pos, adjDirC);
            //Prima diagonale
            if (adjacentA != null) {
                if (enemyPiece.equals(board.get(adjacentA))) {
                    currentPlayerValidMoves.add(new Move<>(
                            new Action<>(adjacentA),
                            new Action<>(adjDirA, 1, pos)
                    ));
                } else if (board.get(adjacentA) == null) {
                    currentPlayerValidMoves.add(new Move<>(
                            new Action<>(adjDirA, 1, pos)
                    ));
                }
            }
            //Frontale
            if (adjacentB != null && board.get(adjacentB) == null) {
                currentPlayerValidMoves.add(new Move<>(new Action<>(adjDirB, 1, pos)));
            }
            //Seconda diagonale
            if (adjacentC != null) {
                if (enemyPiece.equals(board.get(adjacentC))) {
                    currentPlayerValidMoves.add(new Move<>(
                            new Action<>(adjacentC),
                            new Action<>(adjDirC, 1, pos)
                    ));
                } else if (board.get(adjacentC) == null) {
                    currentPlayerValidMoves.add(new Move<>(
                            new Action<>(adjDirC, 1, pos)
                    ));
                }
            }
        });
        //Resign
        currentPlayerValidMoves.add(new Move<>(Move.Kind.RESIGN));
    }

    @Override
    public GameRuler<PieceModel<PieceModel.Species>> copy() {
        MyBreakthrough cpy = new MyBreakthrough(time, board.width(), board.height(), p1Name, p2Name);
        for (Pos p : this.board.positions()) {
            PieceModel<PieceModel.Species> piece = this.board.get(p);
            if (piece != null) {
                cpy.board.put(this.board.get(p), p);
            }
        }
        cpy.gameTurn = this.gameTurn;
        cpy.isGameEnded = this.isGameEnded;
        cpy.computeCurrentPlayerValidMoves();
        cpy.moveHistory.clear();
        cpy.moveHistory.addAll(this.moveHistory);
        return cpy;
    }

    @Override
    public Mechanics<PieceModel<PieceModel.Species>> mechanics() {
        Map<Pos, PieceModel<PieceModel.Species>> startPieces = new HashMap<>();
        for (int x = 0; x < board.width(); ++x) {
            startPieces.put(new Pos(x, 0), WHITE_PIECE);
            startPieces.put(new Pos(x, 1), WHITE_PIECE);
            startPieces.put(new Pos(x, board.height() - 1), BLACK_PIECE);
            startPieces.put(new Pos(x, board.height() - 2), BLACK_PIECE);
        }
        return new Mechanics<>(
                time,
                Collections.unmodifiableList(Arrays.asList(WHITE_PIECE, BLACK_PIECE)),
                boardView.positions(),
                2,
                new Situation<>(startPieces, 1),
                getNext()
        );
    }

    /**
     * Ritorna l'oggetto {@link Next} associato a questo GameRuler
     *
     * @return il Next di questo GameRuler
     */
    private Next<PieceModel<PieceModel.Species>> getNext() {
        return situation -> {
            Objects.requireNonNull(situation);
            MyBreakthrough game = new MyBreakthrough(
                    time,
                    board.width(),
                    board.height(),
                    p1Name,
                    p2Name
            );
            game.board.positions().forEach(pos -> {
                if (game.board.get(pos) != null) {
                    game.board.remove(pos);
                }
            });
            situation.newMap().forEach((pos, piece) -> game.board.put(piece, pos));
            game.isGameEnded = situation.turn <= 0;
            game.gameTurn = situation.turn > 0 ? situation.turn : 0;
            if (game.isGameEnded) {
                return Collections.emptyMap();
            }
            game.computeCurrentPlayerValidMoves();
            Map<Move<PieceModel<PieceModel.Species>>, Situation<PieceModel<PieceModel.Species>>>
                    nextSituations = new HashMap<>();
            Set<Move<PieceModel<PieceModel.Species>>> validMoves = new HashSet<>();
            validMoves.addAll(currentPlayerValidMoves);
            validMoves.forEach(move -> {
                if (move.kind != Move.Kind.RESIGN) {
                    game.move(move);
                    nextSituations.put(move, game.toSituation());
                    game.unMove();
                }
            });
            return nextSituations;
        };
    }
}

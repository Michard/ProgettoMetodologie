package gapp.ulg.games;

import gapp.ulg.game.board.*;
import gapp.ulg.game.util.BoardOct;
import gapp.ulg.game.util.Utils;

import java.util.*;

import static gapp.ulg.game.board.PieceModel.Species;

/**
 * Un oggetto {@code MNKgame} rappresenta un GameRuler per fare una partita a un
 * (m,n,k)-game, generalizzazioni del ben conosciuto Tris o Tic Tac Toe.
 * <br>
 * Un gioco (m,n,k)-game si gioca su una board di tipo {@link Board.System#OCTAGONAL}
 * di larghezza (width) m e altezza (height) n. Si gioca con pezzi o pedine di specie
 * {@link Species#DISC} di due colori "nero" e "bianco". All'inizio la board è vuota.
 * Poi a turno ogni giocatore pone una sua pedina in una posizione vuota. Vince il
 * primo giocatore che riesce a disporre almeno k delle sue pedine in una linea di
 * posizioni consecutive orizzontale, verticale o diagonale. Chiaramente non è
 * possibile passare il turno e una partita può finire con una patta.
 * <br>
 * Per ulteriori informazioni si può consultare
 * <a href="https://en.wikipedia.org/wiki/M,n,k-game">(m,n,k)-game</a>
 */
public class MNKgame implements GameRuler<PieceModel<Species>> {
    private static final PieceModel<Species> whitePiece = new PieceModel<>(Species.DISC, "bianco");
    private static final PieceModel<Species> blackPiece = new PieceModel<>(Species.DISC, "nero");

    private List<Pos> history; // Storico delle posizioni

    private long allowedTimePerMove; // Tempo permesso per mossa
    private int boardWidth, boardHeight; // Dimensioni board
    private int lineLengthToWin; //Lunghezza della linea per vincere

    private List<String> players; // Nomi dei gicatori
    public int currentPlayerIndex; // { 0, 1 }
    private Set<Move<PieceModel<PieceModel.Species>>> currentPlayerValidMoves; // Mosse valide nella situazione attuale
    private Board<PieceModel<Species>> board; // Board di gioco
    private Board<PieceModel<Species>> boardView; // View della board
    private boolean gameEnded; // Se la partita è terminata
    private boolean invalidMovePlayed; // Se è stata giocata una mossa non valida
    private int result; //Valore ritornato da result()

    private Mechanics<PieceModel<Species>> mechanics; // Meccanica del gioco

    /**
     * Crea un {@code MNKgame} con le impostazioni date.
     *
     * @param time tempo in millisecondi per fare una mossa, se &lt;= 0 significa nessun
     *             limite
     * @param m    larghezza (width) della board
     * @param n    altezza (height) della board
     * @param k    lunghezza della linea
     * @param p1   il nome del primo giocatore
     * @param p2   il nome del secondo giocatore
     * @throws NullPointerException     se {@code p1} o {@code p2} è null
     * @throws IllegalArgumentException se i valori di {@code m,n,k} non soddisfano
     *                                  le condizioni 1 &lt;= {@code k} &lt;= max{{@code M,N}} &lt;= 20 e 1 &lt;= min{{@code M,N}}
     */
    public MNKgame(long time, int m, int n, int k, String p1, String p2) {
        if (p1 != null && p2 != null) {
            if (1 <= k && k <= Math.max(m, n) && Math.max(m, n) <= 20 && 1 <= Math.min(m, n)) {
                allowedTimePerMove = time;
                boardWidth = m;
                boardHeight = n;
                lineLengthToWin = k;
                players = Collections.unmodifiableList(Arrays.asList(p1, p2));
                board = new BoardOct<>(boardWidth, boardHeight);
                boardView = Utils.UnmodifiableBoard(board);
                currentPlayerIndex = 0;
                currentPlayerValidMoves = currentPlayerValidMoves();
                gameEnded = false;
                invalidMovePlayed = false;
                result = -1;
                history = new ArrayList<>();
                mechanics = makeMechanics();
            } else {
                throw new IllegalArgumentException();
            }
        } else {
            throw new NullPointerException();
        }
    }

    /**
     * Il nome rispetta il formato:
     * <pre>
     *     <i>M,N,K</i>-game
     * </pre>
     * dove <code><i>M,N,K</i></code> sono i valori dei parametri M,N,K, ad es.
     * "4,5,4-game".
     */
    @Override
    public String name() {
        return boardWidth + "," + boardHeight + "," + lineLengthToWin + "-game";
    }

    @Override
    public <T> T getParam(String name, Class<T> c) {
        // c.cast(Object) lancia ClassCastException se il cast fallisce
        if (name != null && c != null) {
            switch (name) {
                case "Time":
                    return c.cast(timeToParamString());
                case "M":
                    return c.cast(boardWidth);
                case "N":
                    return c.cast(boardHeight);
                case "K":
                    return c.cast(lineLengthToWin);
                default:
                    throw new IllegalArgumentException("Invalid parameter name");
            }
        } else throw new NullPointerException("null value passed to getParam");
    }

    /**
     * Ricava il valore del parametro Time a partire dal numero di millisecondi concessi.
     * @return Il valore del parametro Time.
     */
    private String timeToParamString() {
        if (allowedTimePerMove <= 0) return "No limit";
        if (allowedTimePerMove == 1000) return "1s";
        if (allowedTimePerMove == 2000) return "2s";
        if (allowedTimePerMove == 3000) return "3s";
        if (allowedTimePerMove == 5000) return "5s";
        if (allowedTimePerMove == 10_000) return "10s";
        if (allowedTimePerMove == 20_000) return "20s";
        if (allowedTimePerMove == 30_000) return "30s";
        if (allowedTimePerMove == 1_000 * 60) return "1m";
        if (allowedTimePerMove == 1_000 * 60 * 2) return "2m";
        if (allowedTimePerMove == 1_000 * 60 * 5) return "5m";
        else throw new IllegalStateException("Invalid value for Time parameter - millis: " + allowedTimePerMove);
    }

    @Override
    public List<String> players() {
        return players;
    }

    /**
     * @return il colore "nero" per il primo giocatore e "bianco" per il secondo
     */
    @Override
    public String color(String name) {
        if (name != null) {
            if (name.equals(players().get(0))) {
                return "nero";
            } else if (name.equals(players().get(1))) {
                return "bianco";
            } else {
                throw new IllegalArgumentException(name + " is not a player in this game");
            }
        } else {
            throw new NullPointerException("name in Othello#color(String name) is null");
        }
    }

    @Override
    public Board<PieceModel<Species>> getBoard() {
        return boardView;
    }

    @Override
    public int turn() {
        return gameEnded ? 0 : currentPlayerIndex + 1;
    }

    /**
     * Se la mossa non è valida termina il gioco dando la vittoria all'altro
     * giocatore.
     * Se dopo la mossa la situazione è tale che nessuno dei due giocatori può
     * vincere, si tratta quindi di una situazione che può portare solamente a una
     * patta, termina immediatamente il gioco con una patta. Per determinare se si
     * trova in una tale situazione controlla che nessun dei due giocatori può
     * produrre una linea di K pedine con le mosse rimanenti (in qualsiasi modo siano
     * disposte le pedine rimanenti di entrambi i giocatori).
     */
    @Override
    public boolean move(Move<PieceModel<Species>> m) {
        if (m != null) {
            if (!gameEnded) {
                if (validMoves().contains(m) && m.kind != Move.Kind.RESIGN) {
                    PieceModel<PieceModel.Species> currentPlayerPiece =
                            currentPlayerIndex == 0 ? blackPiece : whitePiece;
                    Pos pos = m.actions.get(0).pos.get(0);
                    board.put(currentPlayerPiece, pos);
                    history.add(pos);
                    if (currentPlayerWins()) {
                        gameEnded = true;
                        result = currentPlayerIndex + 1;
                    } else if (isForcedTie()) {
                        gameEnded = true;
                        result = 0;
                    } else {
                        currentPlayerIndex = 1 - currentPlayerIndex;
                        currentPlayerValidMoves = currentPlayerValidMoves();
                        return true;
                    }
                    currentPlayerIndex = 1 - currentPlayerIndex;
                    return true;
                } else {
                    gameEnded = true;
                    invalidMovePlayed = true;
                    currentPlayerIndex = 1 - currentPlayerIndex;
                    return m.kind.equals(Move.Kind.RESIGN); //Resign e la mossa invalida causano la terminazione del gioco, ma solo resign è valida
                }
            } else {
                throw new IllegalStateException("Cannot play a move in a finished game");
            }
        } else {
            throw new NullPointerException("Cannot execute a null move");
        }
    }

    /**
     * Ritorna true se il giocatore di turno ha disposto una combinazione vincente.
     * @return Se il giocatore corrente vince.
     */
    private boolean currentPlayerWins() {
        PieceModel<PieceModel.Species> currentPlayerPiece =
                currentPlayerIndex == 0 ? blackPiece : whitePiece;
        Set<Pos> ownedByCurrentPlayer = board.get(currentPlayerPiece);
        for (Pos p : ownedByCurrentPlayer) {
            if (isPartOfCompleteLine(p)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Ritorna true se la posizione è parte di una linea completa, tale cioè che il giocatore
     * che ha inserito un pezzo nella posizione abbia quindi vinto la partita.
     * @param p Posizione da controllare
     * @return Se è parte di una linea ininterrotta di pezzi di lunghezza K.
     */
    private boolean isPartOfCompleteLine(Pos p) {
        PieceModel<PieceModel.Species> piece = board.get(p);
        for (Board.Dir dir : Board.Dir.values()) {
            int consecutives = 0;
            Pos new_p = p;
            do {
                consecutives++;
                new_p = board.adjacent(new_p, dir);
            } while (new_p != null && piece.equals(board.get(new_p)));
            if (consecutives >= lineLengthToWin) {
                return true;
            }
        }
        return false;
    }

    /**
     * Ritorna true se è impossibile che continuando a giocare uno dei due giocatori
     * vincaa la partita.
     * @return Se la partita è un pareggio forzato.
     */
    private boolean isForcedTie() {
        return CantWin(0) && CantWin(1);
    }

    /**
     * Ritorna true se per il prossimo giocatore, di indice di turnazione {@code nextPlayerIndex + 1},
     * non può più vincere.
     *
     * @param nextPlayerIndex L'indice di turnazione, meno 1, del prossimo giocatore
     * @return Se il giocatore non può vincere
     */
    private boolean CantWin(int nextPlayerIndex) {
        //Cond ? floor(num di celle vuote) : ceil(num di celle vute)
        int emptyPieces = boardWidth * boardHeight - board.get().size();
        int availablePieces =
                currentPlayerIndex == nextPlayerIndex ?
                        (emptyPieces) / 2 :
                        (emptyPieces + 2 - 1) / 2;
        //Pezzo da inserire
        PieceModel<PieceModel.Species> nextPiece =
                nextPlayerIndex == 0 ? blackPiece : whitePiece;
        //Posizioni non controllate dall'avversario (di questo giocatore o nulle)
        Set<Pos> notTakenByOpponent = new HashSet<>();
        board.positions().forEach(pos -> {
            if (board.get(pos) == null || board.get(pos).equals(nextPiece)) {
                notTakenByOpponent.add(pos);
            }
        });

        //Controlla
        for (Pos p : notTakenByOpponent) {
            for (Board.Dir dir : Board.Dir.values()) {
                Pos new_p = p;
                int rowLength = 0;
                int piecesStillAvailable = availablePieces;

                while (notTakenByOpponent.contains(new_p)) {
                    if (board.get(new_p) == null) piecesStillAvailable--;
                    if (piecesStillAvailable < 0) break;
                    rowLength++;
                    new_p = board.adjacent(new_p, dir);
                }

                if (rowLength >= lineLengthToWin) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean unMove() {
        if (history.size() > 0) {
            board.remove(history.remove(history.size() - 1));
            currentPlayerIndex = 1 - currentPlayerIndex;
            gameEnded = false;
            invalidMovePlayed = false;
            currentPlayerValidMoves = currentPlayerValidMoves();
            result = -1;
            return true;
        } else return false;
    }

    @Override
    public boolean isPlaying(int i) {
        if (i == 1 || i == 2) {
            return !gameEnded;
        } else {
            throw new IllegalArgumentException("1 and 2 are the only valid player indices");
        }
    }

    @Override
    public int result() {
        if (gameEnded) {
            if (!invalidMovePlayed) {
                return result;
            } else {
                return currentPlayerIndex + 1;
            }
        } else {
            return -1;
        }
    }

    /**
     * Ogni mossa (diversa dall'abbandono) è rappresentata da una sola {@link Action}
     * di tipo {@link Action.Kind#ADD}.
     */
    @Override
    public Set<Move<PieceModel<Species>>> validMoves() {
        if (result() == -1) {
            return currentPlayerValidMoves;
        } else {
            throw new IllegalStateException("A finished game has no valid moves");
        }
    }

    /**
     * Crea e ritorna un Set contenente le mosse valide per questo gioco nella situazione corrente.
     * Si assume che il gioco non sia terminato.
     * @return Le mosse valide per la situazione di gioco attuale
     */
    private Set<Move<PieceModel<Species>>> currentPlayerValidMoves() {
        Set<Move<PieceModel<Species>>> moves = new HashSet<>();
        moves.add(new Move<>(Move.Kind.RESIGN));
        PieceModel<PieceModel.Species> playerPiece = currentPlayerIndex == 0 ? blackPiece : whitePiece;
        for (Pos p : board.positions()) {
            if (board.get(p) == null) {
                moves.add(new Move<>(new Action<>(p, playerPiece)));
            }
        }
        return Collections.unmodifiableSet(moves);
    }

    @Override
    public GameRuler<PieceModel<Species>> copy() {
        MNKgame newGame = new MNKgame(
                this.allowedTimePerMove,
                this.boardWidth,
                this.boardHeight,
                this.lineLengthToWin,
                this.players().get(0),
                this.players().get(1)
        );
        newGame.history.addAll(this.history);
        newGame.currentPlayerIndex = this.currentPlayerIndex;
        newGame.currentPlayerValidMoves = this.currentPlayerValidMoves;
        for (Pos p : this.board.get()) {
            newGame.board.put(this.board.get(p), p);
        }
        newGame.gameEnded = this.gameEnded;
        newGame.invalidMovePlayed = this.invalidMovePlayed;
        newGame.result = this.result;
        return newGame;
    }

    @Override
    public Mechanics<PieceModel<Species>> mechanics() {
        return mechanics;
    }

    /**
     * Crea e ritorna un oggetto {@link gapp.ulg.game.board.GameRuler.Mechanics}
     * per questo gioco
     * @return La meccanica di questo gioco
     */
    private Mechanics<PieceModel<Species>> makeMechanics() {
        return new Mechanics<>(
                this.allowedTimePerMove,
                Collections.unmodifiableList(Arrays.asList(blackPiece, whitePiece)),
                this.board.positions(),
                2,
                new Situation<>(new HashMap<>(), 1),
                getNext()
        );
    }

    /**
     * Crea e ritorna un oggetto {@link gapp.ulg.game.board.GameRuler.Next} per questo gioco.
     * @return Il {@code Next} di questo gioco.
     */
    private Next<PieceModel<PieceModel.Species>> getNext() {
        return situation -> {
            Objects.requireNonNull(situation);
            MNKgame game = new MNKgame(
                    this.allowedTimePerMove,
                    this.boardWidth,
                    this.boardHeight,
                    this.lineLengthToWin,
                    players.get(0),
                    players.get(1)
            );
            situation.newMap().forEach((pos, piece) -> {
                game.board.put(piece, pos);
            });
            game.gameEnded = situation.turn <= 0;
            game.currentPlayerIndex = situation.turn - 1;
            if (game.gameEnded /*|| game.currentPlayerWins()*/) {
                return Collections.EMPTY_MAP;
            }
            game.currentPlayerValidMoves = game.currentPlayerValidMoves();
            Set<Move<PieceModel<Species>>> validMoves = game.validMoves();
            Map<Move<PieceModel<PieceModel.Species>>, Situation<PieceModel<PieceModel.Species>>> nextSituations =
                    new HashMap<>();
            validMoves.forEach((move) -> {
                if (move.kind != Move.Kind.RESIGN) {
                    game.move(move);
                    nextSituations.put(move, game.toSituation());
                    game.unMove();
                }
            });
            return nextSituations;
        };
    }
}

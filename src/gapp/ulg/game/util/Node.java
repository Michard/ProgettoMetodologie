package gapp.ulg.game.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Classe che rappresenta un nodo di un albero
 * @param <T> Il tipo del valore associato al nodo
 */
public class Node<T> {
    /**
     * Valore associato al nodo
     */
    public T value;
    /**
     * Genitore di questo nodo, oppure null se questo nodo
     * non ha un genitore
     */
    public Node<T> parent;
    /**
     * Lista dei nodi figli di questo nodo
     */
    public List<Node<T>> children;

    /**
     * Crea un nodo rappresentante un albero vuoto,
     * con valore null e senza genitori né figli
     */
    public Node() {
        this(null, null);
    }

    /**
     * Crea un nodo senza figli (da impostare manualmente tramite
     * l'accesso a {@code children}).
     * @param value Il valore associato al nodo
     * @param parent Il nodo genitore di questo nodo, oppure null
     *               se questo nodo non ha un genitore
     */
    public Node(T value, Node<T> parent) {
        this.value = value;
        this.parent = parent;
        children = new ArrayList<>();
    }
}
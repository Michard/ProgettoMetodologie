package gapp.ulg.game.util;

/**
 * Un oggetto {@code SharedReference} è usato per condividere
 * l'accesso a un riferimento, invece che a un oggetto, tra più
 * thread, supportando quindi non solo le operazioni di chiamata
 * di metodi ma anche quella di assegnamento alla variabile riferimento
 * attraverso l'uso dei metodi {@code get} e {@code set}.
 *
 * La classe è anche utilizzabile per usare variabili non final né
 * effettivamente final all'interno di espressioni lambda.
 *
 * @param <R> Tipo della variabile
 */
public class SharedReference<R> {
    private R value;

    public SharedReference() {
        this(null);
    }

    public SharedReference(R value) {
        this.value = value;
    }

    public void set(R value) {
        this.value = value;
    }

    public R get() {
        return value;
    }
}

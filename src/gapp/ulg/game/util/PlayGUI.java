package gapp.ulg.game.util;

import gapp.ulg.game.GameFactory;
import gapp.ulg.game.Param;
import gapp.ulg.game.PlayerFactory;
import gapp.ulg.game.board.GameRuler;
import gapp.ulg.game.board.Move;
import gapp.ulg.game.board.PieceModel;
import gapp.ulg.game.board.Player;
import gapp.ulg.games.GameFactories;
import gapp.ulg.play.PlayerFactories;

import static gapp.gui.GuiConstants.*;
import static gapp.ulg.game.util.PlayerGUI.MoveChooser;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Un {@code PlayGUI} è un oggetto che facilita la gestione di partite in una
 * applicazione controllata da GUI. Un {@code PlayGUI} segue lo svolgimento di una
 * partita dalla scelta della {@link GameFactory} e dei {@link PlayerFactory} e di
 * tutte le mosse fino alla fine naturale della partita o alla sua interruzione.
 * Inoltre, un {@code PlayGUI} aiuta sia a mantenere la reattività della GUI che a
 * garantire la thread-safeness usando un thread di confinamento per le invocazioni
 * di tutti i metodi e costruttori degli oggetti coinvolti in una partita.
 *
 * @param <P> tipo del modello dei pezzi
 */
public class PlayGUI<P> {
    /**
     * Un {@code Observer} è un oggetto che osserva lo svolgimento di una o più
     * partite. Lo scopo principale è di aggiornare la GUI che visualizza la board
     * ed eventuali altre informazioni a seguito dell'inizio di una nuova partita e
     * di ogni mossa eseguita.
     *
     * @param <P> tipo del modello dei pezzi
     */
    public interface Observer<P> {
        /**
         * Comunica allo {@code Observer} il gioco (la partita) che sta iniziando.
         * Può essere nello stato iniziale o in uno stato diverso, ad es. se la
         * partita era stata sospesa ed ora viene ripresa. L'oggetto {@code g} è
         * una copia del {@link GameRuler} ufficiale del gioco. Lo {@code Observer}
         * può usare e modificare {@code g} a piacimento senza che questo abbia
         * effetto sul {@link GameRuler} ufficiale. In particolare lo {@code Observer}
         * può usare {@code g} per mantenersi sincronizzato con lo stato del gioco
         * riportando in {@code g} le mosse dei giocatori, vedi
         * {@link Observer#moved(int, Move)}. L'uso di {@code g} dovrebbe avvenire
         * solamente nel thread in cui il metodo è invocato.
         * <br>
         * <b>Il metodo non blocca, non usa altri thread e ritorna velocemente.</b>
         *
         * @param g un gioco, cioè una partita
         * @throws NullPointerException se {@code g} è null
         */
        void setGame(GameRuler<P> g);

        /**
         * Comunica allo {@code Observer} la mossa eseguita da un giocatore. Lo
         * {@code Observer} dovrebbe usare tale informazione per aggiornare la sua
         * copia del {@link GameRuler}. L'uso del GameRuler dovrebbe avvenire
         * solamente nel thread in cui il metodo è invocato.
         * <br>
         * <b>Il metodo non blocca, non usa altri thread e ritorna velocemente.</b>
         *
         * @param i indice di turnazione di un giocatore
         * @param m la mossa eseguita dal giocatore
         * @throws IllegalStateException    se non c'è un gioco impostato o c'è ma è
         *                                  terminato.
         * @throws NullPointerException     se {@code m} è null
         * @throws IllegalArgumentException se {@code i} non è l'indice di turnazione
         *                                  di un giocatore o {@code m} non è una mossa valida nell'attuale situazione
         *                                  di gioco
         */
        void moved(int i, Move<P> m);

        /**
         * Comunica allo {@code Observer} che il giocatore con indice di turnazione
         * {@code i} ha violato un vincolo sull'esecuzione (ad es. il tempo concesso
         * per una mossa). Dopo questa invocazione il giocatore {@code i} è
         * squalificato e ciò produce gli stessi effetti che si avrebbero se tale
         * giocatore si fosse arreso. Quindi lo {@code Observer} per sincronizzare
         * la sua copia con la partita esegue un {@link Move.Kind#RESIGN} per il
         * giocatore {@code i}. L'uso del GameRuler dovrebbe avvenire solamente nel
         * thread in cui il metodo è invocato.
         *
         * @param i   indice di turnazione di un giocatore
         * @param msg un messaggio che descrive il tipo di violazione
         * @throws NullPointerException     se {@code msg} è null
         * @throws IllegalArgumentException se {@code i} non è l'indice di turnazione
         *                                  di un giocatore
         */
        void limitBreak(int i, String msg);

        /**
         * Comunica allo {@code Observer} che la partita è stata interrotta. Ad es.
         * è stato invocato il metodo {@link PlayGUI#stop()}.
         *
         * @param msg una stringa con una descrizione dell'interruzione
         */
        void interrupted(String msg);
    }

    private PlayGUI.Observer<P> obs;
    private long maxBlockTime;

    private GameFactory<GameRuler<PieceModel<PieceModel.Species>>> gF;
    private GameRuler<P> gR;
    private Map<Integer, PlayerSlot<P>> playerSlots; // Slot giocatori (vedi classe PlayerSlot)
    private final AtomicBoolean playing; // Se c'è una partita in corso

    private ExecutorService containmentThread; // Thread di confinamento
    private ExecutorService controllerThread; // Thread controllore

    private TimeoutController timeoutController = new TimeoutController();

    /**
     * Variabile che indica se, dopo l'esecuzione del play, è stato richiamato setGameFactory.
     * Se non è stato fatto, gli altri setter e getter non sono utilizzabili.
     */
    private AtomicBoolean restarted;

    /**
     * Crea un oggetto {@link PlayGUI} per partite controllate da GUI. L'oggetto
     * {@code PlayGUI} può essere usato per giocare più partite anche con giochi e
     * giocatori diversi. Per garantire che tutti gli oggetti coinvolti
     * {@link GameFactory}, {@link PlayerFactory}, {@link GameRuler} e {@link Player}
     * possano essere usati tranquillamente anche se non sono thread-safe, crea un
     * thread che chiamiamo <i>thread di confinamento</i>, in cui invoca tutti i
     * metodi e costruttori di tali oggetti. Il thread di confinamento può cambiare
     * solo se tutti gli oggetti coinvolti in una partita sono creati ex novo. Se
     * durante una partita un'invocazione (ad es. a {@link Player#getMove()}) blocca
     * il thread di confinamento per un tempo superiore a {@code maxBlockTime}, la
     * partita è interrotta.
     * <br>
     * All'inizio e durante una partita invoca i metodi di {@code obs}, rispettando
     * le specifiche di {@link Observer}, sempre nel thread di confinamento.
     * <br>
     * <b>Tutti i thread usati sono daemon thread</b>
     *
     * @param obs          un osservatore del gioco
     * @param maxBlockTime tempo massimo in millisecondi di attesa per un blocco
     *                     del thread di confinamento, se &lt; 0, significa nessun
     *                     limite di tempo
     * @throws NullPointerException se {@code obs} è null
     */
    public PlayGUI(Observer<P> obs, long maxBlockTime) {
        this.obs = Objects.requireNonNull(obs);
        this.maxBlockTime = maxBlockTime;

        gF = null;
        playerSlots = new HashMap<>();
        playing = new AtomicBoolean(false);
        restarted = new AtomicBoolean(false);
    }

    /**
     * Imposta la {@link GameFactory} con il nome dato. Usa {@link GameFactories}
     * per creare la GameFactory nel thread di confinamento. Se già c'era una
     * GameFactory impostata, la sostituisce con la nuova e se c'erano anche
     * PlayerFactory impostate le cancella. Però se c'è una partita in corso,
     * fallisce.
     *
     * @param name nome di una GameFactory
     * @throws NullPointerException     se {@code name} è null
     * @throws IllegalArgumentException se {@code name} non è il nome di una
     *                                  GameFactory
     * @throws IllegalStateException    se la creazione della GameFactory fallisce o se
     *                                  c'è una partita in corso.
     */
    public void setGameFactory(String name) {
        Objects.requireNonNull(name);
        if (!Arrays.asList(GameFactories.availableBoardFactories()).contains(name))
            throw new IllegalArgumentException(name + PGUI_EXC_NOT_A_GAMEFACTORY);
        if (playing.get())
            throw new IllegalStateException(PGUI_EXC_ALREADY_PLAYING);

        if (containmentThread != null && !containmentThread.isShutdown()) {
            containmentThread.shutdown();
        }
        containmentThread = Executors.newSingleThreadExecutor(runnable -> {
            Thread t = Executors.defaultThreadFactory().newThread(runnable);
            t.setName("Thread di confinamento");
            t.setDaemon(true);
            return t;
        });

        if (controllerThread != null && !controllerThread.isShutdown()) {
            controllerThread.shutdown();
        }
        controllerThread = Executors.newSingleThreadExecutor(runnable -> {
            Thread t = Executors.defaultThreadFactory().newThread(runnable);
            t.setName("Thread controllore");
            t.setDaemon(true);
            return t;
        });

        try {
            containmentThread.submit(() ->
                    gF = GameFactories.getBoardFactory(name)
            ).get();
            playerSlots.clear();
            restarted.set(true);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Ritorna i nomi dei parametri della {@link GameFactory} impostata. Se la
     * GameFactory non ha parametri, ritorna un array vuoto.
     *
     * @return i nomi dei parametri della GameFactory impostata
     * @throws IllegalStateException se non c'è una GameFactory impostata
     */
    public String[] getGameFactoryParams() {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            return containmentThread.submit(() -> {

                if (gF == null)
                    throw new IllegalStateException(PGUI_EXC_GAMEFACTORY_NOT_SET);
                return gF.params().stream().map(Param::name).collect(Collectors.toList()).toArray(new String[0]);

            }).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Ritorna il prompt del parametro con il nome specificato della
     * {@link GameFactory} impostata.
     *
     * @param paramName nome del parametro
     * @return il prompt del parametro con il nome specificato della GameFactory
     * impostata.
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la GameFactory impostata non ha un
     *                                  parametro di nome {@code paramName}
     * @throws IllegalStateException    se non c'è una GameFactory impostata
     */
    public String getGameFactoryParamPrompt(String paramName) {
        return getGameFactoryParamInfo(paramName, Param::prompt);
    }

    /**
     * Ritorna i valori ammissibili per il parametro con nome dato della
     * {@link GameFactory} impostata.
     *
     * @param paramName nome del parametro
     * @return i valori ammissibili per il parametro della GameFactory impostata
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la GameFactory impostata non ha un
     *                                  parametro di nome {@code paramName}
     * @throws IllegalStateException    se non c'è una GameFactory impostata
     */
    public Object[] getGameFactoryParamValues(String paramName) {
        return getGameFactoryParamInfo(paramName, p -> p.values().toArray());
    }

    /**
     * Ritorna il valore del parametro di nome dato della {@link GameFactory}
     * impostata.
     *
     * @param paramName nome del parametro
     * @return il valore del parametro della GameFactory impostata
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la GameFactory impostata non ha un
     *                                  parametro di nome {@code paramName}
     * @throws IllegalStateException    se non c'è una GameFactory impostata
     */
    public Object getGameFactoryParamValue(String paramName) {
        return getGameFactoryParamInfo(paramName, Param::get);
    }

    /**
     * Ritorna il valore ottenuto applicando {@code map} al parametro
     * di nome {@code paramName}.
     *
     * @param paramName Il nome del parametro
     * @param map       La funzione a applicare al parametro
     * @param <T>       Tipo del valore da ritornare
     * @return Il valore della funzione applicata al parametro
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la GameFactory impostata non ha un
     *                                  parametro di nome {@code paramName}
     * @throws IllegalStateException    se non c'è una GameFactory impostata
     */
    private <T> T getGameFactoryParamInfo(String paramName, Function<Param<?>, T> map) {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            Future<T> f = containmentThread.submit(() -> {

                Objects.requireNonNull(paramName);
                Objects.requireNonNull(map);
                if (gF == null)
                    throw new IllegalStateException(PGUI_EXC_GAMEFACTORY_NOT_SET);
                for (Param<?> param : gF.params()) {
                    if (param.name().equals(paramName)) {
                        return map.apply(param);
                    }
                }
                throw new IllegalArgumentException(paramName + PGUI_EXC_NOT_A_GAMEFACTORY_PARAMETER);

            });
            return f.get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NullPointerException) {
                throw (NullPointerException) cause;
            } else if (cause instanceof IllegalArgumentException) {
                throw (IllegalArgumentException) cause;
            } else if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Imposta il valore del parametro di nome dato della {@link GameFactory}
     * impostata.
     *
     * @param paramName nome del parametro
     * @param value     un valore ammissibile per il parametro
     * @throws NullPointerException     se {@code paramName} o {@code value} è null
     * @throws IllegalArgumentException se la GameFactory impostata non ha un
     *                                  parametro di nome {@code paramName} o {@code value} non è un valore
     *                                  ammissibile per il parametro
     * @throws IllegalStateException    se non c'è una GameFactory impostata o è già
     *                                  stato impostata la PlayerFactory di un giocatore
     */
    public void setGameFactoryParamValue(String paramName, Object value) {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            containmentThread.submit(() -> {

                Objects.requireNonNull(paramName);
                Objects.requireNonNull(value);
                if (gF == null)
                    throw new IllegalStateException(PGUI_EXC_GAMEFACTORY_NOT_SET);
                if (playerSlots.size() > 0)
                    throw new IllegalStateException(PGUI_EXC_PLAYERFACTORY_ALREADY_SET);
                for (Param<?> param : gF.params()) {
                    if (param.name().equals(paramName)) {
                        param.set(value);
                        return;
                    }
                }
                throw new IllegalArgumentException(paramName + PGUI_EXC_NOT_A_GAMEFACTORY_PARAMETER);

            }).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NullPointerException) {
                throw (NullPointerException) cause;
            } else if (cause instanceof IllegalArgumentException) {
                throw (IllegalArgumentException) cause;
            } else if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Imposta un {@link PlayerGUI} con il nome e il master dati per il giocatore
     * di indice {@code pIndex}. Se c'era già un giocatore impostato per quell'indice,
     * lo sostituisce.
     *
     * @param pIndex indice di un giocatore
     * @param pName  nome del giocatore
     * @param master il master
     * @throws NullPointerException     se {@code pName} o {@code master} è null
     * @throws IllegalArgumentException se {@code pIndex} non è un indice di giocatore
     *                                  valido per la GameFactory impostata
     * @throws IllegalStateException    se non c'è una GameFactory impostata o se c'è
     *                                  una partita in corso.
     */
    public void setPlayerGUI(int pIndex, String pName, Consumer<MoveChooser<P>> master) {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            containmentThread.submit(() -> {

                Objects.requireNonNull(pName);
                Objects.requireNonNull(master);
                if (gF == null)
                    throw new IllegalStateException(PGUI_EXC_GAMEFACTORY_NOT_SET);
                if (playing.get())
                    throw new IllegalStateException(PGUI_EXC_ALREADY_PLAYING);
                if (pIndex < 0 || pIndex > gF.maxPlayers())
                    throw new IllegalArgumentException(PGUI_EXC_INVALID_INDEX);

                playerSlots.put(pIndex, new PlayerSlot<>(new PlayerGUI<>(pName, master)));

            }).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NullPointerException) {
                throw (NullPointerException) cause;
            } else if (cause instanceof IllegalArgumentException) {
                throw (IllegalArgumentException) cause;
            } else if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Imposta la {@link PlayerFactory} con nome dato per il giocatore di indice
     * {@code pIndex}. Usa {@link PlayerFactories} per creare la PlayerFactory nel
     * thread di confinamento. La PlayerFactory è impostata solamente se il metodo
     * ritorna {@link PlayerFactory.Play#YES}. Se c'era già un giocatore impostato
     * per quell'indice, lo sostituisce.
     *
     * @param pIndex indice di un giocatore
     * @param fName  nome di una PlayerFactory
     * @param pName  nome del giocatore
     * @param dir    la directory della PlayerFactory o null
     * @return un valore (vedi {@link PlayerFactory.Play}) che informa sulle
     * capacità dei giocatori di questa fabbrica di giocare al gioco specificato.
     * @throws NullPointerException     se {@code fName} o {@code pName} è null
     * @throws IllegalArgumentException se {@code pIndex} non è un indice di giocatore
     *                                  valido per la GameFactory impostata o se non esiste una PlayerFactory di nome
     *                                  {@code fName}
     * @throws IllegalStateException    se la creazione della PlayerFactory fallisce o
     *                                  se non c'è una GameFactory impostata o se c'è una partita in corso.
     */
    @SuppressWarnings("unchecked")
    public PlayerFactory.Play setPlayerFactory(int pIndex, String fName, String pName, Path dir) {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            return containmentThread.submit(() -> {
                Objects.requireNonNull(fName);
                Objects.requireNonNull(pName);
                if (gF == null)
                    throw new IllegalStateException(PGUI_EXC_GAMEFACTORY_NOT_SET);
                if (pIndex < 0 || pIndex > gF.maxPlayers())
                    throw new IllegalArgumentException(PGUI_EXC_INVALID_INDEX);
                if (!Arrays.asList(PlayerFactories.availableBoardFactories()).contains(fName))
                    throw new IllegalArgumentException(fName + PGUI_EXC_NOT_A_GAMEFACTORY);
                if (playing.get())
                    throw new IllegalStateException(PGUI_EXC_ALREADY_PLAYING);

                try {
                    PlayerFactory<Player<P>, GameRuler<P>> pF = PlayerFactories.getBoardFactory(fName);
                    pF.setDir(dir);
                    PlayerFactory.Play canPlay = pF.canPlay((GameFactory) gF);
                    if (canPlay.equals(PlayerFactory.Play.YES)) {
                        PlayerSlot<P> pSlot = new PlayerSlot<>(pF);
                        pSlot.set(pName);
                        playerSlots.put(pIndex, pSlot);
                    }
                    return canPlay;
                } catch (IllegalArgumentException e) {
                    throw new IllegalStateException(PGUI_EXC_GAMEFACTORY_CREATION_FAILED);
                }

            }).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NullPointerException) {
                throw (NullPointerException) cause;
            } else if (cause instanceof IllegalArgumentException) {
                throw (IllegalArgumentException) cause;
            } else if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Ritorna i nomi dei parametri della {@link PlayerFactory} di indice
     * {@code pIndex}. Se la PlayerFactory non ha parametri, ritorna un array vuoto.
     *
     * @param pIndex indice di un giocatore
     * @return i nomi dei parametri della PlayerFactory di indice dato
     * @throws IllegalArgumentException se non c'è una PlayerFactory di indice
     *                                  {@code pIndex}
     */
    public String[] getPlayerFactoryParams(int pIndex) {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            return containmentThread.submit(() -> {

                PlayerFactory<Player<P>, GameRuler<P>> pF;
                if (!playerSlots.containsKey(pIndex) || (pF = playerSlots.get(pIndex).getFactory()) == null) {
                    throw new IllegalArgumentException(
                            PGUI_EXC_PLAYERFACTORY_NOT_SET
                    );
                }
                return pF.params().stream().map(Param::name).collect(Collectors.toList()).toArray(new String[0]);

            }).get();
        } catch (ExecutionException e) {
            throw new RuntimeException(e.getCause());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Ritorna il prompt del parametro con il nome specificato della
     * {@link PlayerFactory} di indice {@code pIndex}.
     *
     * @param pIndex    indice di un giocatore
     * @param paramName nome del parametro
     * @return il prompt del parametro con il nome specificato della PlayerFactory
     * di indice dato
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la PlayerFactory non ha un parametro di
     *                                  nome {@code paramName} o non c'è una PlayerFactory di indice {@code pIndex}
     */
    public String getPlayerFactoryParamPrompt(int pIndex, String paramName) {
        return getPlayerFactoryParamInfo(pIndex, paramName, Param::prompt);
    }

    /**
     * Ritorna i valori ammissibili per il parametro di nome dato della
     * {@link PlayerFactory} di indice {@code pIndex}.
     *
     * @param pIndex    indice di un giocatore
     * @param paramName nome del parametro
     * @return i valori ammissibili per il parametro di nome dato della PlayerFactory
     * di indice dato.
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la PlayerFactory non ha un parametro di
     *                                  nome {@code paramName} o non c'è una PlayerFactory di indice {@code pIndex}
     */
    public Object[] getPlayerFactoryParamValues(int pIndex, String paramName) {
        return getPlayerFactoryParamInfo(pIndex, paramName, p -> p.values().toArray());
    }

    /**
     * Ritorna il valore del parametro di nome dato della {@link PlayerFactory} di
     * indice {@code pIndex}.
     *
     * @param pIndex    indice di un giocatore
     * @param paramName nome del parametro
     * @return il valore del parametro di nome dato della PlayerFactory di indice
     * dato
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la PlayerFactory non ha un parametro di
     *                                  nome {@code paramName} o non c'è una PlayerFactory di indice {@code pIndex}
     */
    public Object getPlayerFactoryParamValue(int pIndex, String paramName) {
        return getPlayerFactoryParamInfo(pIndex, paramName, Param::get);
    }

    /**
     * Ritorna il valore ottenuto applicando {@code map} al parametro
     * di nome {@code paramName}.
     *
     * @param paramName Il nome del parametro
     * @param map       La funzione a applicare al parametro
     * @param <T>       Tipo del valore da ritornare
     * @return Il valore della funzione applicata al parametro
     * @throws NullPointerException     se {@code paramName} è null
     * @throws IllegalArgumentException se la PlayerFactory impostata non ha un
     *                                  parametro di nome {@code paramName}
     * @throws IllegalStateException    se non c'è una PlayerFactory impostata
     */
    private <T> T getPlayerFactoryParamInfo(int pIndex, String paramName, Function<Param<?>, T> map) {
        if (!restarted.get()) throw new IllegalStateException();
        if (!playerSlots.containsKey(pIndex) || playerSlots.get(pIndex).getState() != PlayerSlot.State.FACTORY)
            throw new IllegalStateException();

        try {
            return containmentThread.submit(() -> {

                Objects.requireNonNull(paramName);
                Objects.requireNonNull(map);
                PlayerFactory<Player<P>, GameRuler<P>> pF = playerSlots.get(pIndex).getFactory();
                if (pF == null)
                    throw new IllegalStateException(PGUI_EXC_PLAYERFACTORY_NOT_SET);
                for (Param<?> param : pF.params()) {
                    if (param.name().equals(paramName)) {
                        return map.apply(param);
                    }
                }
                throw new IllegalArgumentException(paramName + PGUI_EXC_NOT_A_PLAYERFACTORY_PARAMETER);

            }).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NullPointerException) {
                throw (NullPointerException) cause;
            } else if (cause instanceof IllegalArgumentException) {
                throw (IllegalArgumentException) cause;
            } else if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Imposta il valore del parametro di nome dato della {@link PlayerFactory}
     * di indice {@code pIndex}.
     *
     * @param pIndex    indice di un giocatore
     * @param paramName nome del parametro
     * @param value     un valore ammissibile per il parametro
     * @throws NullPointerException     se {@code paramName} o {@code value} è null
     * @throws IllegalArgumentException se la PlayerFactory non ha un parametro di
     *                                  nome {@code paramName} o {@code value} non è un valore ammissibile per il
     *                                  parametro o non c'è una PlayerFactory di indice {@code pIndex}
     * @throws IllegalStateException    se c'è una partita in corso
     */
    public void setPlayerFactoryParamValue(int pIndex, String paramName, Object value) {
        if (!restarted.get()) throw new IllegalStateException();

        try {
            containmentThread.submit(() -> {

                Objects.requireNonNull(paramName);
                Objects.requireNonNull(value);
                if (playerSlots.get(pIndex) == null) {
                    throw new IllegalArgumentException(PGUI_EXC_PLAYERFACTORY_NOT_SET);
                }
                if (playerSlots.get(pIndex).getState() != PlayerSlot.State.FACTORY) {
                    throw new IllegalArgumentException(pIndex + PGUI_EXC_IS_A_PLAYERGUI);
                }
                PlayerFactory<Player<P>, GameRuler<P>> pF = playerSlots.get(pIndex).getFactory();
                if (playing.get()) {
                    throw new IllegalStateException(PGUI_EXC_ALREADY_PLAYING);
                }
                for (Param<?> param : pF.params()) {
                    if (param.name().equals(paramName)) {
                        param.set(value);
                        return;
                    }
                }
                throw new IllegalArgumentException(paramName + PGUI_EXC_NOT_A_PLAYERFACTORY_PARAMETER);

            }).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NullPointerException) {
                throw (NullPointerException) cause;
            } else if (cause instanceof IllegalArgumentException) {
                throw (IllegalArgumentException) cause;
            } else if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                throw new RuntimeException(cause);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Inizia una partita con un gioco fabbricato dalla GameFactory impostata e i
     * giocatori forniti da {@link PlayerGUI} impostati o fabbricati dalle
     * PlayerFactory impostate. Se non c'è una GameFactory impostata o non ci sono
     * sufficienti giocatori impostati o c'è già una partita in corso, fallisce. Se
     * sono impostati dei vincoli sui thread per le invocazioni di
     * {@link Player#getMove}, allora prima di iniziare la partita invoca i metodi
     * {@link Player#threads(int, ForkJoinPool, ExecutorService)} di tutti i giocatori,
     * ovviamente nel thread di confinamento.
     * <br>
     * Il metodo ritorna immediatamente, non attende che la partita termini. Quindi
     * usa un thread per gestire la partita oltre al thread di confinamento usato
     * per l'invocazione di tutti i metodi del GameRuler e dei Player.
     *
     * @param tol        massimo numero di millisecondi di tolleranza per le mosse, cioè se
     *                   il gioco ha un tempo limite <i>T</i> per le mosse, allora il tempo di
     *                   attesa sarà <i>T</i> + {@code tol}; se {@code tol} &lt;= 0, allora
     *                   nessuna tolleranza
     * @param timeout    massimo numero di millisecondi per le invocazioni dei metodi
     *                   dei giocatori escluso {@link Player#getMove()}, se &lt;= 0,
     *                   allora nessun limite
     * @param minTime    minimo numero di millisecondi tra una mossa e quella successiva,
     *                   se &lt;= 0, allora nessuna pausa
     * @param maxTh      massimo numero di thread addizionali permessi per
     *                   {@link Player#getMove()}, se &lt; 0, nessun limite è imposto
     * @param fjpSize    numero di thread per il {@link ForkJoinTask ForkJoin} pool,
     *                   se == 0, non è permesso alcun pool, se invece è &lt; 0, non c'è
     *                   alcun vincolo e possono usare anche
     *                   {@link ForkJoinPool#commonPool() Common Pool}
     * @param bgExecSize numero di thread permessi per esecuzioni in background, se
     *                   == 0, non sono permessi, se invece è &lt; 0, non c'è alcun
     *                   vincolo
     * @throws IllegalStateException se non c'è una GameFactory impostata o non ci
     *                               sono sufficienti PlayerFactory impostate o la creazione del GameRuler o quella
     *                               di qualche giocatore fallisce o se già c'è una partita in corso.
     */
    @SuppressWarnings("unchecked")
    public void play(long tol, long timeout, long minTime, int maxTh, int fjpSize, int bgExecSize) {
        List<Player<P>> players = new CopyOnWriteArrayList<>();
        final AtomicBoolean gMNeedsControl = new AtomicBoolean(false);
        final AtomicLong gMBlockTime = new AtomicLong(-1);

        if (!restarted.get()) throw new IllegalStateException();

        //Variabili per i setGame e moved temporizzati
        boolean sGMNeedsControl;
        long sGMBlockTime;
        if (maxBlockTime < 0 && timeout <= 0) { // Nessun timeout
            sGMNeedsControl = false;
            sGMBlockTime = 0;
        } else { // Almeno un timeout impostato
            sGMNeedsControl = true;
            if (maxBlockTime >= 0 && timeout > 0) {
                sGMBlockTime = Math.min(maxBlockTime, timeout);
            } else if (maxBlockTime >= 0) {
                sGMBlockTime = maxBlockTime;
            } else {
                sGMBlockTime = timeout;
            }
        }

        // Creazione variabili utili alla partita e controllo delle eccezioni
        Future playSetupFuture = containmentThread.submit(() -> {

            // NB: Nell'ambito del progetto, ci sono due e soli due giocatori
            if (gF == null)
                throw new IllegalStateException("GameFactory non impostata.");
            for (int i = 1; i <= 2; i++) {
                if (!playerSlots.containsKey(i))
                    throw new IllegalStateException("Giocatore " + i + " non impostato.");
            }
            if (playing.get())
                throw new IllegalStateException("È già in corso una partita");
            for (int i = 1; i <= 2; i++) {
                PlayerSlot<P> pSlot = playerSlots.get(i);
                Player<P> p = pSlot.getState() == PlayerSlot.State.PLAYER ?
                        pSlot.getPlayer() :
                        pSlot.getFactory().newPlayer((GameFactory) gF, pSlot.getName());
                players.add(p);
            }
            gF.setPlayerNames(players.stream().map(Player::name).collect(Collectors.toList()).toArray(new String[2]));
            gR = (GameRuler) gF.newGame();

            //Variabili per i getMove temporizzati
            if (gR.mechanics().time == -1) {
                gMNeedsControl.set(false);
                gMBlockTime.set(-1);
            } else {
                gMNeedsControl.set(true);
                gMBlockTime.set(gR.mechanics().time + Math.max(tol, 0));
            }

            // setGame temporizzato
            for (int i = 0; i < players.size(); i++) {
                if (sGMNeedsControl) timeoutController.start(sGMBlockTime, containmentThread, controllerThread, i + 1);
                players.get(i).setGame(gR.copy());
                if (sGMNeedsControl) timeoutController.stop();
            }

        });
        try {
            playSetupFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            Throwable cause = e.getCause();

            if (cause instanceof IllegalStateException) {
                throw (IllegalStateException) cause;
            } else {
                String message = "Creazione del GameRuler o dei Player fallita";
                if (cause != null) {
                    message += ": " + cause.getMessage();
                }
                throw new IllegalStateException(message);
            }
        }

        restarted.set(false);
        //Task della partita
        containmentThread.submit(() -> {

            // Classi per chiamate a threads
            ForkJoinPool[] fjPools = new ForkJoinPool[]{null, null};
            if (fjpSize > 0) {
                for (int i = 0; i < 2; i++) {
                    fjPools[i] = new ForkJoinPool(fjpSize);
                }
            }
            ExecutorService[] executors = new ExecutorService[]{null, null};
            if (bgExecSize > 0) {
                for (int i = 0; i < 2; i++) {
                    executors[i] = Executors.newFixedThreadPool(
                            bgExecSize,
                            r -> {
                                Thread t = Executors.defaultThreadFactory().newThread(r);
                                t.setDaemon(true);
                                return t;
                            }
                    );
                }
            } else if (bgExecSize < 0) {
                for (int i = 0; i < 2; i++) {
                    executors[i] = Executors.newCachedThreadPool(
                            r -> {
                                Thread t = Executors.defaultThreadFactory().newThread(r);
                                t.setDaemon(true);
                                return t;
                            }
                    );
                }
            }

            try {
                playing.set(true);
                for (int i = 0; i < players.size(); i++) {
                    players.get(i).threads(
                            maxTh,
                            fjpSize < 0 ? ForkJoinPool.commonPool() : fjPools[i],
                            executors[i]
                    );
                }
                if (sGMNeedsControl) timeoutController.start(sGMBlockTime, containmentThread, controllerThread, -1);
                obs.setGame(gR.copy());
                if (sGMNeedsControl) timeoutController.stop();
                if (Thread.currentThread().isInterrupted()) return;
                while (gR.result() == -1) {
                    long startTime = System.currentTimeMillis();

                    int currentPlayerIndex = gR.turn();

                    if (gMNeedsControl.get())
                        timeoutController.start(gMBlockTime.get(), containmentThread, controllerThread, currentPlayerIndex);
                    Move currentMove = players.get(currentPlayerIndex - 1).getMove();
                    if (gMNeedsControl.get()) timeoutController.stop();
                    if (Thread.currentThread().isInterrupted()) break;

                    gR.move(currentMove);

                    for (int i = 0; i < players.size(); i++) {
                        if (sGMNeedsControl)
                            timeoutController.start(sGMBlockTime, containmentThread, controllerThread, i + 1);
                        players.get(i).moved(currentPlayerIndex, currentMove);
                        if (sGMNeedsControl) timeoutController.stop();
                        if (Thread.currentThread().isInterrupted()) break;
                    }

                    if (sGMNeedsControl) timeoutController.start(sGMBlockTime, containmentThread, controllerThread, -1);
                    obs.moved(currentPlayerIndex, currentMove);
                    if (sGMNeedsControl) timeoutController.stop();
                    if (Thread.currentThread().isInterrupted()) break;

                    long endTime = System.currentTimeMillis();
                    long pastTime = endTime - startTime;

                    if (minTime > 0) {
                        long waitTime = minTime - pastTime;
                        if (waitTime > 0) {
                            try {
                                Thread.sleep(waitTime);
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    }

                    if (Thread.currentThread().isInterrupted()) break;
                }
                playing.set(false);
            } catch (Exception e) {
                stop();
            } finally { // Effettua lo shutdown dgli esecutori
                for (ForkJoinPool fjPool : fjPools) {
                    if (fjPool != null) {
                        fjPool.shutdown();
                    }
                }
                for (ExecutorService executor : executors) {
                    if (executor != null) {
                        executor.shutdown();
                    }
                }
            }
        });
    }

    /**
     * Se c'è una partita in corso la termina immediatamente e ritorna true,
     * altrimenti non fa nulla e ritorna false.
     *
     * @return true se termina la partita in corso, false altrimenti
     */
    public boolean stop() {
        if (playing.get()) {
            obs.interrupted(PGUI_INTERRUPTED);
            if (!containmentThread.isShutdown()) containmentThread.shutdownNow();
            playing.set(false);
            return true;
        }
        return false;
    }

    /**
     * Classe che gestisce i controlli di timeout all'interno del metodo {@link PlayGUI#play(long, long, long, int, int, int)}.
     * I due metodi di cui dispone segnalano al controllore inizio e fine delle invocazioni da controllare
     */
    private class TimeoutController {
        final Object synchronizer = new Object();
        final AtomicBoolean finished = new AtomicBoolean(false);
        final AtomicBoolean canStart = new AtomicBoolean(true);

        ExecutorService containment;
        ExecutorService controller;

        /**
         * Aspetta, nel thread controllore, un tempo pari a {@code timeoutMillis} millisecondi.
         * Dopodiché, se non è ancora stato invocato stop, termina il gioco, chiamando limitBreak e interrupted
         * dell'Observer e interrompendo il thread di confinamento.
         * Se il thread controllore viene interrotto durante l'attesa, interrompe anche il thread di confinamento
         * e chiude entrambi gli esecutori
         *
         * @param timeoutMillis Il tempo concesso per l'esecuzione temporizzata
         */
        void start(long timeoutMillis, ExecutorService containment, ExecutorService controller, int playerIndex) {
            this.containment = containment;
            this.controller = controller;

            if (canStart.compareAndSet(true, false)) {
                finished.set(false);
                controllerThread.submit(() -> {
                    long startTime = System.currentTimeMillis();
                    long pastTime, waitTime;
                    try {
                        synchronized (synchronizer) {
                            do {
                                pastTime = System.currentTimeMillis() - startTime;
                                waitTime = timeoutMillis - pastTime;
                                if (waitTime > 0) {
                                    synchronizer.wait(waitTime);
                                }
                            } while (!finished.get() && pastTime < timeoutMillis);
                        }
                    } catch (InterruptedException e) {
                        if (!containment.isShutdown()) {
                            containment.shutdownNow();
                        }
                    }
                    if (!finished.get()) {
                        if (playerIndex != -1) {
                            obs.limitBreak(playerIndex, PGUI_TIMEOUT_ERROR);
                        }
                        obs.interrupted(PGUI_TIMEOUT_ERROR);
                        containmentThread.shutdownNow();
                    }
                    finished.set(true);
                    canStart.set(true);
                });
            } else {
                throw new IllegalStateException(PGUI_ALREADY_TIMING_A_METHOD);
            }
        }

        /**
         * Segnala al controllore la fine entro il tempo limite dell'esecuzione temporizzata.
         * Fallisce se non è in atto il controllo di un'esecuzione.
         */
        void stop() {
            finished.set(true);
            canStart.set(true);
            synchronized (synchronizer) {
                synchronizer.notifyAll();
            }
        }
    }
}

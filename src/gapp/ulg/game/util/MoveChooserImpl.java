package gapp.ulg.game.util;

import gapp.ulg.game.board.*;

import java.util.*;

import static gapp.ulg.game.board.Board.Dir.*;

/**
 * Classe che implementa l'interfaccia {@link PlayerGUI.MoveChooser}.
 * La rappresentazione dell'albero si basa sulla classe {@link Node}.
 *
 * @param <P> Tipo del modello dei pezzi
 * @see PlayerGUI.MoveChooser
 * @see Node
 */
public class MoveChooserImpl<P> implements PlayerGUI.MoveChooser<P> {
    /**
     * Il nodo radice dell'albero delle mosse
     */
    private Node<List<Action<P>>> root;
    /**
     * Il nodo dell'albero correntemente selezionato
     */
    private Node<List<Action<P>>> currentNode;
    /**
     * Il GameRuler di appoggio
     */
    private GameRuler<P> gR;
    /**
     * L'oggetto su cui chiamare notifyAll una volta scelta la mossa.
     */
    private final Object synchronizer;
    /**
     * L'insieme di mosse valide nella situazione corrente
     */
    private Set<Move<P>> gRValidMoves;
    /**
     * La selezione corrente
     */
    private List<Pos> selection;
    /**
     * Se è stata scelta una mossa
     */
    private volatile boolean isMoveChosen;
    /**
     * La mossa scelta
     */
    private Move<P> chosenMove;
    /**
     * Il tempo al momento in cui la selezione della mossa è iniziata, irrilevante e quindi
     * indefinito se non c'è limite
     */
    private long startTimeMillis;
    /**
     * Il tempo disponibile per scegliere la mossa, o -1 se
     * non c'è limite
     */
    private long allowedTimePerMove;

    /**
     * Costruisce un oggetto {@code MoveChooserImpl} senza limite di tempo per
     * la scelta della mossa
     *
     * @param gR           Il {@link GameRuler} di appoggio
     * @param synchronizer L'{@code Object} da notificare alla scelta della mossa
     */
    public MoveChooserImpl(GameRuler<P> gR, Object synchronizer) {
        this(gR, synchronizer, -1, -1);
    }

    /**
     * Costruisce un oggetto {@code MoveChooserImpl} con un limite di tempo per
     * la scelta della mossa
     *
     * @param gR              Il {@link GameRuler} di appoggio
     * @param synchronizer    L'{@code Object} da notificare alla scelta della mossa
     * @param startTimeMillis Il tempo nel momento in cui la selezione della mossa è iniziata,
     *                        irrilevante se non c'è limite di tempo
     * @param timePerMove     Il tempo, in millisecondi, per poter selezionare la mossa da giocare,
     *                        oppure -1 se non c'è limite
     */
    public MoveChooserImpl(GameRuler<P> gR, Object synchronizer, long startTimeMillis, long timePerMove) {
        Objects.requireNonNull(gR);
        gRValidMoves = gR.validMoves();
        Set<Move<P>> validMoves = new HashSet<>();
        gRValidMoves.forEach(m -> {
            if (m.kind == Move.Kind.ACTION) {
                validMoves.add(m);
            }
        });

        if (validMoves.size() == 0) {
            //Caso senza mosse valide
            root = emptyTree();
        } else {
            List<List<Action<P>>> actionLists = makeActionListsFromMovesSet(validMoves);
            root = makeRoot(actionLists); // Crea la radice
            for (List<Action<P>> actionList : actionLists) {
                if (!Objects.equals(root.value, actionList)) {
                    addNode(root, actionList);
                }
            }
        }
        currentNode = root;
        this.gR = gR;
        this.synchronizer = synchronizer;
        selection = new ArrayList<>();
        isMoveChosen = false;
        this.startTimeMillis = startTimeMillis;
        allowedTimePerMove = timePerMove;
        chosenMove = null;
    }

    /**
     * Costruisce e ritorna la radice dell'albero
     *
     * @param actionLists La lista di liste di azioni che identifica univocamente l'albero delle mosse
     * @return La radice, senza figli, dell'albero delle mosse
     */
    private synchronized Node<List<Action<P>>> makeRoot(List<List<Action<P>>> actionLists) {
        List<Action<P>> radice = commonPrefix(actionLists);
        return new Node<>(radice, null);
    }

    /**
     * Aggiunge un nodo all'albero radicato in {@code node}
     * (si assume che {@code root} sia già stata calcolata e che {@code actionList}
     * sia una mossa con prefisso in {@code node}).
     *
     * @param node       La radice dell'albero a cui aggiungere il nuovo nodo
     * @param actionList La lista di azioni del nuovo nodo
     */
    private synchronized void addNode(Node<List<Action<P>>> node, List<Action<P>> actionList) {
        actionList = actionList.subList(node.value.size(), actionList.size());
        if (actionList.isEmpty()) { // Se actionList corrisponde al nodo corrente
            return;
        }
        if (node.children.isEmpty()) {
            Node<List<Action<P>>> n = new Node<>(actionList, node);
            node.children.add(n);
        } else {
            for (Node<List<Action<P>>> child : node.children) {
                if (isCommonPrefixPresent(actionList, child.value)) {
                    // Caso (ex. {1,2},{1,3} <- {1})
                    if (actionList.equals(child.value)) {
                        return;
                    }
                    //Caso actionList genitore di child
                    else if (isParentOf(actionList, child.value)) {
                        node.children.remove(child);
                        int commonPrefixSize = commonPrefix(Arrays.asList(actionList, child.value)).size();
                        Node<List<Action<P>>> n = new Node<>(actionList, node);
                        node.children.add(n);
                        n.children.add(child);
                        child.value = child.value.subList(commonPrefixSize, child.value.size());
                        child.parent = n;
                    }
                    // Caso child genitore di actionList
                    else if (isParentOf(child.value, actionList)) {
                        addNode(child, actionList); //chiamata ricorsiva
                    }
                    //caso nuovo livello intermedio
                    else {
                        List<Action<P>> commonPrefix = commonPrefix(Arrays.asList(actionList, child.value));
                        Node<List<Action<P>>> intermedio = new Node<>(commonPrefix, node);
                        node.children.remove(child);
                        node.children.add(intermedio);
                        intermedio.children.add(
                                new Node<>(child.value.subList(commonPrefix.size(), child.value.size()), intermedio)
                        );
                        intermedio.children.add(
                                new Node<>(actionList.subList(commonPrefix.size(), actionList.size()), intermedio)
                        );
                    }
                    return;
                }
            }
            //Caso stesso livello
            Node<List<Action<P>>> n = new Node<>(actionList, node);
            node.children.add(n);
        }
    }

    /**
     * Ritorna true se {@code parent} è effettivamente antenato di {@code child} nell'albero
     * delle mosse in costruzione (non necessariamente quello definitivo).
     *
     * @param parent Il nodo candidato genitore
     * @param child  Il nodo candidato figlio
     * @return True se parent è antenato di child
     */
    private boolean isParentOf(List<Action<P>> parent, List<Action<P>> child) {
        return commonPrefix(Arrays.asList(parent, child)).size() == parent.size();
    }

    /**
     * Ritorna true se le liste in input hanno un prefisso comune non vuoto
     *
     * @param actionList1 Prima lista da confrontare
     * @param actionList2 Seconda lista da confrontare
     * @return True se le liste hanno prefisso comune non vuoto
     */
    private boolean isCommonPrefixPresent(List<Action<P>> actionList1, List<Action<P>> actionList2) {
        return commonPrefix(Arrays.asList(actionList1, actionList2)).size() > 0;
    }

    /**
     * Data una lista di liste di azioni, ritorna il loro prefisso comune,
     * se non c'è ritorna la lista vuota.
     *
     * @param actionLists La lista di liste di cui si vuole il prefisso comune
     * @return Il profisso comune
     */
    private List<Action<P>> commonPrefix(List<List<Action<P>>> actionLists) {
        List<Action<P>> prefix = actionLists.get(0);
        int maxI = prefix.size();
        for (List<Action<P>> actionList : actionLists) {
            maxI = Math.min(actionList.size(), maxI);
            int i = 0;
            for (; i < maxI; ++i) {
                if (!Objects.equals(prefix.get(i), actionList.get(i))) {
                    break;
                }
            }
            maxI = i;
        }
        return prefix.subList(0, maxI);
    }

    /**
     * Dato un insieme di mosse, lo converte in una lista di liste di azioni equivalente
     *
     * @param validMoves Le mosse da convertire
     * @return La lista di liste di azioni che rappresentano l'insieme di mosse
     */
    private List<List<Action<P>>> makeActionListsFromMovesSet(Set<Move<P>> validMoves) {
        List<List<Action<P>>> actionLists = new ArrayList<>();
        validMoves.forEach(m -> actionLists.add(m.actions));
        return actionLists;
    }

    /**
     * Ritorna un albero vuoto, rappresentato da un {@link Node}
     * senza valore o figli.
     *
     * @return Il nodo radice dell'albero vuoto
     */
    private Node<List<Action<P>>> emptyTree() {
        return new Node<>(null, null);
    }

    /**
     * @return True se l'albero radicato in {@code root} è vuoto
     */
    private boolean isTreeEmpty() {
        return root.value == null && root.children.size() == 0;
    }

    /**
     * @return Il nodo radice dell'abero delle mosse
     */
    public synchronized Node<List<Action<P>>> getRoot() {
        return root;
    }

    @Override
    public synchronized Optional<Move<P>> subMove() {
        if (isInValidState()) {
            boolean inEmptyTreeRoot =
                    currentNode == root && Objects.equals(currentNode.value, Collections.EMPTY_LIST) && currentNode.children.size() > 0;
            if (isTreeEmpty()) {
                return null;
            }
            if (inEmptyTreeRoot) {
                return Optional.empty();
            }

            return Optional.of(subMoveFromNode(currentNode));
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * @param node Il nodo di cui si vuole conoscere la sottomossa
     * @return La sottomossa del nodo
     * @throws NullPointerException Se node è null o node.value è null
     */
    private Move<P> subMoveFromNode(Node<List<Action<P>>> node) {
        return new Move<>(node.value);
    }

    /**
     * Ritorna la mossa rappresentata dal nodo, oppure null se ll'albero è vuoto o
     * {@code node} è la radice e questa ha prefisso vuoto
     *
     * @param node Il nodo di cui si vuole conoscere la mossa (compreso il prefisso)
     * @return La mossa rappresentata dal nodo
     */
    private Move<P> moveFromNode(Node<List<Action<P>>> node) {
        if (isTreeEmpty() || node.value.size() == 0) return null;

        List<Action<P>> actionList = new ArrayList<>();
        actionList.addAll(node.value);
        while (node.parent != null) {
            node = node.parent;
            List<Action<P>> old = actionList;
            actionList = new ArrayList<>(node.value);
            actionList.addAll(old);
        }
        return new Move<>(actionList);
    }

    @Override
    public synchronized List<Move<P>> childrenSubMoves() {
        if (isInValidState()) {
            if (isTreeEmpty()) {
                return null;
            }

            List<Move<P>> childrenSubMoves = new ArrayList<>();
            currentNode.children.forEach(c -> childrenSubMoves.add(subMoveFromNode(c)));
            return childrenSubMoves;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized List<Move<P>> select(Pos... pp) {
        if (isInValidState()) {
            Objects.requireNonNull(pp); //pp nullo
            //pp contiene null
            for (Pos p : pp) {
                Objects.requireNonNull(p);
            }
            //duplicati in pp o pp vuoto
            Set<Pos> posSet = new HashSet<>();
            posSet.addAll(Arrays.asList(pp));
            if (posSet.size() != pp.length || pp.length == 0) {
                throw new IllegalArgumentException();
            }
            //posizioni fuori board
            Board<P> board = gR.getBoard();
            for (Pos p : pp) {
                if (!board.isPos(p)) {
                    throw new IllegalArgumentException();
                }
            }
            //albero vuoto
            if (isTreeEmpty()) return null;

            selection.clear();
            selection.addAll(posSet);
            List<Node<List<Action<P>>>> selectedChildren = selectedNodes();
            List<Move<P>> selectedSubMoves = new ArrayList<>();
            selectedChildren.forEach(c -> {
                selectedSubMoves.add(subMoveFromNode(c));
            });
            return selectedSubMoves;
        } else {
            checkValidState();
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized List<Move<P>> quasiSelected() {
        if (isInValidState()) {
            if (isTreeEmpty()) return null;
            if (selection.size() == 0) {
                return new ArrayList<>();
            }
            List<Move<P>> quasiSelectedMoves = new ArrayList<>();
            currentNode.children.forEach(c -> {
                boolean quasiSelectsAction = true;
                Action<P> firstAction = c.value.get(0);
                //Controlla mossa quasi-selezionata
                if (firstAction.kind != Action.Kind.JUMP) {
                    List<Pos> childPositions = firstAction.pos;
                    if (selection.size() >= childPositions.size()) {
                        quasiSelectsAction = false;
                    } else {
                        quasiSelectsAction = childPositions.containsAll(selection);
                    }
                } else {
                    //JUMP mai quasi-selezionabile
                    quasiSelectsAction = false;
                }
                //Aggiungi la sottomossa del nodo
                if (quasiSelectsAction)
                    quasiSelectedMoves.add(subMoveFromNode(c));
            });
            return quasiSelectedMoves;
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Ritorna la lista di nodi selezionati dalla selezione
     * corrente di posizioni, senza prefisso
     *
     * @return La lista dei nodi selezionati
     * @throws IllegalStateException se è già stata scelta una mossa o il tempo
     *                               è scaduto
     */
    private List<Node<List<Action<P>>>> selectedNodes() {
        if (isInValidState()) {
            List<Node<List<Action<P>>>> selectedChildren = new ArrayList<>();
            currentNode.children.forEach(c -> {
                Set<Pos> posSet = new HashSet<>(selection);
                Action<P> firstAction = c.value.get(0); //L'unico nodo con value.size<1 è la radice
                boolean selectsAction = firstAction.kind != Action.Kind.JUMP ?
                        Objects.equals(posSet, new HashSet<>(firstAction.pos)) :
                        posSet.size() == 1 && Objects.equals(selection.get(0), firstAction.pos.get(0));
                if (selectsAction) {
                    selectedChildren.add(c);
                }
            });
            return selectedChildren;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized List<P> selectionPieces() {
        if (isInValidState()) {
            List<Node<List<Action<P>>>> nodes = selectionPieceNodes();
            if (nodes == null) return null; //Albero vuoto
            if (nodes.size() == 0) return new ArrayList<>(); //Nessun nodo selezionato

            List<P> pieces = new ArrayList<>();
            //Un solo nodo, con prima azione di tipo REMOVE
            if (nodes.size() == 1 && nodes.get(0).value.get(0).kind == Action.Kind.REMOVE) {
                pieces.add(null);
            }
            //Altrimenti
            else {
                nodes.forEach(n -> pieces.add(n.value.get(0).piece));
            }
            return pieces;
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Se l'insieme dei nodi figli del nodo corrente selezionati dalla selezione
     * corrente non è vuoto e le prime azioni delle loro sotto-mosse o sono tutte
     * {@link Action.Kind#ADD} o sono tutte {@link Action.Kind#SWAP}, allora
     * ritorna la lista di tali nodi. Se invece c'è un solo nodo
     * figlio selezionato e la prima azione della sua sotto-mossa è
     * {@link Action.Kind#REMOVE}, ritorna una lista con tale nodo.
     * Altrimenti ritorna la lista vuota. La lista ritornata è sempre creata ex
     * novo. Se l'albero è vuoto, ritorna null.
     *
     * @return la lista nodi che soddisfano le condizioni, oppure la lista vuota se
     * nessun nodo soddisfa le condizioni, oppure null se l'albero è vuoto
     * @throws IllegalStateException se è già stata scelta una mossa o il tempo
     *                               è scaduto
     */
    private List<Node<List<Action<P>>>> selectionPieceNodes() {
        if (isInValidState()) {
            if (isTreeEmpty()) return null;

            List<Node<List<Action<P>>>> selectedChildren = selectedNodes();
            //Nessun nodo figlio selezionato
            if (selectedChildren.size() == 0) return Collections.emptyList();
            //Un solo nodo selezionato, di tipo Action.Kind.REMOVE
            if (selectedChildren.size() == 1 && selectedChildren.get(0).value.get(0).kind == Action.Kind.REMOVE) {
                return Arrays.asList(selectedChildren.get(0));
            }
            //Altrimenti
            Action.Kind kind = selectedChildren.get(0).value.get(0).kind;
            if (kind != Action.Kind.SWAP && kind != Action.Kind.ADD)
                return Collections.emptyList();
            List<Node<List<Action<P>>>> nodes = new ArrayList<>();
            for (Node<List<Action<P>>> n : selectedChildren) {
                Action<P> firstAction = n.value.get(0);
                if (firstAction.kind == kind) {
                    nodes.add(n);
                } else {
                    return Collections.emptyList();
                }
            }
            return nodes;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public void clearSelection() {
        if (isInValidState()) {
            selection.clear();
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized Move<P> doSelection(P pm) {
        if (isInValidState()) {
            List<Node<List<Action<P>>>> selectionPieceNodes = selectionPieceNodes();
            if (selectionPieceNodes == null || selectionPieceNodes.size() == 0) return null;
            Action<P> firstAction = selectionPieceNodes.get(0).value.get(0);
            if (selectionPieceNodes.size() == 1 && firstAction.kind == Action.Kind.REMOVE && pm == null) {
                return select(selectionPieceNodes.get(0));
            } else if (firstAction.kind == Action.Kind.ADD || firstAction.kind == Action.Kind.SWAP) {
                for (Node<List<Action<P>>> node : selectionPieceNodes) {
                    P piece = node.value.get(0).piece;
                    if (Objects.equals(pm, piece)) {
                        return select(node);
                    }
                }
            }
            return null;
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Imposta {@code node} come nodo correntemente selezionato nell'albero
     * delle mosse e ritorna la sottomossa di tale nodo
     *
     * @param node Il nodo da selezionare
     * @return La sottomossa del nodo selezionato
     * @throws IllegalStateException se è già stata scelta una mossa o il tempo
     *                               è scaduto
     * @throws NullPointerException  se {@code node} è null
     */
    private synchronized Move<P> select(Node<List<Action<P>>> node) {
        if (isInValidState()) {
            currentNode = Objects.requireNonNull(node);
            clearSelection();
            return subMoveFromNode(currentNode);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized Move<P> jumpSelection(Pos p) {
        if (isInValidState()) {
            List<Node<List<Action<P>>>> selectedNodes = selectedNodes();
            for (Node<List<Action<P>>> node : selectedNodes) {
                Action<P> firstAction = node.value.get(0);
                if (firstAction.kind == Action.Kind.JUMP && Objects.equals(p, firstAction.pos.get(1))) {
                    return select(node);
                }
            }
            return null;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized Move<P> moveSelection(Board.Dir d, int ns) {
        if (isInValidState()) {
            List<Node<List<Action<P>>>> selectedNodes = selectedNodes();
            for (Node<List<Action<P>>> node : selectedNodes) {
                Action<P> firstAction = node.value.get(0);
                if (firstAction.kind == Action.Kind.MOVE && firstAction.dir == d && firstAction.steps == ns) {
                    return select(node);
                }
            }
            return null;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized Move<P> back() {
        if (isInValidState()) {
            if (currentNode == root || isTreeEmpty()) return null;

            //Costruzione board di partenza
            Board<P> board = gR.getBoard();
            Board<P> copyBoard = new CopyBoard<P>(board);

            Move<P>
                    move = moveFromNode(currentNode),
                    subMove = subMoveFromNode(currentNode);
            //Gioca tutte le azioni fino all'inizio della subMove
            for (int i = 0; i < move.actions.size() - subMove.actions.size(); ++i) {
                playAction(move.actions.get(i), copyBoard);
            }

            //Crea la mossa inversa
            List<Action<P>> invertedActions = new ArrayList<>();
            subMove.actions.forEach(a -> {
                switch (a.kind) {
                    case ADD:
                        invertedActions.add(new Action<>(a.pos.get(0)));
                        break;
                    case REMOVE:
                        for (int i = a.pos.size() - 1; i >= 0; --i) {
                            Pos p = a.pos.get(i);
                            invertedActions.add(new Action<>(p, copyBoard.get(p)));
                        }
                        break;
                    case MOVE:
                        invertedActions.add(new Action<>(
                                oppositeDirection(a.dir), a.steps, afterMoveAction(a.dir, a.steps, a.pos)
                        ));
                        break;
                    case JUMP:
                        invertedActions.add(new Action<>(a.pos.get(1), a.pos.get(0)));
                        break;
                    case SWAP:
                        for (int i = a.pos.size() - 1; i >= 0; --i) {
                            Pos p = a.pos.get(i);
                            invertedActions.add(new Action<>(copyBoard.get(p), p));
                        }
                        break;
                }
                playAction(a, copyBoard);
            });
            clearSelection();
            currentNode = currentNode.parent;
            Collections.reverse(invertedActions);
            return new Move<>(invertedActions);
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Board che fa da copia modificabile per un'altra board, delegando a questa le chiamate
     * a getter dai risultati costanti (es. adjacents), ma mantenendo una mappa per le
     * informazioni mutabili. Il principale vantaggio è che una Board del genere può copiare
     * in maniera simile ad una copia profonda una qualsiasi Board, indipendentemente dal system.
     * @param <P> Tipo del modello dei pezzi
     */
    private static class CopyBoard<P> implements Board<P> {
        Map<Pos, P> pieces;
        Board<P> board;

        CopyBoard(Board<P> board) {
            pieces = new HashMap<>();
            this.board = board;
            board.positions().forEach(pos -> {
                P piece = board.get(pos);
                if (piece != null) {
                    pieces.put(pos, piece);
                }
            });
        }

        @Override
        public System system() {
            return board.system();
        }

        @Override
        public int width() {
            return board.width();
        }

        @Override
        public int height() {
            return board.height();
        }

        @Override
        public Pos adjacent(Pos p, Dir d) {
            return board.adjacent(p,d);
        }

        @Override
        public List<Pos> positions() {
            return board.positions();
        }

        @Override
        public P get(Pos p) {
            return pieces.get(Objects.requireNonNull(p));
        }

        @Override
        public P remove(Pos p) {
            return pieces.remove(Objects.requireNonNull(p));
        }

        @Override
        public P put(P pm, Pos p) {
            return pieces.put(Objects.requireNonNull(p), Objects.requireNonNull(pm));
        }

        @Override
        public boolean isModifiable() {
            return true;
        }
    }

    /**
     * Gioca {@code action} su {@code Board}. Si assume che l'azione sia giocabile
     * sulla board e che questa sia modificabile
     *
     * @param action L'azione da giocare
     * @param board  La board da modificare
     */
    private void playAction(Action<P> action, Board<P> board) {
        switch (action.kind) {
            case ADD:
                board.put(action.piece, action.pos.get(0));
                break;
            case REMOVE:
                action.pos.forEach(board::remove);
                break;
            case MOVE:
                action.pos.forEach(p -> {
                    P piece = board.remove(p);
                    Pos pos = afterMoveAction(action.dir, action.steps, Collections.singletonList(p))[0];
                    board.put(piece, pos);
                });
                break;
            case JUMP:
                P piece = board.remove(action.pos.get(0));
                board.put(piece, action.pos.get(1));
                break;
            case SWAP:
                action.pos.forEach(p -> board.put(action.piece, p));
                break;
        }
    }

    /**
     * Ritorna la direzione inversa a quella corrente
     *
     * @param d La direzione da invertire
     * @return La direzione invertita
     * @throws NullPointerException     se d è null
     * @throws IllegalArgumentException se d non è una direzione valida
     */
    private Board.Dir oppositeDirection(Board.Dir d) {
        Objects.requireNonNull(d);
        switch (d) {
            case UP:
                return DOWN;
            case UP_R:
                return DOWN_L;
            case RIGHT:
                return LEFT;
            case DOWN_R:
                return UP_L;
            case DOWN:
                return UP;
            case DOWN_L:
                return UP_R;
            case LEFT:
                return RIGHT;
            case UP_L:
                return DOWN_R;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Ritorna un array di posizioni tale che, per ogni i in [0,{@code pp.length}),
     * l'i-esimo elemento ritornato sarà la posizione {@code pp[i]}, spostata di
     * {@code ns} passi lungo la direzione {@code d}. Ritorna un array vuoto se pp
     * è vuoto. Si assume che l'operazione di spostamento, eseguita tramite
     * {@link Board#adjacent(Pos, Board.Dir)} della {@link Board} del {@link GameRuler}
     * sottostante. Si assume che tutte le operazioni sulla board siano eseguibili
     * senza errori, in caso contrario il comportamento è indeterminato.
     *
     * @param d  La direzione in cui spostare le posizioni
     * @param ns Il numero di passi da eseguire
     * @param pp Le posizioni di cui calcolare lo spostamento
     * @return Le posizioni di destinazione, o un array vuoto se non ci sono
     * posizioni di partenza
     */
    private Pos[] afterMoveAction(Board.Dir d, int ns, List<Pos> pp) {
        Pos[] new_pp = new Pos[pp.size()];
        Board<P> board = gR.getBoard();
        for (int i = 0; i < pp.size(); i++) {
            Pos pos = pp.get(i);
            for (int j = 0; j < ns; j++) {
                pos = board.adjacent(pos, d);
            }
            new_pp[i] = pos;
        }
        return new_pp;
    }

    @Override
    public synchronized boolean isFinal() {
        if (isInValidState() && !isTreeEmpty()) {
            Move<P> move = moveFromNode(currentNode);
            return move != null && gRValidMoves.contains(move);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized void move() {
        checkValidState();

        if (!isTreeEmpty() && isFinal()) {
            choose(moveFromNode(currentNode));
        }
    }

    @Override
    public synchronized boolean mayPass() {
        if (isInValidState()) {
            return gRValidMoves.contains(new Move<P>(Move.Kind.PASS));
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized void pass() {
        if (mayPass()) {
            choose(new Move<>(Move.Kind.PASS));
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public synchronized void resign() {
        choose(new Move<>(Move.Kind.RESIGN));
    }

    /**
     * Sceglie la mossa selezionata, rende l'oggetto inutilizzabile
     * e notifica il PlayerGUI che la scelta della mossa è stata terminata
     *
     * @param move La mossa scelta
     * @throws IllegalStateException se è già stata scelta una mossa o il tempo
     *                               a disposizione è terminato
     */
    private synchronized void choose(Move<P> move) {
        if (isInValidState()) {
            isMoveChosen = true;
            chosenMove = move;
            synchronized (synchronizer) {
                synchronizer.notifyAll();
            }
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Ritorna la mossa scelta. È l'unico metodo che può essere chiamato
     * da utilizzatori esterni, oltre a {@link MoveChooserImpl#isMoveChosen()}
     * dopo che l'oggetto è stato invalidato, e non può essere chiamato prima
     * dell'invalidazione. Ritorna null se l'oggetto viene invalidato prima
     * che la selezione della mossa sia terminata.
     *
     * @return La mossa scelta tramite i metodi di questo oggetto
     * @throws IllegalStateException se non è stata scelta una mossa.
     */
    public synchronized Move<P> getMove() {
        if (isMoveChosen) {
            return chosenMove;
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Ritorna true se è stata scelta una mossa. Se l'oggetto è stato invalidato,
     * ritorna false
     *
     * @return Se è stata scelta una mossa
     */
    public synchronized boolean isMoveChosen() {
        return isMoveChosen;
    }

    /**
     * Rende questo oggetto inutilizzabile
     *
     * @throws IllegalStateException se questo oggetto è già invalido
     */
    public synchronized void invalidate() {
        if (isInValidState()) {
            isMoveChosen = true;
        }
    }

    /**
     * @return Se non è stata scelta una mossa e non è stato superato
     * l'eventuale limite di tempo
     */
    private boolean isInValidState() {
        return !(isMoveChosen || isTimeOut());
    }

    /**
     * Controlla lo stato del MoveChooser e se non è valido lancia l'eccezione appropriata
     */
    private void checkValidState() {
        if (isMoveChosen())
            throw new IllegalStateException("È già stata scelta una mossa");
        else if (isTimeOut())
            throw new IllegalStateException("Timeout");
    }

    /**
     * Ritorna true se è impostato un timeout e tale limite è stato superato
     * @return Se è impostato un timeout e tale limite è stato superato
     */
    private boolean isTimeOut() {
        return allowedTimePerMove != -1 && (System.currentTimeMillis() - startTimeMillis > allowedTimePerMove);
    }
}

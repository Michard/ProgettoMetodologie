package gapp.ulg.game.util;

import gapp.ulg.game.GameFactory;
import gapp.ulg.game.PlayerFactory;
import gapp.ulg.game.board.GameRuler;
import gapp.ulg.game.board.Player;

import java.nio.file.Path;

/**
 * Un {@code PlayerSlot} contiene le informazioni per uno "slot giocatore".
 * <p>
 * La maggior parte dei giocatori è impostabile tramite i metodi di {@code PlayGUI}
 * operanti su {@link PlayerFactory}. Sarebbe quindi possibile disporre di una
 * lista o mappa di Factory, dalle quali all'inizio del gioco si può ricavare
 * la corrispondente lista/mappa di Player.
 * <p>
 * Due sono i problemi che spingono all'uso della classe PlayerSlot:
 * <ol>
 * <li>
 * L'interfaccia {@code PlayerFactory} non dispone di un metodo setName o similare,
 * ma richiede il nome del gicatore al momento della creazione dello stesso
 * (metodo {@link PlayerFactory#newPlayer(GameFactory, String)}. La struttura della
 * classe {@link PlayGUI} invece richiede l'impostazione del nome del giocatore e la creazione
 * dello stesso in due momenti differenti (rispettivamente metodi
 * {@link PlayGUI#setPlayerFactory(int, String, String, Path)} e {@link PlayGUI#play(long, long, long, int, int, int)},
 * rendendo necessaria una struttura dati per mantenere i nomi dei giocatori fino al momento della loro creazione.
 * PlayerSlot fornisce la soluzione tramite il metodo {@link PlayerSlot#set(String)}.
 * </li>
 * <li>
 * Non tutti i giocatori dispongono di PlayerFactory (in particolare, {@link PlayerGUI}.
 * Si rende quindi necessario, a meno dell'uso di un contenitore di Object,
 * un contenitore parallelo a quello delle PlayerFactory, per gestire questi casi.
 * </li>
 * </ol>
 *
 * @param <P> Tipo del modello dei pezzi
 */
class PlayerSlot<P> {
    /**
     * Il tipo dell'oggetto che occupa uno slot
     */
    enum State {
        PLAYER, FACTORY
    }

    /**
     * Il tipo dell'oggetto che occupa questo slot
     */
    private State state;
    /**
     * Il Player impostato per questo slot
     */
    private Player<P> player;
    /**
     * La PlayerFactory impostata per questo slot
     */
    private PlayerFactory<Player<P>, GameRuler<P>> factory;
    /**
     * Il nome del giocatore se è impostata una PlayerFactory,
     * altrimenti non usato
     */
    private String playerName;

    public PlayerSlot(Player<P> p) {
        set(p);
    }

    public PlayerSlot(PlayerFactory<Player<P>, GameRuler<P>> f) {
        set(f);
    }

    void set(Player<P> p) {
        state = State.PLAYER;
        this.player = p;
        playerName = null;
    }

    void set(PlayerFactory<Player<P>, GameRuler<P>> f) {
        state = State.FACTORY;
        this.factory = f;
        playerName = null;
    }

    void set(String n) {
        playerName = n;
    }

    Player<P> getPlayer() {
        return player;
    }

    PlayerFactory<Player<P>, GameRuler<P>> getFactory() {
        return factory;
    }

    String getName() { return playerName; }

    State getState() {
        return state;
    }
}

package gapp.ulg.play;

import java.io.Serializable;

/**
 * Identifica il risultato di una partita in un gioco con esattamente
 * due giocatori.
 */
enum BinaryGameResult implements Serializable {
    /**
     * Pareggio
     */
    TIE,
    /**
     * Vittoria del primo giocatore
     */
    FIRST_PLAYER,
    /**
     * Vittoria del secondo giocatore
     */
    SECOND_PLAYER;
}

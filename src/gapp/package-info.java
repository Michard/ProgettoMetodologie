/**
 * Package per contenere l'intero progetto. Contiene correntemente i package {@link gapp.ulg}, che contiene
 * l'implementazione delle classi del framework, e {@link gapp.gui}, che fornisce una GUI basata sulle
 * funzionalità del framework.
 */
package gapp;
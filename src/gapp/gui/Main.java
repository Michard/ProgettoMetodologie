package gapp.gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe principale del progetto
 */
public class Main extends Application {

    /**
     * Main dell'intera applicazione
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        MainMenu mainMenu = new MainMenu(primaryStage);
        Scene primaryScene = new Scene(mainMenu.get(), GuiConstants.WINDOW_WIDTH, GuiConstants.WINDOW_HEIGHT);
        primaryStage.setTitle("Progetto Metodologie di Programmazione");
        primaryStage.setScene(primaryScene);

        PlayScreen playScreen = new PlayScreen(primaryStage);
        PlaySettingsMenu playSettingsMenu = new PlaySettingsMenu(primaryStage, playScreen.getObserver());
        SettingsMenu settingsMenu = new SettingsMenu(primaryStage);
        CreditsScreen creditsScreen = new CreditsScreen(primaryStage, this);

        mainMenu.setPlaySettingsMenu(playSettingsMenu);
        mainMenu.setSettingsMenu(settingsMenu);
        mainMenu.setCreditsScreen(creditsScreen);
        playSettingsMenu.setBack(mainMenu);
        playSettingsMenu.setSettingsMenu(settingsMenu);
        playSettingsMenu.setPlayScreen(playScreen);
        settingsMenu.setBack(mainMenu);
        creditsScreen.setBack(mainMenu);
        playScreen.setMainMenu(mainMenu);

        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }
}
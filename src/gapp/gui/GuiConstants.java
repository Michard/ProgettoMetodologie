package gapp.gui;

/**
 * Questa classe contiene parte delle costanti usate da PlayGUI e dalla GUI dell'applicazione.
 *
 * Una volta racchiuse tutte le costanti della GUI in questa classe, può risultare più facile
 * sia modificarle, sia creare la possibilità di supportare lingue diverse, ad esempio sostituendo
 * alle costanti delle {@code Function<String, Language>}, con Language una classe o enum
 * apposita.
 */
public class GuiConstants {
    /* Larghezza della finestra */
    public static final double WINDOW_WIDTH = 800;

    /* Altezza della finestra */
    public static final double WINDOW_HEIGHT = 600;

    /** Messaggi per le eccezioni */

    public static final String PGUI_EXC_ALREADY_PLAYING = "È già in corso una partita.";
    public static final String PGUI_EXC_NOT_A_GAMEFACTORY = " non è il nome di una GameFactory.";
    public static final String PGUI_EXC_GAMEFACTORY_CREATION_FAILED = "Creazione della GameFactory fallita.";
    public static final String PGUI_EXC_GAMEFACTORY_NOT_SET = "GameFactory non impostata.";
    public static final String PGUI_EXC_PLAYERFACTORY_NOT_SET = "PlayerFactory per questo giocatore non impostata.";
    public static final String PGUI_EXC_NOT_A_GAMEFACTORY_PARAMETER = " non è un parametro della GameFactory.";
    public static final String PGUI_EXC_NOT_A_PLAYERFACTORY_PARAMETER = " non è un parametro della PlayerFactory";
    public static final String PGUI_EXC_PLAYERFACTORY_ALREADY_SET = "È già stata impostata una PlayerFactory.";
    public static final String PGUI_EXC_INVALID_INDEX = "Indice di turnazione non valido per questo gioco.";
    public static final String PGUI_EXC_IS_A_PLAYERGUI = " è un PlayerGUI";

    /** Vari */

    // Il messaggio passato da PlayGUI all'Observer alla chiamata di interrupted in stop
    public static final String PGUI_INTERRUPTED = "Partita interrotta.";

    public static final String PGUI_TIMEOUT_ERROR = "Errore timeout.";
    public static final String PGUI_ALREADY_TIMING_A_METHOD = "È già in esecuzione il controllo di un'esecuzione temporizzata";
}

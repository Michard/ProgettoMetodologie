package gapp.gui;

import gapp.ulg.game.GameFactory;
import gapp.ulg.game.Param;
import gapp.ulg.game.PlayerFactory;
import gapp.ulg.game.board.GameRuler;
import gapp.ulg.game.board.PieceModel;
import gapp.ulg.game.board.Player;
import gapp.ulg.games.GameFactories;
import gapp.ulg.play.PlayerFactories;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

class SettingsMenu {
    private StringProperty dirPath = new SimpleStringProperty(null);
    private BooleanProperty isTryComputing = new SimpleBooleanProperty(false);
    private GameFactory<GameRuler<PieceModel<PieceModel.Species>>> gF;
    private List<Object> gFParamValues = new ArrayList<>();
    private PlayerFactory<Player<PieceModel<PieceModel.Species>>, GameRuler<PieceModel<PieceModel.Species>>> pF;
    private BooleanProperty canTryCompute = new SimpleBooleanProperty(false);

    // Classe fornita per interrompere il calcolo della strategia
    private static class TryComputeInterrupt implements Supplier<Boolean> {
        volatile boolean interrupt = false;

        public void set(boolean b) {
            interrupt = b;
        }

        @Override
        public Boolean get() {
            return interrupt;
        }
    }

    // Istanza dell'oggetto interruttore
    private static TryComputeInterrupt tCInterrupt = new TryComputeInterrupt();

    private Button
            dirChooserButton = new Button("Scegli cartella..."),
            tryComputeStartButton = new Button("Calcola"),
            tryComputeStopButton = new Button("Interrompi"),
            backButton = new Button("Indietro");
    private ChoiceBox<String>
            gFChoiceBox = new ChoiceBox<>(FXCollections.observableArrayList(Arrays.asList(GameFactories.availableBoardFactories()))),
            gFParamChoiceBox = new ChoiceBox<>(),
            gFParamValueChoiceBox = new ChoiceBox<>(),
            pFChoiceBox = new ChoiceBox<>(FXCollections.observableArrayList(Arrays.asList(PlayerFactories.availableBoardFactories())));
    private Label
            tCOutputLabel = new Label("");

    private Spinner<Integer> maxBlockTimeSpinner =
            new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(1000, 60 * 60 * 1000, 1000, 100));
    private CheckBox maxBlockTimeCheckBox = new CheckBox("Nessun limite");

    private VBox layout;

    private MainMenu mainMenu;

    SettingsMenu(Stage primaryStage) {
        //Try compute
        Label tryComputeTitleLabel = new Label("Calcolo strategie");
        final String NO_DIRECTORY_CHOSEN = "Nessuna cartella impostata";
        Label chosenDirectoryLabel = new Label(NO_DIRECTORY_CHOSEN);
        chosenDirectoryLabel.setAlignment(Pos.CENTER_LEFT);
        chosenDirectoryLabel.setMaxWidth(100.0);
        chosenDirectoryLabel.setMaxHeight(100.0);
        chosenDirectoryLabel.setWrapText(true);
        dirChooserButton.setOnAction(e -> {
            DirectoryChooser dirChooser = new DirectoryChooser();
            dirChooser.setTitle("Scegli la cartella in cui salvare le strategie");
            File dirFile = dirChooser.showDialog(primaryStage);
            if (dirFile != null) {
                dirPath.set(dirFile.getPath());
                gFChoiceBox.setDisable(false);
                pFChoiceBox.setDisable(false);
                chosenDirectoryLabel.setText(dirPath.getValue());
            }
        });
        dirChooserButton.setMinWidth(100.0);
        gFChoiceBox.getSelectionModel().selectedItemProperty().addListener((obs, oldV, newV) -> {
            if (newV != null) {
                gF = GameFactories.getBoardFactory(newV);
                ObservableList<String> entries = FXCollections.observableArrayList(
                        gF.params().stream().map(Param::name).collect(Collectors.toList())
                );
                if (entries.size() > 0) {
                    gFParamChoiceBox.setItems(entries);
                    gFParamChoiceBox.setDisable(false);
                }
                gFParamValueChoiceBox.setDisable(true);

                dirChooserButton.setDisable(true);
                updateCanTryCompute();
                updateCanTryMessage();
            }
        });
        gFChoiceBox.setDisable(true);
        gFParamChoiceBox.getSelectionModel().selectedIndexProperty().addListener((obs, oldIndex, newIndex) -> {
            if (newIndex.intValue() != -1) {
                gFParamValues = gF.params().get(newIndex.intValue()).values().stream().collect(Collectors.toList());
                gFParamValueChoiceBox.setItems(FXCollections.observableArrayList(
                        gFParamValues.stream().map(Object::toString).collect(Collectors.toList())
                ));
                gFParamValueChoiceBox.setDisable(false);

                dirChooserButton.setDisable(true);
            }
        });
        gFParamChoiceBox.setMinWidth(100.0);
        gFParamChoiceBox.setDisable(true);
        gFParamValueChoiceBox.getSelectionModel().selectedIndexProperty().addListener((obs, oldIndex, newIndex) -> {
            if (newIndex.intValue() != -1) {
                gF.params()
                        .get(gFParamChoiceBox.getSelectionModel().getSelectedIndex())
                        .set(gFParamValues.get(newIndex.intValue()));
                updateCanTryCompute();
                updateCanTryMessage();
            }
        });
        gFParamValueChoiceBox.setMinWidth(100.0);
        gFParamValueChoiceBox.setDisable(true);
        pFChoiceBox.setMinWidth(100.0);
        pFChoiceBox.setDisable(true);
        pFChoiceBox.setItems(FXCollections.observableList(
                Arrays.asList(PlayerFactories.availableBoardFactories())
        ));
        pFChoiceBox.getSelectionModel().selectedItemProperty().addListener((obs, oldV, newV) -> {
            if (newV != null) {
                pF = PlayerFactories.getBoardFactory(newV);
                pF.setDir(Paths.get(dirPath.get()));
            }
            updateCanTryCompute();
            updateCanTryMessage();
        });
        tCOutputLabel.setAlignment(Pos.CENTER);

        tryComputeStartButton.disableProperty().bind(
                Bindings.or(Bindings.or(Bindings.or(
                        isTryComputing,
                        dirPath.isNull()),
                        gFChoiceBox.valueProperty().isNull()),
                        Bindings.not(canTryCompute)
                )
        );
        tryComputeStartButton.setOnAction(e -> {
            if (!isTryComputing.get()) {
                isTryComputing.set(true);
                Thread t = new Thread(() -> {
                    tCInterrupt.set(false);
                    try {
                        if (pF.tryCompute(gF, true, tCInterrupt) == null) {
                            Platform.runLater(() -> {
                                tCOutputLabel.setText("Calcolo completato!");
                                tCOutputLabel.setStyle("-fx-text-fill: #00ff00");
                            });
                        } else {
                            Platform.runLater(() -> {
                                tCOutputLabel.setText("Calcolo interrotto!");
                                tCOutputLabel.setStyle("-fx-text-fill: #ff0000");
                            });
                        }
                    } catch (Throwable th) {
                        Platform.runLater(() -> {
                            tCOutputLabel.setText("Errore durante il calcolo!");
                            tCOutputLabel.setStyle("-fx-text-fill: #ff0000");
                        });
                    }

                    Platform.runLater(() -> {
                        isTryComputing.set(false);
                        updateCanTryCompute();

                        dirChooserButton.setDisable(false);
                        gFChoiceBox.setDisable(false);
                        gFParamChoiceBox.setDisable(false);
                        gFParamValueChoiceBox.setDisable(false);
                        pFChoiceBox.setDisable(false);
                    });
                });

                dirChooserButton.setDisable(true);
                gFChoiceBox.setDisable(true);
                gFParamChoiceBox.setDisable(true);
                gFParamValueChoiceBox.setDisable(true);
                pFChoiceBox.setDisable(true);

                t.setDaemon(true);
                t.start();
            }
        });
        tryComputeStartButton.setMinWidth(100.0);
        tryComputeStopButton.setOnAction(e -> {
            tCInterrupt.set(true);
        });
        tryComputeStopButton.visibleProperty().bind(isTryComputing);

        VBox dirChoiceLayout = new VBox(dirChooserButton, chosenDirectoryLabel);
        dirChoiceLayout.setAlignment(Pos.CENTER);
        dirChoiceLayout.setSpacing(25.0);
        HBox gFParamLayout = new HBox(gFParamChoiceBox, gFParamValueChoiceBox);
        gFParamLayout.setSpacing(0);
        gFParamLayout.setAlignment(Pos.BOTTOM_CENTER);
        VBox factoriesLayout = new VBox(
                new Label("GameFactory"),
                gFChoiceBox,
                new Label("Parametri"),
                gFParamLayout,
                new Label("PlayerFactory"),
                pFChoiceBox
        );
        factoriesLayout.setAlignment(Pos.BOTTOM_CENTER);
        VBox tCStartAndStopButtonsLayout = new VBox(tryComputeStartButton, tryComputeStopButton);
        tCStartAndStopButtonsLayout.setAlignment(Pos.CENTER);
        tCStartAndStopButtonsLayout.setSpacing(25.0);
        HBox tryComputeInputsLayout = new HBox(
                dirChoiceLayout,
                factoriesLayout,
                tCStartAndStopButtonsLayout
        );
        tryComputeInputsLayout.setAlignment(Pos.CENTER);
        tryComputeInputsLayout.setSpacing(40.0);
        VBox tryComputeLayout = new VBox(tryComputeTitleLabel, tryComputeInputsLayout, tCOutputLabel);
        tryComputeLayout.setAlignment(Pos.CENTER);
        tryComputeLayout.setSpacing(20.0);

        //MaxBlockTime
        maxBlockTimeCheckBox.setSelected(true);
        maxBlockTimeSpinner.disableProperty().bind(maxBlockTimeCheckBox.selectedProperty());
        VBox maxBlockTimeLayout = new VBox(
                new Label("MaxBlockTime"),
                new HBox(maxBlockTimeSpinner, maxBlockTimeCheckBox) {{
                    setAlignment(Pos.CENTER);
                    setSpacing(20.0);
                }}
        ) {{
            setAlignment(Pos.CENTER);
            setSpacing(20.0);
        }};

        //Indietro
        backButton.setOnAction(e -> {
            primaryStage.getScene().setRoot(mainMenu.get());
            reset();
        });
        backButton.disableProperty().bind(
                tryComputeStopButton.visibleProperty()
        );

        //Layout completo
        List<Separator> separators = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Separator separator = new Separator();
            separator.maxWidthProperty().bind(Bindings.max(0, primaryStage.widthProperty().subtract(200.0)));
            separators.add(separator);
        }
        layout = new VBox(
                tryComputeLayout,
                separators.get(0),
                maxBlockTimeLayout,
                separators.get(1),
                backButton
        );
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(30.0);
    }

    /**
     * Ritorna il layout di questa schermata.
     * @return Il layout di questa schermata
     */
    public Parent get() {
        return layout;
    }

    /**
     * Imposta l'istanza del menu principale.
     * @param mainMenu Il menu principale
     */
    void setBack(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    /**
     * @return La directory impostata per le operazioni delle {@link PlayerFactory}.
     */
    public String getDir() {
        return dirPath.getValue();
    }

    /**
     * Resituisce il valore impostato per MaxBlockTime
     */
    long getMaxBlockTime() {
        return maxBlockTimeCheckBox.isSelected() ? maxBlockTimeSpinner.getValue() : -1;
    }

    /**
     * Riporta alle impostazioni predefinite
     */
    private void reset() {
        dirChooserButton.setDisable(false);

        gF = null;
        pF = null;

        gFChoiceBox.getSelectionModel().clearSelection();
        gFChoiceBox.setDisable(true);
        gFParamChoiceBox.getSelectionModel().clearSelection();
        gFParamChoiceBox.setDisable(true);
        gFParamValueChoiceBox.getSelectionModel().clearSelection();
        gFParamValueChoiceBox.setDisable(true);
        pFChoiceBox.getSelectionModel().clearSelection();
        pFChoiceBox.setDisable(true);

        tCOutputLabel.setText("");
    }

    /**
     * In base allo stato della schermata e dei controlli scelti dall'utente,
     * attiva o disattiva la possibilità di iniziare il tryCompute.
     */
    private void updateCanTryCompute() {
        canTryCompute.setValue(gFChoiceBox.getValue() != null &&
                !isTryComputing.get() &&
                pFChoiceBox.getValue() != null &&
                pF.canPlay(gF).equals(PlayerFactory.Play.TRY_COMPUTE)
        );
    }

    /**
     * In base allo stato della schermata e dei controlli scelti dall'utene,
     * imposta o cancella il messaggio di output relativo alla possibilità
     * di effettuare il tryCompute.
     */
    private void updateCanTryMessage() {
        if (gF != null && pF != null) {
            switch (pF.canPlay(gF)) {
                case YES:
                    tCOutputLabel.setStyle("-fx-text-fill: #ff0000");
                    tCOutputLabel.setText("Il giocatore sa già giocare a questo gioco");
                    break;
                case NO:
                    tCOutputLabel.setStyle("-fx-text-fill: #ff0000");
                    tCOutputLabel.setText("Il giocatore non può giocare a questo gioco");
                    break;
                default:
                    tCOutputLabel.setText("");
                    break;
            }
        }
    }
}

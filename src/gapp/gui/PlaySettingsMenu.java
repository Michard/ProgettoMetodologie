package gapp.gui;

import gapp.ulg.game.PlayerFactory;
import gapp.ulg.game.board.PieceModel;
import gapp.ulg.game.util.*;
import gapp.ulg.games.GameFactories;
import gapp.ulg.play.PlayerFactories;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Classe che agisce da front-end per {@link PlayGUI} fornendo
 * l'interfaccia attraverso la quale avviene l'impostazione del
 * gioco.
 */
class PlaySettingsMenu {
    private SharedReference<PlayGUI<PieceModel<PieceModel.Species>>>
            playGUI = new SharedReference<>();

    private ChoiceBox<String>
            gFChoiceBox = new ChoiceBox<>(),
            gFParamChoiceBox = new ChoiceBox<>(),
            gFParamValueChoiceBox = new ChoiceBox<>();
    private List<Object>
            gFParamValues = new ArrayList<>();
    private List<ChoiceBox<String>>
            pFChoiceBoxes = Arrays.asList(new ChoiceBox<String>(), new ChoiceBox<String>()),
            pFParamChoiceBoxes = Arrays.asList(new ChoiceBox<String>(), new ChoiceBox<String>()),
            pFParamValueChoiceBoxes = Arrays.asList(new ChoiceBox<String>(), new ChoiceBox<String>());
    private List<String>
            pFDirectories = new ArrayList<>();
    private List<Button>
            pFSetDirButtons = new ArrayList<>();
    private List<Label>
            cantPlayLabels = new ArrayList<>();
    private List<TextField>
            pFNameTextFields = new ArrayList<>();
    private List<List<Object>>
            pFParamValuesLists = Arrays.asList(new ArrayList<>(), new ArrayList<>());
    private List<VBox>
            pFLayouts = new ArrayList<>();
    private CheckBox
            timeoutCheckBox = new CheckBox("Nessun limite"),
            threadsCheckBox = new CheckBox("Nessun limite"),
            fjpSizeCheckBox = new CheckBox("Nessun limite"),
            bgExecSizeCheckBox = new CheckBox("Nessun limite");
    private Spinner<Integer>
            timeoutSpinner = makeIntSpinner(100, MILLIS_IN_HOUR, 100, 100),
            toleranceSpinner = makeIntSpinner(0, MILLIS_IN_HOUR, 0, 10),
            minTimeSpinner = makeIntSpinner(0, MILLIS_IN_HOUR, 0, 100),
            threadsSpinner = makeIntSpinner(0, 99, 0, 1),
            fjpSizeSpinner = makeIntSpinner(0, 99, 0, 1),
            bgExecSizeSpinner = makeIntSpinner(0, 99, 0, 1);
    private Button
            backButton = new Button("Indietro"),
            resetButton = new Button("Reset"),
            playButton = new Button("Gioca");

    private VBox layout;

    private Stage stage;
    private MainMenu mainMenu;
    private SettingsMenu settingsMenu;
    private PlayScreen playScreen;
    private PlayGUI.Observer<PieceModel<PieceModel.Species>> obs;

    //Numero di millisecondi in un'ora
    private static final int MILLIS_IN_HOUR = 60 * 60 * 1_000;

    @SuppressWarnings("unchecked")
    PlaySettingsMenu(Stage primaryStage, PlayGUI.Observer<PieceModel<PieceModel.Species>> obs) {
        this.stage = primaryStage;

        this.obs = obs;
        playGUI.set(new PlayGUI<>(obs, settingsMenu != null ? settingsMenu.getMaxBlockTime() : -1));

        // Selezione della GameFactory
        Label gFLabel = new Label("Gioco");
        gFChoiceBox.setItems(FXCollections.observableArrayList(GameFactories.availableBoardFactories()));
        gFChoiceBox.getSelectionModel().selectedItemProperty().addListener((o, oldV, newV) -> {
            if (newV != null) {
                playGUI.get().setGameFactory(newV);

                gFParamChoiceBox.setItems(FXCollections.observableArrayList(playGUI.get().getGameFactoryParams()));
                gFParamChoiceBox.setDisable(false);

                gFParamValueChoiceBox.setItems(FXCollections.emptyObservableList());
                gFParamValueChoiceBox.setDisable(true);

                ObservableList pFNames = FXCollections.observableArrayList("PlayerGUI", new Separator());
                Collections.addAll(pFNames, PlayerFactories.availableBoardFactories());
                pFChoiceBoxes.forEach(cBox -> {
                    cBox.setItems(pFNames);
                    cBox.setDisable(false);
                });

                pFSetDirButtons.forEach(b -> b.setDisable(false));
                pFNameTextFields.forEach(f -> f.setDisable(false));
            }
        });
        VBox gFChoiceLayout = new VBox(gFLabel, gFChoiceBox);
        gFChoiceLayout.setAlignment(Pos.CENTER);

        //Parametri della GameFactory
        Label gFParamLabel = new Label("Parametri");
        gFParamChoiceBox.setMinWidth(100.0);
        gFParamChoiceBox.setDisable(true);
        gFParamValueChoiceBox.setMinWidth(100.0);
        gFParamValueChoiceBox.setDisable(true);

        gFParamChoiceBox.getSelectionModel().selectedItemProperty().addListener((o, oldV, newV) -> {
            if (newV != null) {
                gFParamChoiceBox.setTooltip(new Tooltip(
                        playGUI.get().getGameFactoryParamPrompt(newV)
                ));

                gFParamValues.clear();
                gFParamValues.addAll(Arrays.asList(playGUI.get().getGameFactoryParamValues(newV)));
                gFParamValueChoiceBox.setItems(FXCollections.observableArrayList(
                        gFParamValues
                                .stream()
                                .map(Object::toString)
                                .toArray(String[]::new)
                ));
                gFParamValueChoiceBox.setDisable(false);
            }
        });
        gFParamValueChoiceBox.getSelectionModel().selectedIndexProperty().addListener((o, oldV, newV) -> {
            if (newV.intValue() != -1)
                playGUI.get().setGameFactoryParamValue(gFParamChoiceBox.getValue(), gFParamValues.get(newV.intValue()));
        });

        HBox gFParamChoiceBoxesLayout = new HBox(gFParamChoiceBox, gFParamValueChoiceBox);
        gFParamChoiceBoxesLayout.setAlignment(Pos.CENTER);
        VBox gFParamLayout = new VBox(gFParamLabel, gFParamChoiceBoxesLayout);
        gFParamLayout.setAlignment(Pos.BOTTOM_CENTER);

        VBox gFLayout = new VBox(gFChoiceLayout, gFParamLayout);

        //Scelta delle PlayerFactory
        for (int i = 0; i < 2; i++) {
            ChoiceBox<String>
                    pFChoiceBox = pFChoiceBoxes.get(i),
                    pFParamChoiceBox = pFParamChoiceBoxes.get(i),
                    pFParamValueChoiceBox = pFParamValueChoiceBoxes.get(i);
            Button pFSetDirButton = new Button("Scegli cartella");
            TextField nameField = new TextField();
            List<Object>
                    pFParamValuesList = pFParamValuesLists.get(i);
            final int pFIndex = i + 1;

            Label cantPlayLabel = new Label();
            cantPlayLabel.setAlignment(Pos.CENTER);
            cantPlayLabel.setStyle("-fx-text-fill: #ff0000");
            cantPlayLabel.setTextAlignment(TextAlignment.CENTER);
            cantPlayLabel.setWrapText(true);
            cantPlayLabel.setMaxWidth(200.0);
            cantPlayLabel.setMinHeight(50.0);
            cantPlayLabel.setMaxHeight(50.0);
            cantPlayLabels.add(cantPlayLabel);

            pFChoiceBox.setDisable(true);
            pFChoiceBox.setMinWidth(200.0);
            pFChoiceBox.getSelectionModel().selectedIndexProperty().addListener((o, oldV, newV) -> {
                int index = newV.intValue();
                if (index != -1) {
                    String name = pFNameTextFields.get(pFIndex - 1).getCharacters().toString();
                    if (name.equals("")) {
                        name = "Giocatore " + pFIndex;
                    }
                    boolean factorySelected = true;
                    if (index == 0) {
                        playGUI.get().setPlayerGUI(pFIndex, name, playScreen.getMaster());
                        pFSetDirButton.setDisable(true);
                        nameField.setDisable(true);
                        pFParamChoiceBox.setItems(FXCollections.emptyObservableList());
                        pFParamChoiceBox.setDisable(true);
                    } else {
                        String dir = pFDirectories.get(pFIndex - 1);
                        PlayerFactory.Play canPlay =
                                playGUI.get().setPlayerFactory(
                                        pFIndex,
                                        pFChoiceBox.getItems().get(index),
                                        name,
                                        dir != null ? Paths.get(dir) : null
                                );
                        if (canPlay.equals(PlayerFactory.Play.YES)) {
                            pFParamChoiceBox.setItems(FXCollections.observableArrayList(playGUI.get().getPlayerFactoryParams(pFIndex)));
                            pFParamChoiceBox.setDisable(pFParamChoiceBox.getItems().size() == 0);
                            pFSetDirButton.setDisable(true);
                            nameField.setDisable(true);
                        } else {
                            factorySelected = false;
                            //Il testo è ricalcolato ogni volta e non può essere impostato a priori
                            String errorMessage = "Non sa " +
                                    (canPlay.equals(PlayerFactory.Play.TRY_COMPUTE) ? "correntemente " : "") +
                                    "giocare al gioco scelto.";
                            cantPlayLabel.setText(errorMessage);
                            cantPlayLabel.setVisible(true);
                            pFParamChoiceBox.setDisable(true);
                            pFParamValueChoiceBox.setDisable(true);
                        }
                    }
                    if (factorySelected) {
                        gFChoiceBox.setDisable(true);
                        gFParamChoiceBox.setDisable(true);
                        gFParamValueChoiceBox.setDisable(true);
                        cantPlayLabel.setVisible(false);
                    }
                    pFParamValueChoiceBox.setItems(FXCollections.emptyObservableList());
                    pFParamValueChoiceBox.setDisable(true);
                } else {
                    cantPlayLabel.setVisible(false);
                }
            });
            pFParamChoiceBox.setMinWidth(70.0);
            pFParamChoiceBox.setDisable(true);
            pFParamChoiceBox.getSelectionModel().selectedItemProperty().addListener((o, oldV, newV) -> {
                if (newV != null) {
                    pFParamChoiceBox.setTooltip(new Tooltip(playGUI.get().getPlayerFactoryParamPrompt(pFIndex, newV)));
                    pFParamValuesList.clear();
                    pFParamValuesList.addAll(Arrays.asList(playGUI.get().getPlayerFactoryParamValues(pFIndex, newV)));
                    pFParamValueChoiceBox.setItems(FXCollections.observableArrayList(
                            Arrays.asList(playGUI.get().getPlayerFactoryParamValues(pFIndex, newV))
                                    .stream()
                                    .map(Object::toString)
                                    .collect(Collectors.toList())
                    ));
                    pFParamValueChoiceBox.setDisable(false);
                }
            });
            pFParamValueChoiceBox.setMinWidth(60.0);
            pFParamValueChoiceBox.setDisable(true);
            pFParamValueChoiceBox.getSelectionModel().selectedIndexProperty().addListener((o, oldI, newI) -> {
                if (newI.intValue() != -1)
                    playGUI.get().setPlayerFactoryParamValue(pFIndex, pFParamChoiceBox.getValue(), pFParamValuesList.get(newI.intValue()));
            });

            pFSetDirButton.setDisable(true);
            pFSetDirButton.setWrapText(true);
            pFSetDirButton.setOnAction(e -> {
                DirectoryChooser dirChooser = new DirectoryChooser();
                dirChooser.setTitle("Scegli la cartella");
                File dirPath = dirChooser.showDialog(primaryStage);
                pFDirectories.set(pFIndex - 1, dirPath != null ? dirPath.getPath() : null);
            });
            pFDirectories.add(null);
            pFSetDirButtons.add(pFSetDirButton);

            nameField.setDisable(true);
            nameField.setPromptText("Nome giocatore " + pFIndex);
            pFNameTextFields.add(nameField);

            HBox pFParamBoxesLayout = new HBox(pFParamChoiceBox, pFParamValueChoiceBox);
            pFParamBoxesLayout.setAlignment(Pos.CENTER);
            VBox pFLayout = new VBox(
                    new Label(Integer.toString(pFIndex) + "° giocatore"),
                    pFChoiceBox,
                    new HBox(new Label("Scegli cartella"), pFSetDirButton) {{
                        setAlignment(Pos.CENTER);
                        setSpacing(40.0);
                    }},
                    new HBox(new Label("Nome"), nameField) {{
                        setAlignment(Pos.CENTER);
                        setSpacing(20.0);
                    }},
                    new Label("Parametri"),
                    pFParamBoxesLayout,
                    cantPlayLabel
            );
            pFLayout.setAlignment(Pos.CENTER);
            pFLayouts.add(pFLayout);
        }
        HBox pFLayout = new HBox(pFLayouts.toArray(new VBox[pFLayouts.size()]));
        pFLayout.setAlignment(Pos.CENTER);
        pFLayout.setSpacing(100.0);

        //Gestione threads
        threadsCheckBox.setSelected(false);
        fjpSizeCheckBox.setSelected(false);
        bgExecSizeCheckBox.setSelected(false);
        BooleanBinding noCPUPlayers = Bindings.not(Bindings.or(
                Bindings.greaterThanOrEqual(pFChoiceBoxes.get(0).getSelectionModel().selectedIndexProperty(), 2),
                Bindings.greaterThanOrEqual(pFChoiceBoxes.get(1).getSelectionModel().selectedIndexProperty(), 2)
        )); // Se è vero che non ci sono giocatori controllati dal computer
        threadsCheckBox.disableProperty().bind(noCPUPlayers);
        threadsSpinner.disableProperty().bind(Bindings.or(
                threadsCheckBox.selectedProperty(),
                noCPUPlayers
        ));
        fjpSizeCheckBox.disableProperty().bind(noCPUPlayers);
        fjpSizeSpinner.disableProperty().bind(Bindings.or(
                fjpSizeCheckBox.selectedProperty(),
                noCPUPlayers
        ));
        bgExecSizeCheckBox.disableProperty().bind(noCPUPlayers);
        bgExecSizeSpinner.disableProperty().bind(Bindings.or(
                bgExecSizeCheckBox.selectedProperty(),
                noCPUPlayers
        ));
        VBox threadsLayout = new VBox(
                new Label("Gestione threads"),
                new HBox(
                        new VBox(
                                new Label("Threads aggiuntivi") {{
                                    setAlignment(Pos.CENTER);
                                }}, threadsSpinner, threadsCheckBox
                        ) {{
                            setAlignment(Pos.CENTER);
                        }},
                        new VBox(
                                new Label("Dimensione ForkJoinPool") {{
                                    setAlignment(Pos.CENTER);
                                }}, fjpSizeSpinner, fjpSizeCheckBox
                        ) {{
                            setAlignment(Pos.CENTER);
                        }},
                        new VBox(
                                new Label("Dimensione Esecutore in background") {{
                                    setAlignment(Pos.CENTER);
                                }}, bgExecSizeSpinner, bgExecSizeCheckBox
                        ) {{
                            setAlignment(Pos.CENTER);
                        }}
                ) {{
                    setAlignment(Pos.CENTER);
                    setSpacing(20.0);
                }}
        );
        threadsLayout.setAlignment(Pos.CENTER);
        threadsLayout.setSpacing(15.0);

        //Gestione tempi
        timeoutCheckBox.setSelected(true);
        timeoutSpinner.disableProperty().bind(timeoutCheckBox.selectedProperty());
        minTimeSpinner.disableProperty().bind(
                Bindings.or(
                        Bindings.equal(pFChoiceBoxes.get(0).getSelectionModel().selectedIndexProperty(), 0),
                        Bindings.equal(pFChoiceBoxes.get(1).getSelectionModel().selectedIndexProperty(), 0)
                )
        );
        VBox timeLayout = new VBox(
                new Label("Gestione tempi"),
                new HBox(
                        new VBox(
                                new Label("Timeout") {{
                                    setAlignment(Pos.CENTER);
                                }}, timeoutSpinner, timeoutCheckBox
                        ) {{
                            setAlignment(Pos.CENTER);
                        }},
                        new VBox(
                                new Label("Tolleranza") {{
                                    setAlignment(Pos.CENTER);
                                }}, toleranceSpinner,
                                new CheckBox("") {{
                                    setVisible(false);
                                    setDisable(true);
                                }}
                        ) {{
                            setAlignment(Pos.CENTER);
                        }},
                        new VBox(
                                new Label("Intervallo tra le mosse") {{
                                    setAlignment(Pos.CENTER);
                                }}, minTimeSpinner,
                                new CheckBox("") {{
                                    setVisible(false);
                                    setDisable(true);
                                }}
                        ) {{
                            setAlignment(Pos.CENTER);
                        }}
                ) {{
                    setAlignment(Pos.CENTER);
                    setSpacing(20.0);
                }}
        );
        timeLayout.setAlignment(Pos.CENTER);
        timeLayout.setSpacing(15.0);

        //Layout pulsanti in basso
        backButton.setOnAction(e -> {
            stage.getScene().setRoot(mainMenu.get());
            reset();
        });
        resetButton.setOnAction(e -> reset());
        playButton.disableProperty().bind(
                Bindings.or(Bindings.or(Bindings.or(
                        cantPlayLabels.get(0).visibleProperty(),
                        cantPlayLabels.get(1).visibleProperty()),
                        Bindings.lessThan(pFChoiceBoxes.get(0).getSelectionModel().selectedIndexProperty(), 0)),
                        Bindings.lessThan(pFChoiceBoxes.get(1).getSelectionModel().selectedIndexProperty(), 0)
                )
        );
        playButton.setOnAction(e -> {
            playScreen.setArePlayerGUIs(
                    pFChoiceBoxes.get(0).getSelectionModel().getSelectedIndex() == 0,
                    pFChoiceBoxes.get(1).getSelectionModel().getSelectedIndex() == 0
            );
            playGUI.get().play(
                    toleranceSpinner.getValue(),
                    timeoutCheckBox.isSelected() ? -1 : timeoutSpinner.getValue(),
                    minTimeSpinner.isDisabled() ? 0 : minTimeSpinner.getValue(),
                    threadsCheckBox.isSelected() || noCPUPlayers.get() ? -1 : threadsSpinner.getValue(),
                    fjpSizeCheckBox.isSelected() || noCPUPlayers.get() ? -1 : fjpSizeSpinner.getValue(),
                    bgExecSizeCheckBox.isSelected() || noCPUPlayers.get() ? -1 : bgExecSizeSpinner.getValue()
            );
            reset();
        });

        //Miscellanee
        HBox miscLayout = new HBox(backButton, resetButton, playButton);
        miscLayout.setAlignment(Pos.BOTTOM_CENTER);
        miscLayout.setSpacing(100.0);

        //Layout completo
        layout = new VBox(
                gFLayout,
                pFLayout,
                new Separator(),
                threadsLayout,
                //new Separator(),
                timeLayout,
                new Separator(),
                miscLayout
        );
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(10.0);
    }

    /**
     * @return Il layout corrispondente a questo menu
     */
    public Parent get() {
        return layout;
    }

    /**
     * Imposta il menu principale di riferimento, a cui tornare alla pressione del pulsante indietro
     *
     * @param mainMenu Il menu principale
     */
    void setBack(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    /**
     * Imposta l'istanza del menu delle impostazioni
     *
     * @param settingsMenu Il menu delle impostazioni
     */
    void setSettingsMenu(SettingsMenu settingsMenu) {
        this.settingsMenu = settingsMenu;
    }

    /**
     * Imposta lo schermo di gioco a cui spostarsi alla pressione del pulsante Gioca
     *
     * @param playScreen Lo schermo di gioco
     */
    void setPlayScreen(PlayScreen playScreen) {
        this.playScreen = playScreen;
        this.playScreen.setPlayGUI(playGUI.get());
    }

    /**
     * Ritorna un oggetto {@link Spinner}  di valori compresi tra {@code min} e {@code max},
     * di valore iniziale {@code startValue} e con step di {@code step}.
     *
     * @param min        Valore minimo dello Spinner
     * @param max        Valore massimo dello Spinner
     * @param startValue Valore iniziale dello Spinner
     * @param step       Modifica minima del valore
     * @return Lo Spinner costruito
     */
    private static Spinner<Integer> makeIntSpinner(int min, int max, int startValue, int step) {
        return new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(min, max, startValue, step));
    }

    /**
     * Riporta la schermata alla situazione originale, svuotando tutti i campi e disattivando
     * quelli opportuni. Il {@link PlayGUI} sottostante resta immodificato, ma è nella pratica
     * resettato essendo chiamabile esclusivamente il suo metodo {@link PlayGUI#setGameFactory(String)}.
     */
    private void reset() {
        gFChoiceBox.getSelectionModel().select(null);
        gFChoiceBox.getSelectionModel().select(-1);
        gFChoiceBox.setDisable(false);
        gFParamChoiceBox.setItems(FXCollections.emptyObservableList());
        gFParamChoiceBox.setDisable(true);
        gFParamValueChoiceBox.setItems(FXCollections.emptyObservableList());
        gFParamValueChoiceBox.setDisable(true);

        for (int i = 0; i < pFChoiceBoxes.size(); i++) {
            pFChoiceBoxes.get(i).setItems(FXCollections.emptyObservableList());
            pFChoiceBoxes.get(i).setDisable(true);
            pFDirectories.set(i, null);
            pFSetDirButtons.get(i).setDisable(true);
            pFNameTextFields.get(i).setDisable(true);
            pFNameTextFields.get(i).setText("");
            pFParamChoiceBoxes.get(i).setItems(FXCollections.emptyObservableList());
            pFParamChoiceBoxes.get(i).setDisable(true);
            pFParamValueChoiceBoxes.get(i).setItems(FXCollections.emptyObservableList());
            pFParamValueChoiceBoxes.get(i).setDisable(true);
        }

        threadsCheckBox.setSelected(false);
        fjpSizeCheckBox.setSelected(false);
        bgExecSizeCheckBox.setSelected(false);
        threadsSpinner.getValueFactory().setValue(0);
        fjpSizeSpinner.getValueFactory().setValue(0);
        bgExecSizeSpinner.getValueFactory().setValue(0);

        timeoutCheckBox.setSelected(true);
        timeoutSpinner.getValueFactory().setValue(10);
        toleranceSpinner.getValueFactory().setValue(0);
        minTimeSpinner.getValueFactory().setValue(0);
    }
}

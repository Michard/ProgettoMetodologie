package gapp.gui;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

class CreditsScreen {
    private VBox layout; // Layout della schermata

    private MainMenu mainMenu; // Menu principale

    CreditsScreen(Stage primaryStage, Application app) {
        // Titolo

        Label titleLabel = new Label("Progetto Metodologie di Programmazione");
        titleLabel.setStyle("-fx-font-size: 250%");
        Label academicYearLabel = new Label("A.A. 2015 / 2016");
        academicYearLabel.setStyle("-fx-font-size: 150%");

        VBox titleLayout = new VBox(titleLabel, academicYearLabel) {{
            setAlignment(Pos.CENTER);
            setSpacing(10.0);
        }};

        // Nomi partecipanti

        Label creditsTitleLabel = new Label("Progetto ad opera di:");
        Label creditsNamesLabel = new Label("\u2022 Di Ronza Vincenzo - Matr. 1705322\n" +
                "\u2022 Dini Alessandro - Matr. 1705499\n" +
                "\u2022 Ferraro Michele - Matr. 1717025");
        creditsNamesLabel.setWrapText(true);
        VBox creditsLayout = new VBox(creditsTitleLabel, creditsNamesLabel);
        creditsLayout.setAlignment(Pos.CENTER);
        creditsLayout.setSpacing(20.0);

        // Ringraziamenti speciali

        Label specialThanksLabel = new Label("Un ringraziamento speciale al prof. Riccardo Silvestri per la " +
                "disponibiità e l'esperienza mostrate nella conduzione del corso.");
        specialThanksLabel.setTextAlignment(TextAlignment.CENTER);

        // Tecnologie usate

        Label externalTechsLabel = new Label("Un ringraziamento sentito va anche ai produttori di software gratuito per rendere " +
                "più semplice il processo di sviluppo.\n\n" +
                "Ringraziamo in particolare gli autori dei seguenti software, centrali nella creazione di questo progetto.");
        externalTechsLabel.setAlignment(Pos.CENTER);
        externalTechsLabel.setTextAlignment(TextAlignment.CENTER);
        externalTechsLabel.setWrapText(true);

        Image gradleLogoImage = new Image(getClass().getResource("logo_gradle.png").toString());
        Image gitLogoImage = new Image(getClass().getResource("logo_git.png").toString());
        Image gitlabLogoImage = new Image(getClass().getResource("logo_gitlab.png").toString());
        Image jenkinsLogoImage = new Image(getClass().getResource("logo_jenkins.png").toString());

        ImageView gradleLogo = new ImageView(gradleLogoImage);
        gradleLogo.setPickOnBounds(true);
        gradleLogo.setOnMouseClicked(e -> app.getHostServices().showDocument("https://gradle.org"));
        ImageView gitLogo = new ImageView(gitLogoImage);
        gitLogo.setPickOnBounds(true);
        gitLogo.setOnMouseClicked(e -> app.getHostServices().showDocument("https://git-scm.com"));
        ImageView gitlabLogo = new ImageView(gitlabLogoImage);
        gitlabLogo.setPickOnBounds(true);
        gitlabLogo.setOnMouseClicked(e -> app.getHostServices().showDocument("https://gitlab.com"));
        ImageView jenkinsLogo = new ImageView(jenkinsLogoImage);
        jenkinsLogo.setPickOnBounds(true);
        jenkinsLogo.setOnMouseClicked(e -> app.getHostServices().showDocument("https://jenkins.io"));

        final double LOGO_HEIGHT = 60.0;
        gradleLogo.setFitHeight(LOGO_HEIGHT);
        gradleLogo.setFitWidth(LOGO_HEIGHT * gradleLogoImage.getWidth() / gradleLogoImage.getHeight());
        gitLogo.setFitHeight(LOGO_HEIGHT);
        gitLogo.setFitWidth(LOGO_HEIGHT * gitLogoImage.getWidth() / gitLogoImage.getHeight());
        gitlabLogo.setFitHeight(LOGO_HEIGHT);
        gitlabLogo.setFitWidth(LOGO_HEIGHT * gitlabLogoImage.getWidth() / gitlabLogoImage.getHeight());
        jenkinsLogo.setFitHeight(LOGO_HEIGHT);
        jenkinsLogo.setFitWidth(LOGO_HEIGHT * jenkinsLogoImage.getWidth() / jenkinsLogoImage.getHeight());

        Label myunitLabel = new Label("MyUnit");
        myunitLabel.setStyle("-fx-font-family: serif; -fx-font-size: 60px");
        myunitLabel.setAlignment(Pos.CENTER);
        myunitLabel.setTextAlignment(TextAlignment.CENTER);
        myunitLabel.setPickOnBounds(true);
        myunitLabel.setOnMouseClicked(e -> app.getHostServices().showDocument("https://gitlab.com/Michard/myunit"));

        HBox externalTechsLogosLayout = new HBox(gradleLogo, gitLogo, gitlabLogo, jenkinsLogo, myunitLabel);
        externalTechsLogosLayout.setAlignment(Pos.CENTER);
        externalTechsLogosLayout.setSpacing(20);

        VBox externalTechsLayout = new VBox(externalTechsLabel, externalTechsLogosLayout);
        externalTechsLayout.setAlignment(Pos.CENTER);
        externalTechsLayout.setSpacing(10.0);

        // Indietro

        Button backButton = new Button("Indietro");
        backButton.setOnAction(e -> {
            if (mainMenu != null) {
                primaryStage.getScene().setRoot(mainMenu.get());
            }
        });

        //Layout completo

        layout = new VBox(
                titleLayout,
                creditsLayout,
                specialThanksLabel,
                externalTechsLayout,
                backButton
        );
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(40);
    }

    /**
     * Ritorna il layout di questa schermata
     * @return Il layout di questa schermata
     */
    Parent get() {
        return layout;
    }

    /**
     * Imposta il menu a cui fare ritorno alla pressione del pulsante Indietro.
     * @param mainMenu Il menu principale
     */
    void setBack(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }
}

package gapp.gui;

import gapp.ulg.game.board.*;
import gapp.ulg.game.board.Pos;
import gapp.ulg.game.util.PlayGUI;
import gapp.ulg.game.util.PlayerGUI;
import gapp.ulg.game.util.SharedReference;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Classe che gestisce la schermata di gioco
 */
class PlayScreen {
    private MainMenu mainMenu;
    private final MyObserver observer = new MyObserver();
    private final Master master = new Master();
    private PlayGUI playGUI;

    //Se il primo e il secondo giocatore sono dei PlayerGUI
    private List<Boolean> isPlayerGUI = new ArrayList<>();

    // Il gioco rappresentato
    private GameRuler<PieceModel<PieceModel.Species>> gR;
    private volatile long gRMechanicsTime = -1; // Limite di tempo per il mechanics

    //Costanti
    private static final double
            BOARD_WIDTH = 600,
            BOARD_HEIGHT = 600;
    private static final Color
            COLOR_BOARD_1 = Color.web("#fff2df"),
            COLOR_BOARD_2 = Color.web("#d9ad7c"),
            COLOR_BOARD_BG = Color.web("#a28362");

    //Variabili GUI
    /**
     * Lunghezza del lato di un quadrato della board, calcolato
     * in modo da riempire il Canvas verticalmente se height > width,
     * orizzontalmente altrimenti
     */
    private double squareLength = 0;

    /**
     * Posizione dell'angolo in alto a sinistra della board, usato come base per il rendering
     */
    private double baseBoardX = 0, baseBoardY = 0;

    //Elementi della GUI - Board
    private Canvas boardCanvas = new Canvas(BOARD_WIDTH, BOARD_HEIGHT);
    private Group selectablePositions = new Group(); // Indicatori di posizioni selezionabili dal PlayerGUI
    private Group boardGroup = new Group();

    //Elementi della GUI - Menu laterale
    private Label
            gameNameLabel = new Label(""),
            pANameLabel = new Label(""),
            pBNameLabel = new Label("");

    private volatile boolean hasScore = false; // Se il gioco ha un punteggio
    private Label scoreTitleLabel = new Label("Punteggi");
    private Label pAScoreLabel = new Label(""), pBScoreLabel = new Label(""); // Punteggi dei giocatori
    private Label scoreUnsupportedLabel = new Label("Gioco senza punteggio");
    private Label winnerLabel = new Label("");
    private VBox scoreLayout = new VBox();

    private Label timerLabel = new Label("Tempo per la mossa");
    private Rectangle timerBorder = new Rectangle(); // Il contorno del rettangolo che rappresenta il tempo
    private Rectangle timerFillBackground = new Rectangle(); // Il colore svelato dallo stringimento di Fill
    private Rectangle timerFill = new Rectangle(); // Interno a timerBorder, la lunghezza diminuise col passare del tempo
    private Label timerDisabledLabel = new Label("Nessun limite");
    private FillTransition timerFillTransition = null;
    private Transition timerWidthTransition = null;
    private Group timerGroup = new Group(); // Il Group che contiene Fill e Border del timer

    private Button
            passButton = new Button("Passa il turno"),
            resignButton = new Button("Arrenditi"),
            moveConfirmButton = new Button("Conferma"),
            backButton = new Button("Indietro"),
            stopButton = new Button("Interrompi la partita");

    private HBox layout = new HBox(); // Layout totale della finestra
    private Stage primaryStage;

    /**
     * Immagini dei pezzi, tenute in una mappa e condivise da tutte le ImageView di pezzi dello stesso tipo
     * per ragioni di efficienza
     */
    private Map<PieceModel<PieceModel.Species>, Image> pieceImages = new HashMap<>();

    PlayScreen(Stage primaryStage) {
        this.primaryStage = primaryStage;

        gameNameLabel.setStyle("-fx-font-family: serif; -fx-font-size: 200%; -fx-text-fill: #ff0000");

        Label playerNamesTitle = new Label("Giocatori");
        playerNamesTitle.setAlignment(javafx.geometry.Pos.CENTER);
        VBox playerNamesLayout = new VBox(
                playerNamesTitle,
                new VBox(pANameLabel, pBNameLabel) {
                    {
                        setAlignment(javafx.geometry.Pos.CENTER);
                        setSpacing(5.0);
                    }
                }
        );
        playerNamesLayout.setAlignment(javafx.geometry.Pos.CENTER);
        playerNamesLayout.setSpacing(15.0);

        VBox pScoresLayout = new VBox(pAScoreLabel, pBScoreLabel);
        pScoresLayout.setAlignment(javafx.geometry.Pos.CENTER);
        pScoresLayout.setSpacing(5.0);
        scoreLayout.getChildren().addAll(
                scoreTitleLabel,
                pAScoreLabel,
                pBScoreLabel,
                scoreUnsupportedLabel,
                winnerLabel
        );
        scoreLayout.setAlignment(javafx.geometry.Pos.CENTER);
        scoreLayout.setSpacing(5.0);

        timerBorder.setFill(Color.web("#333333")); // Grigio scuro
        timerBorder.setHeight(30);
        timerBorder.widthProperty().bind(
                Bindings.max(
                        Bindings.subtract(Bindings.subtract(
                                primaryStage.widthProperty(),
                                boardCanvas.widthProperty()),
                                new SimpleIntegerProperty(20)
                        ),
                        new SimpleIntegerProperty(20) // Border spesso 5 per lato, la barra vera e propria avrà lunghezza minima 10px
                )
        );
        timerFillBackground.setFill(Color.web("#555555")); // Grigio un po' meno scuro
        timerFillBackground.setHeight(20);
        timerFillBackground.widthProperty().bind(Bindings.subtract(timerBorder.widthProperty(), 10)); // 5 per lato
        timerFillBackground.setLayoutX(5); // Spostato di 5 px verso destra...
        timerFillBackground.setLayoutY(5); // ... e 5 px verso il basso rispetto all'origine del Border
        timerFill.setFill(Color.GREY); // Cambierà col tempo con una FillTransition verde -> rosso se il timer è abilitato
        timerFill.setHeight(20);
        timerFill.setWidth(0); // Invisibile, attivato ad inizio mossa
        timerFill.setLayoutX(5);
        timerFill.setLayoutY(5);
        timerGroup.getChildren().addAll(timerBorder, timerFillBackground, timerFill);

        VBox timerLayout = new VBox(
                timerLabel,
                timerGroup,
                timerDisabledLabel
        );
        timerLayout.setAlignment(javafx.geometry.Pos.CENTER);
        timerLayout.setSpacing(5.0);

        passButton.setDisable(true);
        resignButton.setDisable(true);
        moveConfirmButton.setDisable(true);
        backButton.setDisable(true);

        HBox confirmAndBackLayout = new HBox(moveConfirmButton, backButton);
        confirmAndBackLayout.setAlignment(javafx.geometry.Pos.CENTER);
        confirmAndBackLayout.setSpacing(10.0);
        HBox passAndResignButtonsLayout = new HBox(passButton, resignButton);
        passAndResignButtonsLayout.setAlignment(javafx.geometry.Pos.CENTER);
        passAndResignButtonsLayout.setSpacing(10.0);
        VBox playButtonsLayout = new VBox(confirmAndBackLayout, passAndResignButtonsLayout);
        playButtonsLayout.setAlignment(javafx.geometry.Pos.CENTER);
        playButtonsLayout.setSpacing(5.0);

        stopButton.setOnAction(e -> {
            playGUI.stop();
            primaryStage.getScene().setRoot(mainMenu.get());
        });

        VBox rightLayout = new VBox(
                gameNameLabel,
                playerNamesLayout,
                new Separator(),
                scoreLayout,
                new Separator(),
                timerLayout,
                new Separator(),
                playButtonsLayout,
                new Separator(),
                stopButton
        );
        rightLayout.setAlignment(javafx.geometry.Pos.CENTER);
        rightLayout.setSpacing(20.0);
        rightLayout.prefWidthProperty().bind(
                Bindings.subtract(primaryStage.getScene().widthProperty(), boardCanvas.widthProperty())
        );

        boardGroup.getChildren().add(0, boardCanvas);
        boardGroup.getChildren().add(1, selectablePositions);

        layout.getChildren().addAll(
                boardGroup,
                rightLayout
        );

        for (PieceModel.Species species : PieceModel.Species.values()) {
            for (String color : Arrays.asList("bianco", "nero")) {
                PieceModel<PieceModel.Species> piece = new PieceModel<>(species, color);
                Image pieceImage = new Image(getClass().getResource(pieceToImageFile(piece)).toString());
                pieceImages.put(piece, pieceImage);
            }
        }
    }

    /**
     * Imposta l'istanza del menu principale a cui spostarsi alla fine della partita.
     * @param mainMenu Il menu principale
     */
    void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    /**
     * Imposta il {@link PlayGUI} che regola la partita
     * @param playGUI Il PlayGUI che regola la partita
     */
    void setPlayGUI(PlayGUI playGUI) {
        this.playGUI = playGUI;
    }

    /**
     * Imposta le flag booleane che indicano se i giocatori sono {@link PlayerGUI}.
     * @param first Se il primo giocatore è un PlayerGUI
     * @param second Se il secondo giocatore è un PlayerGUI
     */
    void setArePlayerGUIs(boolean first, boolean second) {
        isPlayerGUI.clear();
        isPlayerGUI.add(first);
        isPlayerGUI.add(second);
    }

    /**
     * Ritorna l'{@link gapp.ulg.game.util.PlayGUI.Observer} della GUI.
     * @return L'Observer che rappresenta la GUI.
     */
    public PlayGUI.Observer<PieceModel<PieceModel.Species>> getObserver() {
        return this.observer;
    }

    /**
     * Ritorna il Master gestito dalla GUI.
     * @return Il Master della GUI.
     */
    Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>> getMaster() {
        return this.master;
    }

    /**
     * Ritorna il nodo radice della schermata
     * @return Il nodo radice della schermata
     */
    public Parent get() {
        return layout;
    }

    /**
     * Ritorna true se il giocatore di indice di turnazione (i+1) è un PlayerGUI
     *
     * @param i L'indice (partendo da 0) del giocatore
     * @return Se il giocatore è un PlayerGUI
     */
    private boolean isPlayerGUI(int i) {
        return isPlayerGUI.get(i);
    }

    /**
     * Imposta le label coi nomi dei giocatori in modo da indicare in maniera più evidente
     * il giocatore di turno corrente. Se il gioco è terminato, nessuna label mostra gli
     * effetti aggiuntivi. Il metodo va chiamato nel thread di JavaFX.
     *
     * @param turn Il turno corrente
     * @throws IllegalStateException se il metodo non è chiamato nel JavaFX Application Thread o
     *                               se {@code turn} è maggiore di due (la GUI supporta solo giochi per due giocatori)
     */
    private void setCurrentPlayerLabel(int turn) {
        if (!Platform.isFxApplicationThread()) {
            throw new IllegalStateException("Il metodo va chiamato nel JavaFX Application Thread.");
        }

        Consumer<Label> enabler = l -> {
            l.setStyle("-fx-font-size: 100%; -fx-text-fill: #ff0000");
            l.setEffect(new Glow(1.5));
        };
        Consumer<Label> disabler = l -> {
            l.setStyle("");
            l.setEffect(null);
        };

        if (turn == 1) { // Turno del primo giocatore
            enabler.accept(pANameLabel);
            disabler.accept(pBNameLabel);
        } else if (turn == 2) { // Turno del secondo giocatore
            disabler.accept(pANameLabel);
            enabler.accept(pBNameLabel);
        } else if (turn <= 0) { // Gioco terminato
            disabler.accept(pANameLabel);
            disabler.accept(pBNameLabel);
        } else { // Turno > 2
            throw new IllegalStateException("La GUI supporta solo giochi per due giocatori.");
        }
    }

    /**
     * Imposta il testo delle label degli score in base alla situazione corrente del gioco
     */
    private void updateScores() {
        if (hasScore) {
            pAScoreLabel.setText(gR.players().get(0) + ": " + gR.score(1));
            pBScoreLabel.setText(gR.players().get(1) + ": " + gR.score(2));
        } else {
            pAScoreLabel.setText(gR.players().get(0) + ": -");
            pBScoreLabel.setText(gR.players().get(1) + ": -");
        }
    }

    /**
     * Fa iniziare lo scorrimento della barra del timer, se il gioco ha un timeout,
     * altrimenti non fa nulla. Se il timer sta già scorrendo, viene resettato.
     * <p>
     * Questo metodo va chiamato nel JavaFX Application Thread.
     */
    private void startMoveTimer() {
        if (timerFillTransition != null) {
            timerFillTransition.stop();
        }
        if (timerWidthTransition != null) {
            timerWidthTransition.stop();
        }
        if (gR.mechanics().time > 0) {
            timerFillTransition = new FillTransition(
                    Duration.millis(gRMechanicsTime / 2.0),
                    timerFill,
                    Color.web("#00ff00ff"),
                    Color.YELLOW
            );
            timerFillTransition.setOnFinished(e -> {
                timerFillTransition = new FillTransition(
                        Duration.millis(gRMechanicsTime / 2.0),
                        timerFill,
                        Color.YELLOW,
                        Color.RED
                );
                timerFillTransition.play();
            });
            timerWidthTransition = new Transition() {
                {
                    setCycleDuration(Duration.millis(gRMechanicsTime));
                }

                @Override
                protected void interpolate(double frac) {
                    timerFill.setWidth((timerBorder.getWidth() - 10.0) * (1.0 - frac));
                }
            };

            timerFillTransition.play();
            timerWidthTransition.play();
        }
    }

    /**
     * Interrompe le animazioni relative al timer per la scelta della mossa e
     * svuota la barra del timer stesso.
     */
    private void stopMoveTimer() {
        if (timerFillTransition != null) {
            timerFillTransition.stop();
        }
        if (timerWidthTransition != null) {
            timerWidthTransition.stop();
        }

        timerFill.setWidth(0);
    }

    /**
     * Classe che si occupa della comunicazione tra la GUI e il PlayGUI.
     */
    private class MyObserver implements PlayGUI.Observer<PieceModel<PieceModel.Species>> {

        @Override
        public void setGame(GameRuler<PieceModel<PieceModel.Species>> g) {
            gR = Objects.requireNonNull(g);
            gRMechanicsTime = gR.mechanics().time;

            try {
                gR.score(1);
                hasScore = true;
            } catch (UnsupportedOperationException e) {
                hasScore = false;
            }

            Board<PieceModel<PieceModel.Species>> gRBoard = gR.getBoard();
            final String gRName = gR.name();
            final String pAName = gR.players().get(0);
            final String pBName = gR.players().get(1);

            if (gRBoard.system() == Board.System.OCTAGONAL) {

                squareLength = Math.min(
                        BOARD_WIDTH / Math.max(gRBoard.width(), gRBoard.height()),
                        BOARD_HEIGHT / Math.max(gRBoard.width(), gRBoard.height())
                );
                baseBoardX = (BOARD_WIDTH - squareLength * gRBoard.width()) / 2.0;
                baseBoardY = (BOARD_HEIGHT - squareLength * gRBoard.height()) / 2.0;

                FutureTask<Void> platformTask = new FutureTask<>(() -> {
                    gameNameLabel.setText(gRName);
                    pANameLabel.setText(pAName);
                    pBNameLabel.setText(pBName);

                    scoreUnsupportedLabel.setVisible(!hasScore);
                    winnerLabel.setText("");
                    updateScores();

                    timerDisabledLabel.setVisible(gRMechanicsTime == -1);

                    GraphicsContext gC = boardCanvas.getGraphicsContext2D();

                    //Disegno della board
                    gC.setFill(COLOR_BOARD_BG);
                    gC.fillRect(0, 0, BOARD_WIDTH, BOARD_HEIGHT);
                    for (int y = 0; y < gRBoard.height(); ++y) {
                        for (int x = 0; x < gRBoard.width(); ++x) {
                            Pos p = new Pos(x, y);
                            if (gRBoard.isPos(p)) {
                                gC.setFill((x + y) % 2 == 0 ? COLOR_BOARD_1 : COLOR_BOARD_2);
                                gC.fillRect(
                                        baseBoardX + x * squareLength,
                                        BOARD_HEIGHT - baseBoardY - (y + 1) * squareLength,
                                        squareLength,
                                        squareLength
                                );
                            }
                        }
                    }

                    selectablePositions.getChildren().clear();

                    //Disegno dei pezzi all'inizio della partita
                    if (boardGroup.getChildren().size() > 2) {
                        boardGroup.getChildren().remove(2, boardGroup.getChildren().size());
                    }
                    Set<Pos> posWithPieces = gRBoard.get();
                    posWithPieces.forEach(pos -> {
                        addOctGuiPiece(gRBoard.get(pos), pos);
                    });
                    startMoveTimer();

                    primaryStage.getScene().setRoot(layout);
                    return null;
                });

                Platform.runLater(platformTask);
                try {
                    platformTask.get();
                } catch (ExecutionException | InterruptedException e) {
                    throw new RuntimeException(e instanceof ExecutionException ? e.getCause() : e);
                }
            }
        }

        @Override
        public void moved(int i, Move<PieceModel<PieceModel.Species>> m) {
            if (gR != null && gR.result() == -1) {
                if (m != null) {
                    if (gR.isPlaying(i) && gR.isValid(m)) {
                        gR.move(m);
                        AtomicInteger result = new AtomicInteger(gR.result());

                        Platform.runLater(() -> {
                            stopMoveTimer();
                            moveConfirmButton.setDisable(true);
                            backButton.setDisable(true);
                            passButton.setDisable(true);
                            resignButton.setDisable(true);
                            selectablePositions.getChildren().clear();
                        });

                        /**
                         * Se la mossa è stata eseguita da un PlayerGUI, allora è già stata rappresentata sulla GUI e
                         * basta confermarla, altrimenti va mostrata graficamente
                         */
                        if (isPlayerGUI(i - 1)) {
                            //conferma la mossa (non fare niente?)
                        } else if (m.kind == Move.Kind.ACTION) {
                            playActions(m.actions);
                        }
                        final int gRTurn = gR.turn();
                        FutureTask<Void> updateGUI = new FutureTask<>(() -> {
                            setCurrentPlayerLabel(gRTurn);
                            updateScores();
                            if (result.get() == -1) {
                                startMoveTimer();
                            } else if (result.get() == 0) {
                                winnerLabel.setText("Pareggio");
                            } else {
                                winnerLabel.setText("Vincitore: " + (i == 1 ? pANameLabel : pBNameLabel).getText());
                            }
                            return null;
                        });
                        Platform.runLater(updateGUI);
                        try {
                            updateGUI.get();
                        } catch (ExecutionException e) {
                            throw new RuntimeException(e.getCause());
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    } else {
                        throw new IllegalArgumentException("Illegal turn or move.");
                    }
                } else {
                    throw new NullPointerException("m in RandPlayer#moved(int i, Move<P> m) must not be null.");
                }
            } else {
                throw new IllegalStateException("gR is either not set or finished.");
            }
        }

        @Override
        public void limitBreak(int i, String msg) {
            /**
             * Non c'è più bisogno di eseguire il RESIGN
             */
            Objects.requireNonNull(msg);
            if (i <= 0 || i > 2) {
                throw new IllegalArgumentException("Indice di turnazione non valido");
            }
            // Sarebbe graficamente migliore qualche effetto grafico, ma comunque sta per venire lanciato anche interrupted
        }

        @Override
        public void interrupted(String msg) {
            Platform.runLater(() -> {
                if (timerWidthTransition != null && timerWidthTransition.getStatus() != Animation.Status.STOPPED) {
                    timerWidthTransition.stop();
                }
                timerFill.setWidth(0);
                if (timerFillTransition != null && timerFillTransition.getStatus() != Animation.Status.STOPPED) {
                    timerFillTransition.stop();
                }

                selectablePositions.getChildren().clear();

                winnerLabel.setText(msg);

                moveConfirmButton.setDisable(true);
                backButton.setDisable(true);
                passButton.setDisable(true);
                resignButton.setDisable(true);
            });
        }
    }

    private class Master implements Consumer<PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>>> {
        private PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>> mC;
        private List<Pos> selected; // Istanziata concorrente
        private volatile Integer mCTreeDepth;

        @Override
        public synchronized void accept(PlayerGUI.MoveChooser<PieceModel<PieceModel.Species>> pieceModelMoveChooser) {
            this.mC = pieceModelMoveChooser;

            selected = new CopyOnWriteArrayList<>(); //Posizioni selezionate

            //Profondità nell'albero delle mosse, se 0 non è possibile fare back
            mCTreeDepth = 0;

            //Gioca la radice
            mC.subMove().ifPresent(m -> {
                playActions(m.actions);
                Platform.runLater(() -> {
                    updateSelectablePositions();
                    moveConfirmButton.setDisable(!mC.isFinal());
                });
            });

            //Imposta la GUI per ricevere gli input dell'utente
            Platform.runLater(() -> {
                //Segna le posizioni selezionabili
                updateSelectablePositions();

                moveConfirmButton.setDisable(!mC.isFinal());
                moveConfirmButton.setOnAction(e -> {
                    mC.move();
                    disablePlayButtons();
                });

                backButton.setDisable(true);
                backButton.setOnAction(e -> {
                    if (mCTreeDepth > 0) {
                        mCTreeDepth--;
                        disableInput();
                        Move<PieceModel<PieceModel.Species>> backMove = mC.back();
                        selected.clear();
                        Thread t = new Thread(() -> {
                            playActions(backMove.actions);
                            Platform.runLater(this::enableInputs);
                        });
                        t.setDaemon(true);
                        t.start();
                    }
                });

                passButton.setDisable(!mC.mayPass() && mCTreeDepth <= 0);
                passButton.setOnAction(e -> {
                    mC.pass();
                    disablePlayButtons();
                });

                resignButton.setDisable(false);
                resignButton.setOnAction(e -> {
                    mC.resign();
                    disablePlayButtons();
                });

                boardGroup.setOnMouseClicked(me -> {
                    if (me.getButton() != MouseButton.PRIMARY) {
                        return;
                    }

                    //Posizione del mouse e Pos cliccata
                    double mouseX = me.getX(), mouseY = me.getY();
                    int x = (int) Math.floor((mouseX - baseBoardX) / squareLength);
                    int y = (int) Math.floor((gR.getBoard().height() * squareLength - 1 - (mouseY - baseBoardY)) / squareLength);
                    if (x < 0 || y < 0 || !gR.getBoard().isPos(new Pos(x, y))) {
                        return;
                    }

                    //Controlla cerchio di selezione cliccato
                    double circleRadiusSquared = (CIRCLE_RADIUS_PERCENTAGE * squareLength / 2.0) *
                            (CIRCLE_RADIUS_PERCENTAGE * squareLength / 2.0);

                    for (Node node : selectablePositions.getChildren()) {
                        //Distanza del click dal centro del nodo
                        double dx = mouseX - ((Circle) node).getCenterX(), dy = mouseY - ((Circle) node).getCenterY();
                        boolean clicked = circleRadiusSquared >= (dx * dx) + (dy * dy);
                        if (clicked) {
                            Platform.runLater(() -> selectablePositions.getChildren().clear()); //Rimuovi i cerchi di selezione
                            if (selected.size() == 1) { // Nei giochi attuali del framework, solo move e jump necessitano di due azioni dell'utente
                                List<Move<PieceModel<PieceModel.Species>>> subMoves = mC.childrenSubMoves();
                                for (Move<PieceModel<PieceModel.Species>> move : subMoves) { // Cerca la jump
                                    Action<PieceModel<PieceModel.Species>> firstAction = move.actions.get(0);
                                    if (firstAction.kind == Action.Kind.JUMP &&
                                            firstAction.pos.get(0).equals(selected.get(0)) &&
                                            firstAction.pos.get(1).equals(new Pos(x, y))
                                            ) {
                                        selected.clear();
                                        mC.jumpSelection(new Pos(x, y));
                                        moveConfirmButton.setDisable(true);
                                        Thread t = new Thread(() -> {
                                            playActions(move.actions);
                                            Platform.runLater(()->{
                                                updateSelectablePositions();
                                                moveConfirmButton.setDisable(!mC.isFinal());
                                                backButton.setDisable(false);
                                                mCTreeDepth++;
                                            });
                                        });
                                        t.setDaemon(true);
                                        t.start();
                                        return;
                                    }
                                }
                                //Cerca la move
                                for (Move<PieceModel<PieceModel.Species>> move : subMoves) {
                                    Action<PieceModel<PieceModel.Species>> firstAction = move.actions.get(0);
                                    if (firstAction.equals(moveActionFromPositions(selected.get(0), new Pos(x,y)))) {
                                        selected.clear();
                                        mC.moveSelection(firstAction.dir, firstAction.steps);
                                        moveConfirmButton.setDisable(true);
                                        Thread t = new Thread(() -> {
                                            playActions(move.actions);
                                            Platform.runLater(()->{
                                                updateSelectablePositions();
                                                moveConfirmButton.setDisable(!mC.isFinal());
                                                backButton.setDisable(false);
                                                mCTreeDepth++;
                                            });
                                        });
                                        t.setDaemon(true);
                                        t.start();
                                        return;
                                    }
                                }
                            } else { // selected.size() == 0
                                selected.add(new Pos(x, y));
                                List<Move<PieceModel<PieceModel.Species>>> selectedMoves = mC.select(selected.toArray(new Pos[1]));
                                if (selectedMoves.size() == 1) {
                                    Action<PieceModel<PieceModel.Species>> firstAction = selectedMoves.get(0).actions.get(0);
                                    if (firstAction.kind == Action.Kind.REMOVE || firstAction.kind == Action.Kind.ADD || firstAction.kind == Action.Kind.SWAP) {
                                        if (mC.selectionPieces().size() == 1) {
                                            mC.doSelection(selectedMoves.get(0).actions.get(0).piece);
                                        } else {
                                            System.out.println("Non testato");
                                            List<PieceModel<PieceModel.Species>> pieces =
                                                    mC.selectionPieces();
                                            final SharedReference<PieceModel<PieceModel.Species>> choosenPiece = new SharedReference<>(pieces.get(0));
                                            Stage chosePiece = new Stage();
                                            Scene scene = new Scene(new VBox(
                                                    new ChoiceBox<String>() {{
                                                        setItems(
                                                                FXCollections.observableArrayList(pieces.stream().map(
                                                                        p -> p == null ? "null" : p.species.toString() + " " + p.color
                                                                ).collect(Collectors.toList()))
                                                        );
                                                        getSelectionModel().selectedIndexProperty().addListener((obs, oldI, newI) -> {
                                                            if (newI.intValue() != -1)
                                                                choosenPiece.set(pieces.get(newI.intValue()));
                                                            chosePiece.close();
                                                        });
                                                        getSelectionModel().select(0);
                                                    }}
                                            ));
                                            chosePiece.setScene(scene);
                                            chosePiece.initModality(Modality.WINDOW_MODAL);
                                            chosePiece.initOwner(primaryStage);
                                            chosePiece.showAndWait();
                                            mC.doSelection(choosenPiece.get());
                                        }
                                    } else if (firstAction.kind == Action.Kind.JUMP) {
                                        mC.jumpSelection(firstAction.pos.get(1));
                                    } else {
                                        mC.moveSelection(firstAction.dir, firstAction.steps);
                                    }
                                    selected.clear();
                                    moveConfirmButton.setDisable(true);
                                    Thread t = new Thread(() -> {
                                        playActions(selectedMoves.get(0).actions);
                                        Platform.runLater(() -> {
                                            updateSelectablePositions();
                                            moveConfirmButton.setDisable(!mC.isFinal());
                                            backButton.setDisable(false);
                                            mCTreeDepth++;
                                        });
                                    });

                                    t.setDaemon(true);
                                    t.start();
                                } else {
                                    Platform.runLater(this::updateSelectablePositions);
                                }
                            }
                            return;
                        }
                    }
                });
            });
        }

        /**
         * Partendo dalla selezione corrente e, a seconda dei casi, dai figli del nodo corrente dell'albero delle
         * mosse oppure dalle mosse quasi-selezionate, ricava le posizioni della board che il giocatore può
         * cliccare per effettuare azioni di gioco e le mostra graficamente
         */
        private void updateSelectablePositions() {
            selectablePositions.getChildren().clear();

            //Mosse non escluse dalla selezione, data una selezione non vuota
            List<Move<PieceModel<PieceModel.Species>>> notUnplayables = new ArrayList<>();
            notUnplayables.addAll(mC.quasiSelected());
            if (!selected.isEmpty())
                notUnplayables.addAll(mC.select(selected.toArray(new Pos[1])));

            List<Move<PieceModel<PieceModel.Species>>> moves =
                    selected.isEmpty() ? mC.childrenSubMoves() : notUnplayables;

            Set<Pos> selectables = new HashSet<>();
            moves.forEach(move -> {
                //Le mosse che non sono PASS o RESIGN hanno tutte almeno un'azione
                Action<PieceModel<PieceModel.Species>> firstAction = move.actions.get(0);
                if (firstAction.kind == Action.Kind.JUMP) {
                    selectables.add(firstAction.pos.get(selected.size() != 1 ? 0 : 1));
                } else if (firstAction.kind == Action.Kind.MOVE && selected.size() == 1) {
                    firstAction.pos.forEach(p -> selectables.add(destination(p, firstAction.dir, firstAction.steps)));
                } else {
                    selectables.addAll(firstAction.pos.stream().filter(p -> !selected.contains(p)).collect(Collectors.toList()));
                }
            });
            selectables.forEach(p -> {
                if (selected.contains(p)) return;

                Circle circle = new Circle(
                        getSquareStartX(p.b) + squareLength / 2.0,
                        getSquareStartY(p.t) + squareLength / 2.0,
                        squareLength / 2.0 * CIRCLE_RADIUS_PERCENTAGE,
                        Color.web("#ff8888ff")
                );
                selectablePositions.getChildren().add(circle);
            });
        }

        /**
         * Metodo di supporto a {@link this#updateSelectablePositions()}, data una mossa restituisce true
         * se la mossa ha come prima azione una REMOVE e come seconda azione una MOVE tale che almeno un pezzo spostato
         * finisca in una delle posizioni della prima azione.
         *
         * @param move La mossa da esaminare
         * @return Se la mossa è una REMOVE + MOVE
         */
        private boolean isRemoveAndMove(Move<PieceModel<PieceModel.Species>> move) {
            if (move.kind != Move.Kind.ACTION) return false;
            if (move.actions.size() < 2) return false;
            Action<PieceModel<PieceModel.Species>> a1 = move.actions.get(0);
            Action<PieceModel<PieceModel.Species>> a2 = move.actions.get(1);
            if (a1.kind != Action.Kind.REMOVE || a2.kind != Action.Kind.MOVE) return false;
            for (Pos p : a2.pos) {
                Pos destPos = destination(p, a2.dir, a2.steps);
                if (a1.pos.contains(destPos)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Ritorna la posizione di {@code p} dopo essere spostata di {@code steps} passi nella direzione {@code d}.
         *
         * @param p     Posizione di partenza
         * @param d     Direzione del movimento
         * @param steps Numero di passi
         * @return Posizione di destinazione
         */
        private Pos destination(Pos p, Board.Dir d, int steps) {
            int dx = 0, dy = 0;
            switch (d) {
                case UP_L:
                    dx = -1;
                    dy = 1;
                    break;
                case UP:
                    dx = 0;
                    dy = 1;
                    break;
                case UP_R:
                    dx = 1;
                    dy = 1;
                    break;
                case RIGHT:
                    dx = 1;
                    dy = 0;
                    break;
                case DOWN_R:
                    dx = 1;
                    dy = -1;
                    break;
                case DOWN:
                    dx = 0;
                    dy = -1;
                    break;
                case DOWN_L:
                    dx = -1;
                    dy = -1;
                    break;
                case LEFT:
                    dx = -1;
                    dy = 0;
                    break;
                default:
                    throw new IllegalStateException();
            }
            return new Pos(p.b + dx * steps, p.t + dy * steps);
        }

        /**
         * Date una posizione iniziale e una finale, e assumendo che sia possibile passare dalla prima alla seconda
         * tramite un'azione MOVE, ritorna l'azione di tale movimento
         * @param start Posizione iniziale
         * @param end Posizione finale
         * @return L'azione di movimento
         */
        private Action<PieceModel<PieceModel.Species>> moveActionFromPositions(Pos start, Pos end) {
            int dx = end.b - start.b;
            int dy = end.t - start.t;
            int abs_dx = dx >= 0 ? dx : -dx;
            int abs_dy = dy >= 0 ? dy : -dy;
            Board.Dir d;
            if (abs_dx == abs_dy) {
                if (dx > 0 && dy > 0) d = Board.Dir.UP_R;
                else if (dx > 0 && dy < 0) d = Board.Dir.DOWN_R;
                else if (dx < 0 && dy > 0) d = Board.Dir.UP_L;
                else d = Board.Dir.DOWN_L;
            } else {
                if (dx == 0) d = dy > 0 ? Board.Dir.UP : Board.Dir.DOWN;
                else d = dx > 0 ? Board.Dir.RIGHT : Board.Dir.LEFT;
            }
            int steps = dx == 0 ? abs_dy : abs_dx;
            return new Action<>(d, steps, start);
        }

        /**
         * Disabilita ogni forma di input interna alla finestra
         */
        private void disableInput() {
            disablePlayButtons();
        }

        /**
         * Disabilita i bottoni usati dalla PlayerGUI (moveConfirm, back, pass e resign e le posizioni selezionabili)
         */
        private void disablePlayButtons() {
            selectablePositions.getChildren().clear();
            moveConfirmButton.setDisable(true);
            backButton.setDisable(true);
            passButton.setDisable(true);
            resignButton.setDisable(true);
        }

        /**
         * Riabilita i meccanismi di input interni alla finestra
         */
        private void enableInputs() {
            updateSelectablePositions();
            moveConfirmButton.setDisable(!mC.isFinal());
            backButton.setDisable(mCTreeDepth <= 0);
            passButton.setDisable(!mC.mayPass() && mCTreeDepth <= 0);
            resignButton.setDisable(false);
        }

        //Il rapporto (raggio del cerchio di selezione / semilunghezza della casella della board)
        private static final double CIRCLE_RADIUS_PERCENTAGE = 0.9;
    }

    private String pieceToImageFile(PieceModel<PieceModel.Species> p) {
        Objects.requireNonNull(p);
        return (p.species.toString().toLowerCase()) +
                "_" +
                (p.color.equals("bianco") ? "white" : "black") +
                ".png";
    }

    /**
     * Aggiunge al {@link Group} e alla mappa dei pezzi il pezzo {@code piece}, nella
     * posizione {@pos} di una board ottagonale.
     *
     * @param piece Il pezzo da disegnare
     * @param pos   La posizione in cui disegnarlo
     * @throws NullPointerException se piece è null, pos è null, oppure se non
     *                              è stato impostato un gioco
     */
    private PosImageView addOctGuiPiece(PieceModel<PieceModel.Species> piece, Pos pos) {
        return addOctGuiPiece(piece, pos, 1);
    }

    private PosImageView addOctGuiPiece(PieceModel<PieceModel.Species> piece, Pos pos, double startOpacity) {
        PosImageView pieceIV = new PosImageView(Objects.requireNonNull(pieceImages.get(piece)), pos);
        pieceIV.setLayoutX(getSquareStartX(pos.b));
        pieceIV.setLayoutY(getSquareStartY(pos.t));
        pieceIV.setFitWidth(squareLength);
        pieceIV.setFitHeight(squareLength);
        pieceIV.setOpacity(startOpacity);
        boardGroup.getChildren().add(pieceIV);
        return pieceIV;
    }

    private void addOctGuiPiece(PieceModel<PieceModel.Species> piece, Pos pos, boolean fade) {
        addOctGuiPiece(piece, pos, fade, e -> {
        });
    }

    private void addOctGuiPiece(PieceModel<PieceModel.Species> piece, Pos pos, boolean fade, EventHandler<ActionEvent> onEnd) {
        PosImageView pieceIV = new PosImageView(Objects.requireNonNull(pieceImages.get(piece)), pos);
        pieceIV.setLayoutX(getSquareStartX(pos.b));
        pieceIV.setLayoutY(getSquareStartY(pos.t));
        pieceIV.setFitWidth(squareLength);
        pieceIV.setFitHeight(squareLength);
        pieceIV.setOpacity(0);
        boardGroup.getChildren().add(pieceIV);
        if (fade) {
            FadeTransition ft = new FadeTransition(Duration.millis(400), pieceIV);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.setOnFinished(onEnd);
            ft.play();
        }
    }

    /**
     * Retituisce la coordinata orizzontale nel Group dell'angolo in alto a sinistra
     * del quadrato della board di ascissa b.
     *
     * @param b L'ascissa del quadrato
     * @return La coordinata orizzontale del quadrato
     */
    private double getSquareStartX(int b) {
        return baseBoardX + b * squareLength;
    }

    /**
     * Retituisce la coordinata orizzontale nel Group dell'angolo in alto a sinistra
     * del quadrato della board di ascissa b.
     * Assume che sia impostato un GameRuler.
     *
     * @param t L'ascissa del quadrato
     * @return La coordinata orizzontale del quadrato
     */
    private double getSquareStartY(int t) {
        return baseBoardY + (gR.getBoard().height() - 1 - t) * squareLength;
    }

    /**
     * Rimuove il pezzo in posizione p dal {@link Group} della board e dalla mappa dei pezzi
     *
     * @param p La posizione del pezzo da rimuovere
     */
    private void removeOctGuiPiece(Pos p) {
        boardGroup.getChildren().remove(getPieceIndexInBoardGroup(p));
    }

    private static class PosImageView extends ImageView {
        private Pos pos;

        PosImageView() {
            super();
        }

        PosImageView(Image image) {
            this(image, new Pos(0, 0));
        }

        PosImageView(Image image, Pos p) {
            super(image);
            this.pos = p;
        }

        void setPos(Pos p) {
            this.pos = Objects.requireNonNull(p);
        }

        Pos getPos() {
            return pos;
        }
    }

    private final Object actionAnimNotifier = new Object();
    private volatile AtomicBoolean isActionAnimFinished = new AtomicBoolean(false); // Flag per il wait-notify
    //private volatile Timeline

    private void playActions(List<Action<PieceModel<PieceModel.Species>>> actions) {
        if (Thread.currentThread().getName().equals("JavaFX Application Thread"))
            throw new RuntimeException("playActions non va chiamato nel JavaFX Application Thread.");

        for (Action<PieceModel<PieceModel.Species>> action : actions) {
            isActionAnimFinished.set(false);
            Platform.runLater(() -> {
                switch (action.kind) {
                    case ADD:
                        PosImageView newPiece = addOctGuiPiece(action.piece, action.pos.get(0), 0);
                        Timeline addTimeline = new Timeline();
                        addTimeline.getKeyFrames().add(new KeyFrame(
                                Duration.millis(400), new KeyValue(newPiece.opacityProperty(), 1)
                        ));
                        addTimeline.setOnFinished(e -> {
                            isActionAnimFinished.set(true);
                            synchronized (actionAnimNotifier) {
                                actionAnimNotifier.notifyAll();
                            }
                        });
                        addTimeline.play();
                        break;
                    case REMOVE:
                        Timeline removeTimeline = new Timeline();
                        for (Pos p : action.pos) {
                            removeTimeline.getKeyFrames().add(new KeyFrame(
                                    Duration.millis(400), new KeyValue(getPieceIVInBoardGroup(p).opacityProperty(), 0)
                            ));
                        }
                        removeTimeline.setOnFinished(e -> {
                            action.pos.forEach(this::removeOctGuiPiece);
                            isActionAnimFinished.set(true);
                            synchronized (actionAnimNotifier) {
                                actionAnimNotifier.notifyAll();
                            }
                        });
                        removeTimeline.play();
                        break;
                    case MOVE:
                        int dx_move, dy_move;
                        if (Arrays.asList(Board.Dir.UP_L, Board.Dir.LEFT, Board.Dir.DOWN_L).contains(action.dir)) {
                            dx_move = -1;
                        } else if (Arrays.asList(Board.Dir.UP_R, Board.Dir.RIGHT, Board.Dir.DOWN_R).contains(action.dir)) {
                            dx_move = 1;
                        } else {
                            dx_move = 0;
                        }

                        if (Arrays.asList(Board.Dir.DOWN_L, Board.Dir.DOWN, Board.Dir.DOWN_R).contains(action.dir)) {
                            dy_move = -1;
                        } else if (Arrays.asList(Board.Dir.UP_L, Board.Dir.UP, Board.Dir.UP_R).contains(action.dir)) {
                            dy_move = 1;
                        } else {
                            dy_move = 0;
                        }

                        playMoveAction(action.pos, dx_move * action.steps, dy_move * action.steps);
                        break;
                    case JUMP:
                        //Differenza in coordinate del movimento
                        int dx = action.pos.get(1).b - action.pos.get(0).b;
                        int dy = action.pos.get(1).t - action.pos.get(0).t;

                        playMoveAction(Collections.singletonList(action.pos.get(0)), dx, dy);
                        break;
                    case SWAP:
                        Timeline swapOutTimeline = new Timeline();
                        for (Pos p : action.pos) {
                            PosImageView oldPiv = (PosImageView) getPieceIVInBoardGroup(p);
                            oldPiv.setOpacity(1.0);
                            swapOutTimeline.getKeyFrames().add(new KeyFrame(
                                    Duration.millis(400), new KeyValue(getPieceIVInBoardGroup(p).opacityProperty(), 0)
                            ));
                        }
                        swapOutTimeline.setOnFinished(e -> {
                            Timeline swapInTimeline = new Timeline();
                            action.pos.forEach(p -> {
                                removeOctGuiPiece(p);
                                PosImageView newpiece = addOctGuiPiece(action.piece, p, 0);
                                swapInTimeline.getKeyFrames().add(new KeyFrame(
                                        Duration.millis(400), new KeyValue(newpiece.opacityProperty(), 1)
                                ));
                            });
                            swapInTimeline.setOnFinished(e2 -> {
                                isActionAnimFinished.set(true);
                                synchronized (actionAnimNotifier) {
                                    actionAnimNotifier.notifyAll();
                                }
                            });
                            swapInTimeline.play();
                        });
                        swapOutTimeline.play();
                        break;
                    default:
                        break;
                }
            });
            synchronized (actionAnimNotifier) {
                while (!isActionAnimFinished.get()) {
                    try {
                        actionAnimNotifier.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    }

    /**
     * Da inizio ad animazioni di translazione, per tutti i pezzi nelle posizioni in {@code startPositions},
     * con uno spostamento di {@code dx} caselle orizzontalmente e {@code dy} caselle verticalmente.
     * Il metodo ritorna subito, ma l'animazione segnala la sua fine con un notify.
     *
     * @param startPositions Posizioni dei pezzi da spostare
     * @param dx             Traslazione orizzontale
     * @param dy             Traslazione trasversale
     * @return               La Tiimeline creata
     */
    private Timeline playMoveAction(List<Pos> startPositions, int dx, int dy) {
        Timeline t = new Timeline();
        List<PosImageView> pIVs = startPositions
                .stream()
                .map(p -> (PosImageView) getPieceIVInBoardGroup(p))
                .collect(Collectors.toList());
        for (PosImageView pIV : pIVs) {
            t.getKeyFrames().add(new KeyFrame(
                    Duration.millis(400), new KeyValue(pIV.xProperty(), pIV.xProperty().get() + dx * squareLength)
            ));
            t.getKeyFrames().add(new KeyFrame(
                    Duration.millis(400), new KeyValue(pIV.yProperty(), pIV.yProperty().get() - dy * squareLength)
            ));
        }
        t.setOnFinished(e -> {
            pIVs.forEach(pIV -> {
                Pos pivPos = pIV.getPos();
                pIV.setPos(new Pos(pivPos.b + dx, pivPos.t + dy));
            });
            isActionAnimFinished.set(true);
            synchronized (actionAnimNotifier) {
                actionAnimNotifier.notifyAll();
            }
        });
        t.play();
        return t;
    }

    /**
     * Ritorna l'indice, all'interno del Group della board, dell'immagine del pezzo in posizione {@code p}.
     * @param p Posizione del pezzo
     * @return L'indice dell'immagine del pezzo
     */
    private int getPieceIndexInBoardGroup(Pos p) {
        ObservableList<Node> groupNodes = boardGroup.getChildren();
        for (int i = 2; i < groupNodes.size(); i++) { //Il primo elemento è la board, il secondo selectablePositions
            Node n = groupNodes.get(i);
            if (n instanceof PosImageView && PosImageView.class.cast(n).getPos().equals(p)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Ritorna la {@link PosImageView} del pezzo in posizione {@code p} della Board.
     * @param p Posizione del pezzzo
     * @return ImageView del pezzo
     */
    private Node getPieceIVInBoardGroup(Pos p) {
        return boardGroup.getChildren().get(getPieceIndexInBoardGroup(p));
    }
}
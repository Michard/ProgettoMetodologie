package gapp.gui;

import javafx.scene.paint.Color;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Classe usata per ottenere oggetti {@link Color} tramite nomi di colori
 * espressi non necessariamente in inglese.
 */
public class Colors {

    /**
     * Costruttore senza argomenti privato, usato per impedire
     * l'istaniazione della classe.
     */
    private Colors() {};

    /**
     * Ritorna il {@link Color} del colore scelto.
     *
     * @param color Il nome del colore di cui si vuole il {@link Color}
     * @return Il {@link Color} corrispondente al colore
     * @throws NullPointerException se {@code color} è null
     * @throws IllegalArgumentException se {@code color} non è un colore valido
     */
    public static Color get(String color) {
        if (colorMap == null) {
            makeColorMap();
        }
        Color c = colorMap.get(Objects.requireNonNull(color).toLowerCase());
        if (c != null) {
            return c;
        } else {
            throw new IllegalArgumentException("Colore non valido");
        }
    }

    /**
     * La mappa che associa i nomi dei colori ai colori stessi
     */
    private static Map<String, Color> colorMap;

    /**
     * Metodo di creazione della mappa
     */
    private static void makeColorMap() {
        colorMap = new HashMap<>();

        //Italiano
        colorMap.put("bianco", Color.WHITE);
        colorMap.put("nero", Color.BLACK);
        colorMap.put("rosso", Color.RED);
        colorMap.put("blu", Color.BLUE);
        colorMap.put("verde", Color.GREEN);
        colorMap.put("giallo", Color.YELLOW);

        //Inglese
        colorMap.put("white", Color.WHITE);
        colorMap.put("black", Color.BLACK);
        colorMap.put("red", Color.RED);
        colorMap.put("blue", Color.BLUE);
        colorMap.put("green", Color.GREEN);
        colorMap.put("yellow", Color.YELLOW);
    }
}

package gapp.gui;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

class MainMenu {
    private VBox layout;

    private PlaySettingsMenu playSettingsMenu;
    private SettingsMenu settingsMenu;
    private CreditsScreen creditsScreen;

    MainMenu(Stage stage) {
        Label title = new Label("Progetto Metodologie di Programmazione");
        title.setAlignment(Pos.CENTER);
        title.setTextAlignment(TextAlignment.CENTER);
        title.setStyle("-fx-font-family: serif; -fx-font-size: 300%");

        Button btn_play = new Button("Gioca");
        btn_play.setOnAction(e -> {
            if (playSettingsMenu != null)
                stage.getScene().setRoot(playSettingsMenu.get());
        });
        Button btn_settings = new Button("Impostazioni");
        btn_settings.setOnAction(e -> {
            stage.getScene().setRoot(settingsMenu.get());
        });
        Button btn_about = new Button("Crediti");
        btn_about.setOnAction(e -> {
            stage.getScene().setRoot(creditsScreen.get());
        });
        Button btn_exit = new Button("Esci");
        btn_exit.setOnAction(e -> {
            stage.close();
        });

        VBox buttonsLayout = new VBox(btn_play, btn_settings, btn_about, btn_exit);
        buttonsLayout.setAlignment(Pos.CENTER);
        buttonsLayout.setSpacing(20.0);

        layout = new VBox(
                title,
                buttonsLayout
        );
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(60);
    }

    /**
     * @return Il layout del menu
     */
    public Parent get() {
        return layout;
    }

    /**
     * Imposta il menu a cui spostarsi alla pressione
     * del tasto Gioca.
     * @param psMenu Il menu delle impostazioni partita
     */
    void setPlaySettingsMenu(PlaySettingsMenu psMenu) {
        this.playSettingsMenu = psMenu;
    }

    /**
     * Imposta il menu a cui spostarsi alla pressione
     * del taso Impostazioni.
     * @param settingsMenu Il menu delle impostazioni
     */
    void setSettingsMenu(SettingsMenu settingsMenu) {
        this.settingsMenu = settingsMenu;
    }

    /**
     * Imposta il menu a cui spostarsi alla pressione
     * del tasto Crediti
     * @param creditsScreen Il menu dei crediti
     */
    void setCreditsScreen(CreditsScreen creditsScreen) { this.creditsScreen = creditsScreen; }
}
